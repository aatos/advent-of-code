use std::alloc::{alloc, dealloc, Layout};
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead};

struct List {
    begin: *mut Node,
    end: *mut Node,
}

impl List {
    unsafe fn new(data: u32) -> List {
        let n = Node::new(data);

        List { begin: n, end: n }
    }

    unsafe fn new_vec(data: &Vec<u32>) -> List {
        let mut l = List::new(*data.iter().next().unwrap());

        for i in data.iter().skip(1) {
            l.insert(l.begin, *i);
        }

        l
    }

    unsafe fn insert(&mut self, pos: *mut Node, num: u32) -> *mut Node {
        let new = Node::new(num);

        if pos == self.begin {
            (*new).next = pos;
            (*pos).prev = new;

            self.begin = new;
            if pos == self.end {
                self.end = new;
            }
        } else if pos == self.end {
            (*self.end).next = new;

            self.end = new;
        } else {
            let old_prev = (*pos).prev;

            (*old_prev).next = new;
            (*new).prev = old_prev;

            (*pos).prev = new;
            (*new).next = pos;
        }

        new
    }

    unsafe fn remove(&mut self, pos: *mut Node) -> u32 {
        if pos == self.begin {
            self.begin = (*self.begin).next;
            (*self.begin).prev = std::ptr::null::<Node>() as *mut Node;
        } else {
            let old_prev = (*pos).prev;

            (*old_prev).next = (*pos).next;
            (*(*pos).next).prev = old_prev;
        }

        let num = (*pos).data;

        dealloc(pos as *mut u8, (*pos).layout);

        num
    }
}

impl fmt::Debug for List {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        unsafe {
            let mut l = &mut fmt.debug_list();

            let mut n = self.begin;

            while !n.is_null() {
                l = l.entry(&(*n));
                n = (*n).next;
            }

            l.finish()
        }
    }
}

struct Node {
    data: u32,
    next: *mut Node,
    prev: *mut Node,
    layout: Layout,
}

impl Node {
    unsafe fn new(num: u32) -> *mut Node {
        let layout = Layout::new::<Node>();
        let ptr = alloc(layout);

        *(ptr as *mut Node) = Node {
            layout: layout,
            data: num,
            next: std::ptr::null::<Node>() as *mut Node,
            prev: std::ptr::null::<Node>() as *mut Node,
        };

        ptr as *mut Node
    }
}

impl fmt::Debug for Node {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "{}", self.data)
    }
}
fn read(path: &str) -> io::Result<Vec<u32>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .chars()
                    .map(|x| x.to_digit(10).unwrap() as u32)
                    .collect::<Vec<u32>>()
            })
            .collect()
    })
}

fn pick_up(nums: &mut Vec<u32>, mut current_idx: usize) -> (usize, Vec<u32>) {
    let cups = (0..3)
        .map(|_| {
            let to_remove = (current_idx + 1) % nums.len();
            let num = nums.remove((current_idx + 1) % nums.len());
            if to_remove < current_idx {
                current_idx -= 1;
            }

            num
        })
        .collect::<Vec<_>>();

    (current_idx, cups)
}

fn dest(nums: &Vec<u32>, current_idx: usize) -> usize {
    let mut to_find = nums[current_idx] - 1;

    loop {
        if to_find == 0 {
            return nums.iter().enumerate().max_by_key(|&(_, n)| n).unwrap().0;
        }

        if let Some((i, _)) = nums.iter().enumerate().find(|(_, &x)| x == to_find) {
            return i;
        }

        to_find -= 1;
    }
}

fn ans(nums: &mut Vec<u32>, mut current_idx: usize, mut moves: usize) -> usize {
    loop {
        if moves == 0 {
            let i = nums.iter().enumerate().find(|&(_, x)| *x == 1).unwrap().0;

            let mut s = String::new();

            for i in (i + 1)..(i + 1 + nums.len() - 1) {
                s.push(std::char::from_digit(nums[i % nums.len()], 10).unwrap());
            }

            return s.parse::<usize>().unwrap();
        } else {
            let (mut cur_idx, picked_up) = pick_up(nums, current_idx);
            let mut dest_idx = dest(nums, cur_idx);

            picked_up.iter().for_each(|&x| {
                nums.insert(dest_idx + 1, x);

                if dest_idx < cur_idx {
                    cur_idx = (cur_idx + 1) % nums.len();
                }

                dest_idx += 1;
            });

            current_idx = (cur_idx + 1) % nums.len();
            moves -= 1;
        }
    }
}

fn ans_2(nums: &mut Vec<u32>, mut current_idx: usize, mut moves: usize) -> usize {
    let x = *nums.iter().max().unwrap() as usize;
    for i in (x + 1)..=1000000 {
        nums.push(i as u32);
    }

    while moves != 0 {
        let (mut cur_idx, picked_up) = pick_up(nums, current_idx);
        let mut dest_idx = dest(nums, cur_idx);

        picked_up.iter().for_each(|&x| {
            nums.insert(dest_idx + 1, x);

            if dest_idx < cur_idx {
                cur_idx = (cur_idx + 1) % nums.len();
            }

            dest_idx += 1;
        });

        current_idx = (cur_idx + 1) % nums.len();
        moves -= 1;
    }

    let i = nums.iter().enumerate().find(|&(_, x)| *x == 1).unwrap().0;

    nums[(i + 1) % nums.len()] as usize * nums[(i + 2) % nums.len()] as usize
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&mut foo.clone(), 0, 100), 47598263);
    //assert_eq!(ans_2(&mut foo.clone(), 0, 10000000), 4355897790573);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&mut foo.clone(), 0, 0), 25467389);

    assert_eq!(ans(&mut foo.clone(), 0, 100), 67384529);
    //assert_eq!(ans_2(&mut foo.clone(), 0, 10000000), 248009574232);
}

#[test]
fn test_pickup() {
    let mut nums = vec![3, 8, 9, 1, 2, 5, 4, 6, 7];

    assert_eq!(pick_up(&mut nums.clone(), 0), (0, vec![8, 9, 1]));
    assert_eq!(
        pick_up(&mut vec![3, 2, 8, 9, 1, 5, 4, 6, 7], 1),
        (1, vec![8, 9, 1])
    );

    assert_eq!(
        pick_up(&mut vec![3, 2, 5, 4, 6, 7, 8, 9, 1], 2),
        (2, vec![4, 6, 7])
    );

    assert_eq!(
        pick_up(&mut vec![7, 2, 5, 8, 4, 1, 9, 3, 6], 6),
        (5, vec![3, 6, 7])
    );

    nums = vec![9, 2, 5, 8, 4, 1, 3, 6, 7];
    assert_eq!(pick_up(&mut nums, 5), (5, vec![3, 6, 7]));
    assert_eq!(nums[5], 1);
    assert_eq!(nums, vec![9, 2, 5, 8, 4, 1]);
    assert_eq!(dest(&nums, 5), 0);

    nums = vec![8, 3, 6, 7, 4, 1, 9, 2, 5];
    assert_eq!(pick_up(&mut nums, 7), (5, vec![5, 8, 3]));
    assert_eq!(nums[5], 2);
    assert_eq!(nums, vec![6, 7, 4, 1, 9, 2]);
    assert_eq!(dest(&nums, 5), 3);

    nums = vec![8, 3, 6, 7, 4, 1, 9, 2, 5];
    assert_eq!(pick_up(&mut nums, 7), (5, vec![5, 8, 3]));
    assert_eq!(nums[5], 2);
    assert_eq!(nums, vec![6, 7, 4, 1, 9, 2]);
    assert_eq!(dest(&nums, 5), 3);

    nums = vec![7, 4, 1, 5, 8, 3, 9, 2, 6];
    assert_eq!(pick_up(&mut nums, 8), (5, vec![7, 4, 1]));
    assert_eq!(nums[5], 6);
    assert_eq!(nums, vec![5, 8, 3, 9, 2, 6]);
    assert_eq!(dest(&nums, 5), 0);
}

#[test]
fn test_list() {
    unsafe {
        let mut l = List::new(2);
        assert_eq!(format!("{:?}", l), "[2]");

        l.insert(l.begin, 3);

        assert_eq!(format!("{:?}", l), "[3, 2]");

        let i = l.insert((*l.begin).next, 4);

        assert_eq!(format!("{:?}", l), "[3, 4, 2]");

        assert_eq!(l.remove(i), 4);

        assert_eq!(format!("{:?}", l), "[3, 2]");
    }
}
