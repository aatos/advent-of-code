use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<(i64, Vec<i64>)> {
    let file = File::open(path)?;

    let lines: Vec<i64> = io::BufReader::new(file)
        .lines()
        .flat_map(|x| {
            x.map(|x| {
                x.split(",")
                    .map(|t| {
                        if t == "x" {
                            -1
                        } else {
                            t.parse::<i64>().unwrap()
                        }
                    })
                    .collect::<Vec<i64>>()
            })
            .unwrap()
        })
        .collect();

    Ok((lines[0], lines.iter().skip(1).map(|&x| x).collect()))
}

fn ans(timestamp: i64, times: &Vec<i64>) -> i64 {
    for t in timestamp.. {
        if let Some(bus) = times.iter().find(|&&b| b != -1 && t % b == 0) {
            return (t - timestamp) * bus;
        }
    }

    0
}

fn ans_2(times: &Vec<i64>, start: i64, incr: i64) -> u64 {
    let (max_idx, max) = times.iter().enumerate().max_by_key(|(_, x)| *x).unwrap();

    for i in start.. {
        let time = (max * incr * i) as u64;

        let maybe = ((time - (max_idx as u64))..((time - (max_idx as u64)) + times.len() as u64))
            .collect::<Vec<_>>();
        if maybe
            .iter()
            .zip(times)
            .all(|(&t, &b)| b == -1 || (t as u64) % (b as u64) == 0)
        {
            return time - max_idx as u64;
        }
    }

    0
}

fn main() {
    let (timestamp, times) = read("src/input").unwrap();

    println!("{}", ans(timestamp, &times));
    println!("{}", ans_2(&times, 11149257, 13 * 29 * 37)); // By manual analysis, the result contains these primes...
}

#[test]
fn test_input() {
    let (timestamp, times) = read("src/test").unwrap();

    assert_eq!(timestamp, 939);
    assert_eq!(times, vec![7, 13, -1, -1, 59, -1, 31, 19]);

    assert_eq!(ans(timestamp, &times), 295);

    assert_eq!(ans_2(&times, 1, 1), 1068781);
    assert_eq!(ans_2(&vec![17, -1, 13, 19], 1, 1), 3417);
    assert_eq!(ans_2(&vec![67, 7, 59, 61], 1, 1), 754018);
    assert_eq!(ans_2(&vec![67, -1, 7, 59, 61], 1, 1), 779210);
    assert_eq!(ans_2(&vec![67, 7, -1, 59, 61], 1, 1), 1261476);
    assert_eq!(ans_2(&vec![1789, 37, 47, 1889], 1, 1), 1202161486);
}
