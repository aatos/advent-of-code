use std::fs::File;
use std::io::{self, BufRead};

struct SeatKind {
    max: usize,
    low_char: char,
    high_char: char,
}

const ROWS: SeatKind = SeatKind {
    max: 127,
    low_char: 'F',
    high_char: 'B',
};

const COLS: SeatKind = SeatKind {
    max: 7,
    low_char: 'L',
    high_char: 'R',
};

fn read(path: &str) -> io::Result<Vec<String>> {
    let file = File::open(path)?;

    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap())
        .collect())
}

fn binary_search(seat: &str, kind: SeatKind) -> usize {
    seat.chars()
        .fold((0, kind.max), |(low, high), c| {
            if c == kind.low_char {
                (low, (high + low) / 2)
            } else if c == kind.high_char {
                ((high + low) / 2 + 1, high)
            } else {
                panic!("unexpected char: {}", c)
            }
        })
        .0
}

fn seat_id(seat: &String) -> usize {
    let row = binary_search(&seat[0..7], ROWS);
    let col = binary_search(&seat[7..10], COLS);

    row * 8 + col
}

fn ans(seats: &Vec<String>) -> usize {
    seats.iter().map(seat_id).max().unwrap()
}

fn ans_2(seats: &Vec<String>) -> usize {
    let mut ids: Vec<usize> = seats.iter().map(seat_id).collect();
    ids.sort();

    for (i, x) in ids.iter().take(ids.len() - 1).enumerate() {
        if x + 1 != ids[i + 1] {
            return x + 1;
        }
    }

    panic!("can't reach this")
}

fn main() {
    let seats = read("src/input").unwrap();

    println!("{}", ans(&seats)); // 826
    println!("{}", ans_2(&seats)); // 678
}

#[test]
fn test_input() {
    let seats = read("src/test").unwrap();
    assert_eq!(ans(&seats), 820);
}

#[test]
fn test_row() {
    assert_eq!(binary_search("FBFBBFF", ROWS), 44);
    assert_eq!(binary_search("FFFBBBF", ROWS), 14);
    assert_eq!(binary_search("BBFFBBF", ROWS), 102);
}

#[test]
fn test_col() {
    assert_eq!(binary_search("RLR", COLS), 5);
    assert_eq!(binary_search("RRR", COLS), 7);
    assert_eq!(binary_search("RLL", COLS), 4);
}
