use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<u64>> {
    let file = File::open(path)?;

    let mut v: Vec<u64> = io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap().parse::<u64>().unwrap())
        .collect();

    v.sort();
    // Starts with zero
    v.insert(0, 0);
    // And ends with +3
    v.push(v[v.len() - 1] + 3);

    Ok(v)
}

fn ans(jolts: &Vec<u64>) -> usize {
    let (ones, threes) = jolts
        .windows(2)
        .fold((0, 0), |(ones, threes), w| match w[1] - w[0] {
            1 => (ones + 1, threes),
            3 => (ones, threes + 1),
            _ => (ones, threes),
        });

    ones * threes
}

fn ans_2(jolts: &Vec<u64>) -> usize {
    jolts
        .iter()
        .enumerate()
        .skip(1)
        .fold((1, 0, jolts[0]), |(count, start, prev), (i, &x)| {
            if x != prev + 1 {
                let c = match i - start {
                    // How to generalize?
                    3 => count * 2,
                    4 => count * 4,
                    5 => count * 7,
                    _ => count,
                };
                (c, i, x)
            } else {
                (count, start, x)
            }
        })
        .0
}

fn main() {
    let jolts = read("src/input").unwrap();

    assert_eq!(ans(&jolts), 1856);
    assert_eq!(ans_2(&jolts), 2314037239808);
}

#[test]
fn test_input() {
    let jolts = read("src/test").unwrap();

    assert_eq!(ans(&jolts), 220);
    assert_eq!(ans_2(&jolts), 19208);
}

#[test]
fn test_input_2() {
    let jolts = read("src/test_1").unwrap();

    assert_eq!(ans(&jolts), 35);
    assert_eq!(ans_2(&jolts), 8);
}
