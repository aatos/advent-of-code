use std::cmp::max;
use std::collections::HashMap;
use std::fs::File;
use std::hash::{BuildHasher, Hasher};
use std::io::{self, BufRead};

// Hasher that should be collision-free for u64.
struct U64Hasher {
    h: u64,
}

impl Hasher for U64Hasher {
    fn finish(&self) -> u64 {
        self.h
    }

    fn write(&mut self, m: &[u8]) {
        self.h = ((m[0] as u64) << 0)
            | ((m[1] as u64) << 8)
            | ((m[2] as u64) << 16)
            | ((m[3] as u64) << 24)
            | ((m[4] as u64) << 32)
            | ((m[5] as u64) << 40)
            | ((m[6] as u64) << 48)
            | ((m[7] as u64) << 56)
    }
}

impl BuildHasher for U64Hasher {
    type Hasher = U64Hasher;

    fn build_hasher(&self) -> <Self as BuildHasher>::Hasher {
        U64Hasher { h: 0 }
    }
}

fn read(path: &str) -> io::Result<Vec<u64>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .split(",")
                    .map(|x| x.parse::<u64>().unwrap())
                    .collect::<Vec<u64>>()
            })
            .collect()
    })
}

// Default hashmap
fn ans_map(nums: &Vec<u64>, until: u64) -> u64 {
    let mut mem: HashMap<u64, u64> = nums
        .iter()
        .enumerate()
        .map(|(i, x)| (*x, (i + 1) as u64))
        .collect();

    mem.reserve(max(until, *nums.iter().max().unwrap()) as usize);

    let mut last = nums.last().copied().unwrap();

    for i in (nums.len() as u64 + 1)..=until {
        let first = mem.get(&last).copied();
        mem.insert(last, i - 1);

        last = if let Some(first) = first {
            i - first - 1
        } else {
            0
        }
    }

    last
}

// Own hasher
fn ans_own_map(nums: &Vec<u64>, until: u64) -> u64 {
    let mut mem: HashMap<u64, u64, U64Hasher> = HashMap::with_capacity_and_hasher(
        max(until, *nums.iter().max().unwrap()) as usize,
        U64Hasher { h: 0 },
    );

    for (i, x) in nums.iter().enumerate() {
        mem.insert(*x, (i + 1) as u64);
    }

    let mut last = nums.last().copied().unwrap();

    for i in (nums.len() as u64 + 1)..=until {
        let first = mem.get(&last).copied();
        mem.insert(last, i - 1);

        last = if let Some(first) = first {
            i - first - 1
        } else {
            0
        }
    }

    last
}

// Long enough vec
fn ans(nums: &Vec<u64>, until: usize) -> usize {
    let mut mem: Vec<usize> = vec![0; max(until, *nums.iter().max().unwrap() as usize)];

    for (i, x) in nums.iter().enumerate() {
        mem[*x as usize] = i + 1;
    }

    let mut last = nums.last().copied().unwrap() as usize;

    for i in (nums.len() + 1)..=until {
        let first = mem[last];
        mem[last] = i - 1;

        last = if first == 0 { 0 } else { i - first - 1 }
    }

    last
}

fn main() {
    let nums = read("src/input").unwrap();

    assert_eq!(ans(&nums, 2020), 253);
    assert_eq!(ans_map(&nums, 2020), 253);
    assert_eq!(ans_own_map(&nums, 2020), 253);

    assert_eq!(ans(&nums, 30000000), 13710); // ~1 second

    // assert_eq!(ans_map(&nums, 30000000), 13710); // ~5 seconds
    // assert_eq!(ans_own_map(&nums, 30000000), 13710); // ~2 seconds
}

#[test]
fn test_input() {
    let nums = read("src/test").unwrap();

    assert_eq!(ans(&nums, 2020), 436);
    assert_eq!(ans_map(&nums, 2020), 436);
    assert_eq!(ans_own_map(&nums, 2020), 436);
    assert_eq!(ans(&nums, 30000000), 175594);
}

#[test]
fn test_hasher() {
    let mut hasher = U64Hasher { h: 0 };

    hasher.write_u64(0);
    assert_eq!(hasher.finish(), 0);
    hasher.write_u64(1);
    assert_eq!(hasher.finish(), 1);
    hasher.write_u64(12342525235);
    assert_eq!(hasher.finish(), 12342525235);
    hasher.write_u64(5940040404);
    assert_eq!(hasher.finish(), 5940040404);
    hasher.write_u64(u64::MAX);
    assert_eq!(hasher.finish(), u64::MAX);
}
