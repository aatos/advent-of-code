use std::collections::HashMap;
use std::collections::HashSet;

use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<Vec<String>>> {
    let file = File::open(path)?;

    let mut groups: Vec<Vec<String>> = Vec::new();
    let mut group: Vec<String> = Vec::new();

    for l in io::BufReader::new(file).lines().map(|x| x.unwrap()) {
        if l.is_empty() {
            groups.push(group);
            group = Vec::new();
        } else {
            group.push(l);
        }
    }

    if !group.is_empty() {
        groups.push(group);
    }

    Ok(groups)
}

fn handle_group(group: &Vec<String>) -> usize {
    let mut answers: HashSet<char> = HashSet::new();

    group.iter().flat_map(|x| x.chars()).for_each(|c| {
        answers.insert(c);
    });

    answers.len()
}

fn ans(groups: &Vec<Vec<String>>) -> usize {
    groups.iter().map(handle_group).sum()
}

fn handle_group_2(group: &Vec<String>) -> usize {
    let mut answers: HashMap<char, usize> = HashMap::new();

    group.iter().flat_map(|x| x.chars()).for_each(|c| {
        let a = answers.entry(c).or_insert(0);
        *a += 1;
    });

    answers.iter().filter(|&(_, a)| *a == group.len()).count()
}

fn ans_2(groups: &Vec<Vec<String>>) -> usize {
    groups.iter().map(handle_group_2).sum()
}

fn main() {
    let groups = read("src/input").unwrap();

    println!("{}", ans(&groups)); // 6735
    println!("{}", ans_2(&groups)); // 3221
}

#[test]
fn test_input() {
    let groups = read("src/test").unwrap();

    assert_eq!(ans(&groups), 11);
    assert_eq!(ans_2(&groups), 6);
}
