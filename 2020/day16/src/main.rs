use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::ops::Range;

struct TicketRange {
    name: String,
    first: Range<u64>,
    second: Range<u64>,
}

struct TicketData {
    ranges: Vec<TicketRange>,
    ticket: Vec<u64>,
    others: Vec<Vec<u64>>,
}

fn read(path: &str) -> io::Result<TicketData> {
    let mut data = TicketData {
        ranges: vec![],
        ticket: vec![],
        others: vec![],
    };

    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .for_each(|x| parse(&x.unwrap(), &mut data))
    })?;

    Ok(data)
}

fn parse(line: &str, data: &mut TicketData) {
    if line.contains(": ") {
        for x in line.split(": ").skip(1) {
            let ranges = x
                .split(" or ")
                .map(|x| {
                    let nums = x
                        .split("-")
                        .map(|n| n.parse::<u64>().unwrap())
                        .collect::<Vec<u64>>();

                    Range {
                        start: nums[0],
                        end: nums[1] + 1,
                    }
                })
                .collect::<Vec<Range<u64>>>();

            data.ranges.push(TicketRange {
                name: line.split(": ").next().unwrap().to_string(),
                first: ranges[0].clone(),
                second: ranges[1].clone(),
            });
        }
    } else if line.contains(",") {
        let ticket = line
            .split(",")
            .map(|x| x.parse::<u64>().unwrap())
            .collect::<Vec<u64>>();
        if data.ticket.is_empty() {
            data.ticket = ticket;
        } else {
            data.others.push(ticket);
        }
    }
}

fn ans(data: &TicketData) -> u64 {
    data.others
        .iter()
        .flat_map(|t| {
            t.iter()
                .filter(|num| {
                    data.ranges
                        .iter()
                        .all(|range| !range.first.contains(num) && !range.second.contains(num))
                })
                .cloned()
                .collect::<Vec<u64>>()
        })
        .sum()
}

fn ans_2(data: &TicketData, field_name: &str) -> u64 {
    let valid_tickets: Vec<Vec<u64>> = data
        .others
        .iter()
        .filter(|t| {
            t.iter().all(|num| {
                data.ranges
                    .iter()
                    .any(|range| range.first.contains(num) || range.second.contains(num))
            })
        })
        .cloned()
        .collect();

    // ticket value index -> (range index, occurrences of said range index)
    let mut range_maps: HashMap<usize, HashMap<usize, usize>> = HashMap::new();

    for ticket in &valid_tickets {
        for (ticket_idx, num) in ticket.iter().enumerate() {
            for (range_idx, _) in data
                .ranges
                .iter()
                .enumerate()
                .filter(|(_, range)| range.first.contains(num) || range.second.contains(num))
            {
                *range_maps
                    .entry(ticket_idx)
                    .or_default()
                    .entry(range_idx)
                    .or_default() += 1;
            }
        }
    }

    let mut range_to_ticket_idx: HashMap<usize, usize> = HashMap::new();

    while !range_maps.is_empty() {
        range_maps = range_maps
            .into_iter()
            .filter(|(ticket_value_idx, occurrences)| {
                let range_idxs = occurrences
                    .iter()
                    .filter(|(range_idx, count)| {
                        **count == valid_tickets.len()
                            && !range_to_ticket_idx.contains_key(range_idx)
                    })
                    .map(|(&range_idx, _)| range_idx)
                    .collect::<Vec<usize>>();

                if range_idxs.len() == 1 {
                    range_to_ticket_idx.insert(range_idxs[0], *ticket_value_idx);
                    false
                } else {
                    true
                }
            })
            .collect();
    }

    data.ranges
        .iter()
        .enumerate()
        .filter(|(_, range)| range.name.starts_with(field_name))
        .map(|(i, _)| data.ticket[*range_to_ticket_idx.get(&i).unwrap()])
        .product()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(ans(&data), 25972);
    assert_eq!(ans_2(&data, "departure"), 622670335901);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(ans(&data), 71);
}

#[test]
fn test_input_2() {
    let data = read("src/test_2").unwrap();

    assert_eq!(ans_2(&data, "class"), 12);
    assert_eq!(ans_2(&data, "row"), 11);
    assert_eq!(ans_2(&data, "seat"), 13);
}
