use std::fs::File;
use std::io::{self, BufRead};

enum MoveKind {
    Forward(i64),
    North(i64),
    South(i64),
    East(i64),
    West(i64),
    Right(i64),
    Left(i64),
}

impl MoveKind {
    fn new(s: &str) -> MoveKind {
        let t = s.chars().nth(0).unwrap();
        let n = s[1..].parse::<usize>().unwrap() as i64;
        match t {
            'N' => MoveKind::North(n),
            'S' => MoveKind::South(n),
            'E' => MoveKind::East(n),
            'W' => MoveKind::West(n),
            'L' => MoveKind::Left(n),
            'R' => MoveKind::Right(n),
            'F' => MoveKind::Forward(n),
            _ => panic!("invalid line: {}", s),
        }
    }
}

fn read(path: &str) -> io::Result<Vec<MoveKind>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| MoveKind::new(&x.unwrap()))
            .collect()
    })
}

fn ans(moves: &Vec<MoveKind>) -> u64 {
    let (_, east, north) = moves
        .iter()
        .fold((0, 0, 0), |(dir, east, north), mov| match mov {
            MoveKind::North(c) => (dir, east, north + *c),
            MoveKind::South(c) => (dir, east, north - *c),
            MoveKind::East(c) => (dir, east + *c, north),
            MoveKind::West(c) => (dir, east - *c, north),
            MoveKind::Left(c) => ((dir + -*c) % 360, east, north),
            MoveKind::Right(c) => ((dir + *c) % 360, east, north),
            MoveKind::Forward(deg) => match dir {
                270 | -90 => (dir, east, north + *deg),
                90 | -270 => (dir, east, north - *deg),
                0 => (dir, east + *deg, north),
                180 | -180 => (dir, east - *deg, north),
                _ => panic!("invalid direction: {}", dir),
            },
        });

    (east.abs() + north.abs()) as u64
}

fn ans_2(moves: &Vec<MoveKind>) -> u64 {
    fn rotate(we: i64, wn: i64, degrees: i64) -> (i64, i64) {
        match degrees {
            270 | -90 => (wn * -1, we),
            180 | -180 => (we * -1, wn * -1),
            90 | -270 => (wn, we * -1),
            0 => (wn, we),
            _ => panic!("invalid rotate: {}", degrees),
        }
    }

    let (east, north, _, _) = moves.iter().fold(
        (0, 0, 10, 1),
        |(east, north, way_east, way_north), mov| match mov {
            MoveKind::North(c) => (east, north, way_east, way_north + *c),
            MoveKind::South(c) => (east, north, way_east, way_north - *c),
            MoveKind::East(c) => (east, north, way_east + *c, way_north),
            MoveKind::West(c) => (east, north, way_east - *c, way_north),
            MoveKind::Left(deg) => {
                let (way_east, way_north) = rotate(way_east, way_north, -*deg);
                (east, north, way_east, way_north)
            }
            MoveKind::Right(deg) => {
                let (way_east, way_north) = rotate(way_east, way_north, *deg);
                (east, north, way_east, way_north)
            }
            MoveKind::Forward(c) => (
                east + way_east * *c,
                north + way_north * *c,
                way_east,
                way_north,
            ),
        },
    );

    (east.abs() + north.abs()) as u64
}

fn main() {
    let moves = read("src/input").unwrap();

    assert_eq!(ans(&moves), 1319);
    assert_eq!(ans_2(&moves), 62434);
}

#[test]
fn test_input() {
    let moves = read("src/test").unwrap();

    assert_eq!(ans(&moves), 25);
    assert_eq!(ans_2(&moves), 286);
}

#[test]
fn test_input_2() {
    let moves = read("src/test_2").unwrap();

    assert_eq!(ans(&moves), 31);
}
