use std::collections::HashSet;

use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone)]
enum OperationKind {
    Jmp(i32),
    Acc(i32),
    Nop(i32),
}

fn read(path: &str) -> io::Result<Vec<OperationKind>> {
    let file = File::open(path)?;

    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| parse(&x.unwrap()))
        .collect())
}

fn parse(line: &str) -> OperationKind {
    let p: Vec<&str> = line.split(" ").collect();
    let operand = p[1]
        .parse::<i32>()
        .expect(&format!("failed to parse {}", line));

    match p[0] {
        "jmp" => OperationKind::Jmp(operand),
        "nop" => OperationKind::Nop(operand),
        "acc" => OperationKind::Acc(operand),
        _ => panic!("invalid line: {}", line),
    }
}

fn ans(code: &Vec<OperationKind>) -> Result<i32, i32> {
    let mut history: HashSet<usize> = HashSet::new();
    let mut line: i32 = 0;
    let mut acc: i32 = 0;

    while !history.contains(&(line as usize)) && (line as usize) < code.len() {
        history.insert(line as usize);

        line += match code[line as usize] {
            OperationKind::Jmp(i) => i,
            OperationKind::Acc(i) => {
                acc += i;
                1
            }
            OperationKind::Nop(_) => 1,
        }
    }

    if (line as usize) == code.len() {
        Ok(acc)
    } else {
        Err(acc)
    }
}

fn ans_2(code: &Vec<OperationKind>) -> Option<i32> {
    for (i, c) in code.iter().enumerate() {
        let new_line = match c {
            OperationKind::Jmp(i) => OperationKind::Nop(*i),
            OperationKind::Nop(i) => OperationKind::Jmp(*i),
            _ => continue,
        };

        let mut new_code = code.to_vec();
        new_code[i] = new_line;
        if let Ok(acc) = ans(&new_code) {
            return Some(acc);
        }
    }

    None
}

fn main() {
    let code = read("src/input").unwrap();

    println!("{}", ans(&code).unwrap_err()); // 1487
    println!("{}", ans_2(&code).unwrap()); // 1607
}
