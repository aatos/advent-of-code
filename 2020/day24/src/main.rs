use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Debug)]
enum Dir {
    East,
    Southeast,
    Southwest,
    West,
    Northwest,
    Northeast,
}

fn read(path: &str) -> io::Result<Vec<Vec<Dir>>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let mut v = vec![];
                let mut iter = x.chars();
                while let Some(c) = iter.next() {
                    match c {
                        'e' => v.push(Dir::East),
                        'w' => v.push(Dir::West),
                        's' => match iter.next() {
                            Some('w') => v.push(Dir::Southwest),
                            Some('e') => v.push(Dir::Southeast),
                            _ => panic!("invalid direction"),
                        },
                        'n' => match iter.next() {
                            Some('w') => v.push(Dir::Northwest),
                            Some('e') => v.push(Dir::Northeast),
                            _ => panic!("invalid direction"),
                        },
                        _ => panic!("invalid direction"),
                    }
                }
                v
            })
            .collect()
    })
}

fn tiles_to_coords(lines: &Vec<Vec<Dir>>) -> HashMap<(u32, u32), usize> {
    let mut h: HashMap<(u32, u32), usize> = HashMap::new();

    lines
        .iter()
        .map(|l| {
            l.iter().fold((500, 500), |(x, y), dir| match dir {
                Dir::East => (x, y + 1),
                Dir::Southeast => (x + 1, y),
                Dir::Southwest => (x + 1, y - 1),
                Dir::West => (x, y - 1),
                Dir::Northwest => (x - 1, y),
                Dir::Northeast => (x - 1, y + 1),
            })
        })
        .for_each(|p| {
            let c = h.entry(p).or_insert(0);
            *c += 1;
        });

    h
}

fn ans(lines: &Vec<Vec<Dir>>) -> usize {
    let h = tiles_to_coords(lines);

    h.iter().filter(|(_, &c)| c % 2 != 0).count()
}

fn count_neighbors(coords: &Vec<Vec<bool>>, x: usize, y: usize) -> usize {
    let min_x = cmp::max(x as i32 * -1, -1);
    let max_x = cmp::min(coords.len() as i32 - x as i32, 2);

    let min_y = cmp::max(y as i32 * -1, -1);
    let max_y = cmp::min(coords[x as usize].len() as i32 - y as i32, 2);

    (min_x..max_x)
        .flat_map(move |i| {
            (min_y..max_y).filter(move |&j| {
                // (i != 1 || j != 1)
                //     && (i != -1 || j != -1)
                //     && (i != 0 || j != 0)
                ((i == 0 && j == 1)
                    || (i == 1 && j == 0)
                    || (i == 1 && j == -1)
                    || (i == 0 && j == -1)
                    || (i == -1 && j == 0)
                    || (i == -1 && j == 1))
                    && coords[(x as i32 + i) as usize][(y as i32 + j) as usize]
            })
        })
        .count()
}

fn game_of_life(coords: &Vec<Vec<bool>>) -> Vec<Vec<bool>> {
    (0..coords.len())
        .map(|x| {
            (0..coords[x].len())
                .map(|y| match (coords[x][y], count_neighbors(coords, x, y)) {
                    (true, x) if x > 2 || x == 0 => false,
                    (false, 2) => true,
                    (t, _) => t,
                })
                .collect::<Vec<bool>>()
        })
        .collect::<Vec<Vec<bool>>>()
}

fn count_blacks(coords: &Vec<Vec<bool>>) -> usize {
    coords.iter().fold(0, |acc, xx| {
        acc + xx.iter().fold(0, |acc, &xy| if xy { acc + 1 } else { acc })
    })
}

fn ans_2(lines: &Vec<Vec<Dir>>) -> usize {
    let h = tiles_to_coords(lines);

    // TODO: fix this
    let mut coords = vec![vec![false; 1000]; 1000];

    h.iter().for_each(|(&(x, y), &c)| {
        coords[x as usize][y as usize] = c % 2 != 0;
    });

    for _ in 0..100 {
        coords = game_of_life(&coords);
    }

    count_blacks(&coords)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo), 254);
    assert_eq!(ans_2(&foo), 3697);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&foo), 10);
    //    assert_eq!(ans_2(&foo), 2208);
}

#[test]
fn test_parts() {
    assert_eq!(
        ans(&vec![
            vec![
                Dir::Northwest,
                Dir::West,
                Dir::Southwest,
                Dir::East,
                Dir::East
            ],
            vec![Dir::Northeast, Dir::Southeast, Dir::West]
        ]),
        0
    );

    assert_eq!(
        ans(&vec![
            vec![Dir::East, Dir::Southeast, Dir::West],
            vec![Dir::Southeast]
        ]),
        0
    );

    assert_eq!(
        ans(&vec![
            vec![Dir::Northwest, Dir::East, Dir::Southwest],
            vec![Dir::East, Dir::West]
        ]),
        0
    );
}
