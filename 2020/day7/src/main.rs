#[macro_use]
extern crate lazy_static;
use regex::Regex;

use std::collections::HashMap;

use std::fs::File;
use std::io::{self, BufRead};

const MY_BAG: &str = "shiny gold";

fn read(path: &str) -> io::Result<HashMap<String, Vec<(usize, String)>>> {
    let file = File::open(path)?;

    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| parse(&x.unwrap()))
        .collect())
}

fn parse(line: &str) -> (String, Vec<(usize, String)>) {
    lazy_static! {
        // Works only with max four inner bags.
        static ref RE: Regex = Regex::new(
            &(r"^(\w+ \w+) bags contain".to_string() +
              r"(?:" +
              r"(?: (\d+) (\w+ \w+) bags*[,.]?" +
              r"(?: (\d+) (\w+ \w+) bags?[,.]?)?" +
              r"(?: (\d+) (\w+ \w+) bags?[,.]?)?" +
              r"(?: (\d+) (\w+ \w+) bags?[,.]?)?" +
              ")" +
              r"|(?: no other bags.))$")).unwrap();
    }

    let captures = RE
        .captures(line)
        .expect(&format!("failed to parse {}", line));

    (
        captures.get(1).unwrap().as_str().to_string(),
        captures
            .iter()
            .skip(2)
            .filter_map(|s| s.map(|e| e.as_str().to_string()))
            .collect::<Vec<String>>()
            .chunks(2)
            .map(|v| {
                (
                    v[0].parse::<usize>()
                        .expect(&format!("failed to parse {:?}", v)),
                    v[1].to_string(),
                )
            })
            .collect(),
    )
}

fn check_bag(bags: &HashMap<String, Vec<(usize, String)>>, bag: &Vec<(usize, String)>) -> bool {
    bag.iter()
        .any(|(_, b)| b == MY_BAG || bags.get(b).map(|bs| check_bag(bags, bs)).unwrap_or(false))
}

fn ans(bags: &HashMap<String, Vec<(usize, String)>>) -> usize {
    bags.values().filter(|b| check_bag(bags, b)).count()
}

fn ans_2(bags: &HashMap<String, Vec<(usize, String)>>, bag: &str) -> usize {
    bags.get(bag)
        .unwrap()
        .iter()
        .fold(0, |total, (c, b)| total + c + c * ans_2(bags, b))
}

fn main() {
    let bags = read("src/input").unwrap();

    println!("{}", ans(&bags)); // 211
    println!("{}", ans_2(&bags, MY_BAG)); // 12414
}

#[test]
fn test_input() {
    let bags = read("src/test").unwrap();

    assert_eq!(
        *bags.get("light red").unwrap(),
        vec![
            (1, "bright white".to_string()),
            (2, "muted yellow".to_string()),
        ],
    );
    assert_eq!(
        *bags.get("dark orange").unwrap(),
        vec![
            (3, "bright white".to_string()),
            (4, "muted yellow".to_string()),
        ],
    );
    assert_eq!(
        *bags.get("bright white").unwrap(),
        vec![(1, "shiny gold".to_string())],
    );
    assert_eq!(
        *bags.get("muted yellow").unwrap(),
        vec![(2, "shiny gold".to_string()), (9, "faded blue".to_string())],
    );
    assert_eq!(
        *bags.get("shiny gold").unwrap(),
        vec![
            (1, "dark olive".to_string()),
            (2, "vibrant plum".to_string()),
        ],
    );
    assert_eq!(
        *bags.get("dark olive").unwrap(),
        vec![
            (3, "faded blue".to_string()),
            (4, "dotted black".to_string()),
        ],
    );
    assert_eq!(
        *bags.get("vibrant plum").unwrap(),
        vec![
            (5, "faded blue".to_string()),
            (6, "dotted black".to_string()),
        ],
    );
    assert_eq!(*bags.get("faded blue").unwrap(), vec![]);
    assert_eq!(*bags.get("dotted black").unwrap(), vec![]);

    assert_eq!(bags.len(), 9);

    assert_eq!(ans(&bags), 4);
    assert_eq!(ans_2(&bags, MY_BAG), 32);
}

#[test]
fn test_input_2() {
    let bags = read("src/test_2").unwrap();
    assert_eq!(ans_2(&bags, MY_BAG), 126);
}
