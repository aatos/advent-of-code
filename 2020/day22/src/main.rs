use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<(VecDeque<u64>, VecDeque<u64>)> {
    File::open(path).map(|file| {
        let mut player1 = VecDeque::new();
        let mut player2 = VecDeque::new();

        let mut push_to = &mut player1;
        for l in io::BufReader::new(file).lines().map(|x| x.unwrap()) {
            if l.ends_with(":") {
                continue;
            }

            if l.is_empty() {
                push_to = &mut player2;
                continue;
            }

            let num = l.parse::<u64>().unwrap();

            push_to.push_back(num);
        }

        (player1, player2)
    })
}

fn calculate_score(p: &VecDeque<u64>) -> usize {
    p.iter()
        .enumerate()
        .fold(0, |acc, (i, num)| acc + (*num as usize) * (p.len() - i))
}

fn ans(p1: &mut VecDeque<u64>, p2: &mut VecDeque<u64>) -> usize {
    if p1.is_empty() {
        calculate_score(p2)
    } else if p2.is_empty() {
        calculate_score(p1)
    } else {
        let f = p1.pop_front().unwrap();
        let s = p2.pop_front().unwrap();

        if f > s {
            p1.push_back(f);
            p1.push_back(s);
        } else {
            p2.push_back(s);
            p2.push_back(f);
        }

        ans(p1, p2)
    }
}

fn ans_2(
    p1: &mut VecDeque<u64>,
    p2: &mut VecDeque<u64>,
    prev: &mut Vec<VecDeque<u64>>,
) -> (usize, bool) {
    if prev.contains(p1) || prev.contains(p2) {
        (calculate_score(p1), true)
    } else if p1.is_empty() {
        (calculate_score(p2), false)
    } else if p2.is_empty() {
        (calculate_score(p1), true)
    } else {
        prev.push(p1.clone());
        prev.push(p2.clone());

        let f = p1.pop_front().unwrap();
        let s = p2.pop_front().unwrap();

        let p1_won = if f as usize > p1.len() || s as usize > p2.len() {
            f > s
        } else {
            let mut new_p1 = p1.iter().cloned().take(f as usize).collect();
            let mut new_p2 = p2.iter().cloned().take(s as usize).collect();

            ans_2(&mut new_p1, &mut new_p2, &mut vec![]).1
        };

        if p1_won {
            p1.push_back(f);
            p1.push_back(s);
        } else {
            p2.push_back(s);
            p2.push_back(f);
        }

        ans_2(p1, p2, prev)
    }
}

fn main() {
    let (p1, p2) = read("src/input").unwrap();

    assert_eq!(ans(&mut p1.clone(), &mut p2.clone()), 30138);
    assert_eq!(
        ans_2(&mut p1.clone(), &mut p2.clone(), &mut vec![]),
        (31587, true)
    );
}

#[test]
fn test_input() {
    let (p1, p2) = read("src/test").unwrap();

    assert_eq!(ans(&mut p1.clone(), &mut p2.clone()), 306);
    assert_eq!(
        ans_2(&mut p1.clone(), &mut p2.clone(), &mut vec![]),
        (291, false)
    );
}

#[test]
fn test_input_loop() {
    let (p1, p2) = read("src/loop").unwrap();

    assert_eq!(
        ans_2(&mut p1.clone(), &mut p2.clone(), &mut vec![]),
        (105, true)
    );
}
