use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

enum Operation {
    Memset(u64, u64),
    Bitmask(Vec<(usize, i32)>),
}

fn read(path: &str) -> io::Result<Vec<Operation>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| parse(&x.unwrap()))
            .collect()
    })
}

fn parse(line: &str) -> Operation {
    if line.starts_with("mem[") {
        let x: Vec<&str> = line.split("] = ").collect();
        Operation::Memset(
            x[0].strip_prefix("mem[").unwrap().parse::<u64>().unwrap(),
            x[1].parse::<u64>().unwrap(),
        )
    } else {
        Operation::Bitmask(
            line.strip_prefix("mask = ")
                .unwrap()
                .chars()
                .enumerate()
                .map(|(i, x)| {
                    (
                        35 - i,
                        if x == 'X' {
                            -1
                        } else {
                            x.to_digit(2).unwrap() as i32
                        },
                    )
                })
                .collect(),
        )
    }
}

fn bitset(val: u64, i: usize, set: bool) -> u64 {
    if set {
        val | (1 << i)
    } else {
        val & !(1 << i)
    }
}

fn ans(operations: &Vec<Operation>) -> u64 {
    let mut memory: HashMap<u64, u64> = HashMap::new();

    let mut bitmask: Vec<(usize, i32)> = vec![];

    for oper in operations {
        match oper {
            Operation::Memset(mem, val) => {
                memory.insert(
                    *mem,
                    bitmask
                        .iter()
                        .filter(|(_, b)| *b != -1)
                        .fold(*val, |v, &(i, b)| bitset(v, i, b == 1)),
                );
            }
            Operation::Bitmask(mask) => bitmask = mask.to_vec(),
        }
    }

    memory.iter().fold(0, |acc, (_, v)| acc + v)
}

fn ans_2(operations: &Vec<Operation>) -> u64 {
    let mut memory: HashMap<u64, u64> = HashMap::new();

    let mut bitmask: Vec<(usize, i32)> = vec![];

    for oper in operations {
        match oper {
            Operation::Memset(mem, val) => {
                let base_address = bitmask
                    .iter()
                    .filter(|(_, b)| *b == 1)
                    .fold(*mem, |v, &(i, _)| bitset(v, i, true));

                bitmask
                    .iter()
                    .filter(|(_, b)| *b == -1)
                    .fold(vec![base_address], |v, (i, _)| {
                        v.iter()
                            .flat_map(|&mem| vec![bitset(mem, *i, true), bitset(mem, *i, false)])
                            .collect()
                    })
                    .into_iter()
                    .collect::<HashSet<u64>>()
                    .iter()
                    .for_each(|&mem| {
                        memory.insert(mem, *val);
                    });
            }
            Operation::Bitmask(mask) => bitmask = mask.to_vec(),
        }
    }

    memory.iter().fold(0, |acc, (_, v)| acc + v)
}

fn main() {
    let operations = read("src/input").unwrap();

    assert_eq!(ans(&operations), 9967721333886);
    assert_eq!(ans_2(&operations), 4355897790573);
}

#[test]
fn test_input() {
    let operations = read("src/test").unwrap();

    assert_eq!(ans(&operations), 165);
}

#[test]
fn test_input_2() {
    let operations = read("src/test_2").unwrap();

    assert_eq!(ans_2(&operations), 208);
}
