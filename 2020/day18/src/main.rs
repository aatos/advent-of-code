use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::Peekable;

fn read(path: &str) -> io::Result<Vec<Vec<char>>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .filter(|&x| x != ' ')
                    .collect::<Vec<char>>()
            })
            .collect()
    })
}

struct Parser<'a> {
    iter: Peekable<std::slice::Iter<'a, char>>,
    level_1: HashSet<char>,
    level_2: HashSet<char>,
}

impl<'a> Parser<'a> {
    pub fn process(&mut self) -> i64 {
        let res = self.parse_level_1();

        match self.iter.next() {
            None => res,
            t => panic!("Expected EOF, found {:?}", t),
        }
    }

    fn calculate(lval: i64, oper: Option<&char>, rval: i64) -> i64 {
        match oper {
            Some('+') => lval + rval,
            Some('-') => lval - rval,
            Some('*') => lval * rval,
            Some('/') => {
                if rval == 0 {
                    panic!("Cannot divide by zero");
                }
                lval / rval
            }
            _ => panic!("This shouldn't be possible"),
        }
    }

    fn parse_level_1(&mut self) -> i64 {
        let mut level_2 = self.parse_level_2();

        match self.iter.peek() {
            Some(c) if self.level_1.contains(&c) => {
                while self.level_1.contains(self.iter.peek().unwrap_or(&&'x')) {
                    let oper = self.iter.next();
                    let rval = self.parse_level_2();

                    level_2 = Parser::calculate(level_2, oper, rval)
                }
                level_2
            }

            _ => level_2,
        }
    }

    fn parse_level_2(&mut self) -> i64 {
        let mut term = self.parse_term();

        match self.iter.peek() {
            Some(c) if self.level_2.contains(&c) => {
                while self.level_2.contains(self.iter.peek().unwrap_or(&&'x')) {
                    let oper = self.iter.next();
                    let rval = self.parse_term();

                    term = Parser::calculate(term, oper, rval)
                }
                term
            }

            _ => term,
        }
    }

    fn parse_term(&mut self) -> i64 {
        match self.iter.next() {
            Some(i) if ('0'..='9').contains(&i) => i.to_digit(10).unwrap() as i64,
            Some('(') => {
                let res = self.parse_level_1();

                match self.iter.next() {
                    Some(')') => res,
                    t => panic!("Expected ')', found {:?}", t),
                }
            }

            t => panic!("Unexpected term: {:?}", t),
        }
    }
}

fn ans(exprs: &Vec<Vec<char>>) -> i64 {
    exprs
        .iter()
        .map(|expr| {
            Parser {
                iter: expr.iter().peekable(),
                level_1: ['*', '/', '+', '-'].into_iter().collect(),
                level_2: HashSet::new(),
            }
            .process()
        })
        .sum()
}

fn ans_2(exprs: &Vec<Vec<char>>) -> i64 {
    exprs
        .iter()
        .map(|expr| {
            Parser {
                iter: expr.iter().peekable(),
                level_1: ['*', '/'].into_iter().collect(),
                level_2: ['+', '-'].into_iter().collect(),
            }
            .process()
        })
        .sum()
}

fn main() {
    let exprs = read("src/input").unwrap();

    assert_eq!(ans(&exprs), 30753705453324);
    assert_eq!(ans_2(&exprs), 244817530095503);
}

#[test]
fn test_input() {
    let exprs = read("src/test").unwrap();

    assert_eq!(ans(&exprs), 71);
    assert_eq!(ans_2(&exprs), 231);
}
