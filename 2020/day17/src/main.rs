use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<Vec<Vec<char>>>> {
    File::open(path).map(|file| {
        vec![io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect::<Vec<char>>())
            .collect()]
    })
}

#[allow(dead_code)]
fn print_state(state: &Vec<Vec<Vec<char>>>) {
    for (i, z) in state.iter().enumerate() {
        println!("z={}", i);

        for x in z {
            println!("{:?}", x);
        }

        println!();
    }
}

#[allow(dead_code)]
fn print_state_2(state: &Vec<Vec<Vec<Vec<char>>>>) {
    for (h, w) in state.iter().enumerate() {
        for (i, z) in w.iter().enumerate() {
            println!("z={}, w={}", i, h);

            for x in z {
                println!("{:?}", x);
            }

            println!();
        }
    }
}

fn count_neighbors(state: &Vec<Vec<Vec<char>>>, z: i64, x: i64, y: i64) -> usize {
    let min_z = cmp::max(z * -1, -1);
    let max_z = cmp::min(state.len() as i64 - z, 2);

    let min_x = cmp::max(x * -1, -1);
    let max_x = cmp::min(state[z as usize].len() as i64 - x, 2);

    let min_y = cmp::max(y * -1, -1);
    let max_y = cmp::min(state[z as usize][x as usize].len() as i64 - y, 2);

    (min_z..max_z)
        .flat_map(|i| {
            (min_x..max_x).flat_map(move |j| {
                (min_y..max_y).filter(move |&k| {
                    (i != 0 || j != 0 || k != 0)
                        && state[(z + i) as usize][(x + j) as usize][(y + k) as usize] == '#'
                })
            })
        })
        .count()
}

fn count_neighbors_2(state: &Vec<Vec<Vec<Vec<char>>>>, w: i64, z: i64, x: i64, y: i64) -> usize {
    let min_w = cmp::max(w * -1, -1);
    let max_w = cmp::min(state.len() as i64 - w, 2);

    let min_z = cmp::max(z * -1, -1);
    let max_z = cmp::min(state[w as usize].len() as i64 - z, 2);

    let min_x = cmp::max(x * -1, -1);
    let max_x = cmp::min(state[w as usize][z as usize].len() as i64 - x, 2);

    let min_y = cmp::max(y * -1, -1);
    let max_y = cmp::min(
        state[w as usize][z as usize][x as usize].len() as i64 - y,
        2,
    );

    (min_w..max_w)
        .flat_map(|h| {
            (min_z..max_z).flat_map(move |i| {
                (min_x..max_x).flat_map(move |j| {
                    (min_y..max_y).filter(move |&k| {
                        (h != 0 || i != 0 || j != 0 || k != 0)
                            && state[(w + h) as usize][(z + i) as usize][(x + j) as usize]
                                [(y + k) as usize]
                                == '#'
                    })
                })
            })
        })
        .count()
}

fn expand(state: &Vec<Vec<Vec<char>>>) -> Vec<Vec<Vec<char>>> {
    let mut state = state.to_vec();

    let new_len = state[0].len() + 2;

    for z in 0..state.len() {
        for x in 0..state[z].len() {
            state[z][x].insert(0, '.');
            state[z][x].push('.');
        }

        state[z].insert(0, vec!['.'; new_len]);
        state[z].push(vec!['.'; new_len]);
    }

    state.insert(0, vec![vec!['.'; new_len]; new_len]);
    state.push(vec![vec!['.'; new_len]; new_len]);

    state
}

fn expand_2(state: &Vec<Vec<Vec<Vec<char>>>>) -> Vec<Vec<Vec<Vec<char>>>> {
    let mut state = state.to_vec();

    let new_len = state[0][0].len() + 2;
    let new_z_len = state[0].len() + 2;

    for w in 0..state.len() {
        for z in 0..state[w].len() {
            for x in 0..state[w][z].len() {
                state[w][z][x].insert(0, '.');
                state[w][z][x].push('.');
            }

            state[w][z].insert(0, vec!['.'; new_len]);
            state[w][z].push(vec!['.'; new_len]);
        }

        state[w].insert(0, vec![vec!['.'; new_len]; new_len]);
        state[w].push(vec![vec!['.'; new_len]; new_len]);
    }

    state.insert(0, vec![vec![vec!['.'; new_len]; new_len]; new_z_len]);
    state.push(vec![vec![vec!['.'; new_len]; new_len]; new_z_len]);

    state
}

fn ans(state: &Vec<Vec<Vec<char>>>, cycles: usize) -> usize {
    if cycles == 0 {
        return state.iter().fold(0, |acc, z| {
            acc + z
                .iter()
                .fold(0, |acc, x| acc + x.iter().filter(|&y| *y == '#').count())
        });
    }

    let state = expand(state);

    let mut new_state = state.to_vec();

    for z in 0..state.len() {
        for x in 0..state[z].len() {
            for y in 0..state[z][x].len() {
                new_state[z][x][y] = match (
                    state[z][x][y],
                    count_neighbors(&state, z as i64, x as i64, y as i64),
                ) {
                    ('#', 2) | ('#', 3) => '#',
                    ('.', 3) => '#',
                    _ => '.',
                }
            }
        }
    }

    ans(&new_state, cycles - 1)
}

fn ans_2(state: &Vec<Vec<Vec<Vec<char>>>>, cycles: usize) -> usize {
    if cycles == 0 {
        return state.iter().fold(0, |acc, w| {
            acc + w.iter().fold(0, |acc, z| {
                acc + z
                    .iter()
                    .fold(0, |acc, x| acc + x.iter().filter(|&y| *y == '#').count())
            })
        });
    }

    let state = expand_2(state);
    let mut new_state = state.to_vec();

    for w in 0..state.len() {
        for z in 0..state[w].len() {
            for x in 0..state[w][z].len() {
                for y in 0..state[w][z][x].len() {
                    new_state[w][z][x][y] = match (
                        state[w][z][x][y],
                        count_neighbors_2(&state, w as i64, z as i64, x as i64, y as i64),
                    ) {
                        ('#', 2) | ('#', 3) => '#',
                        ('.', 3) => '#',
                        _ => '.',
                    }
                }
            }
        }
    }

    ans_2(&new_state, cycles - 1)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo, 6), 375);
    assert_eq!(ans_2(&vec![foo], 6), 2192);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&foo, 6), 112);
    assert_eq!(ans_2(&vec![foo], 6), 848);
}
