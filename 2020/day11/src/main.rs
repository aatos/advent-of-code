use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone, Copy, PartialEq)]
enum SeatKind {
    Empty,
    Occupied,
    Floor,
}

impl SeatKind {
    fn new(c: char) -> SeatKind {
        match c {
            '#' => SeatKind::Occupied,
            'L' => SeatKind::Empty,
            '.' => SeatKind::Floor,
            _ => panic!("invalid char: {}", c),
        }
    }
}

fn read(path: &str) -> io::Result<Vec<Vec<SeatKind>>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().map(SeatKind::new).collect())
            .collect()
    })
}

fn count_adjacents(seats: &Vec<Vec<SeatKind>>, i: i64, j: i64) -> usize {
    let min_x = cmp::max(i * -1, -1);
    let max_x = cmp::min(seats.len() as i64 - i, 2);

    let min_y = cmp::max(j * -1, -1);
    let max_y = cmp::min(seats[0].len() as i64 - j, 2);

    (min_x..max_x)
        .flat_map(|x| {
            (min_y..max_y).filter(move |&y| {
                (y != 0 || x != 0)
                    && seats[(i + x) as usize][(j + y) as usize] == SeatKind::Occupied
            })
        })
        .count()
}

fn count_adjacents_2(seats: &Vec<Vec<SeatKind>>, i: i64, j: i64) -> usize {
    let min_x = i * -1;
    let max_x = seats.len() as i64 - i;

    let min_y = j * -1;
    let max_y = seats[0].len() as i64 - j;

    // This is bad..
    vec![
        (1..max_x)
            .map(|x| seats[(i + x) as usize][j as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (1..max_x)
            .zip(1..max_y)
            .map(|(x, y)| seats[(i + x) as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (1..max_x)
            .zip((min_y..=-1).rev())
            .map(|(x, y)| seats[(i + x) as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (min_x..=-1)
            .rev()
            .zip(1..max_y)
            .map(|(x, y)| seats[(i + x) as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (min_x..=-1)
            .rev()
            .zip((min_y..=-1).rev())
            .map(|(x, y)| seats[(i + x) as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (min_x..=-1)
            .rev()
            .map(|x| seats[(i + x) as usize][j as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (1..max_y)
            .map(|y| seats[i as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
        (min_y..=-1)
            .rev()
            .map(|y| seats[i as usize][(j + y) as usize])
            .find(|&s| s == SeatKind::Occupied || s == SeatKind::Empty),
    ]
    .iter()
    .filter(|&&s| s == Some(SeatKind::Occupied))
    .count()
}

fn ans(
    seats: &Vec<Vec<SeatKind>>,
    f_adj: fn(&Vec<Vec<SeatKind>>, i64, i64) -> usize,
    max_neighbors: usize,
) -> usize {
    let new_seats = seats
        .iter()
        .enumerate()
        .map(|(i, row)| {
            row.iter()
                .enumerate()
                .map(|(j, s)| match s {
                    SeatKind::Empty if f_adj(seats, i as i64, j as i64) == 0 => SeatKind::Occupied,
                    SeatKind::Occupied if f_adj(seats, i as i64, j as i64) >= max_neighbors => {
                        SeatKind::Empty
                    }
                    s => *s,
                })
                .collect()
        })
        .collect();

    if *seats == new_seats {
        seats
            .iter()
            .map(|row| row.iter().filter(|&&s| s == SeatKind::Occupied).count())
            .sum()
    } else {
        ans(&new_seats, f_adj, max_neighbors)
    }
}

fn main() {
    let seats = read("src/input").unwrap();

    assert_eq!(ans(&seats, count_adjacents, 4), 2368);
    assert_eq!(ans(&seats, count_adjacents_2, 5), 2124);
}

#[test]
fn test_input() {
    let seats = read("src/test").unwrap();

    assert_eq!(count_adjacents(&seats, 0, 2), 0);
    assert_eq!(ans(&seats, count_adjacents, 4), 37);
    assert_eq!(ans(&seats, count_adjacents_2, 5), 26);
}

#[test]
fn test_input_2() {
    let seats = read("src/test_2").unwrap();

    assert_eq!(count_adjacents(&seats, 0, 2), 4);
    assert_eq!(ans(&seats, count_adjacents, 4), 37);
}

#[test]
fn test_input_3() {
    let seats = read("src/test_3").unwrap();

    assert_eq!(count_adjacents_2(&seats, 3, 3), 0);
}

#[test]
fn test_input_4() {
    let seats = read("src/test_4").unwrap();

    assert_eq!(count_adjacents_2(&seats, 4, 3), 8);
}

#[test]
fn test_input_5() {
    let seats = read("src/test_5").unwrap();

    assert_eq!(count_adjacents_2(&seats, 1, 1), 0);
}
