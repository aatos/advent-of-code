use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<(u64, u64)> {
    File::open(path).map(|file| {
        let v: Vec<u64> = io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<u64>().unwrap())
            .collect();

        if v.len() != 2 {
            panic!("invalid input");
        }

        (v[0], v[1])
    })
}

fn transform(subject: u64, loop_size: usize) -> u64 {
    let mut val = 1;
    for _ in 1..=loop_size {
        val *= subject;
        val = val % 20201227;
    }

    val
}

fn get_loop_size(key: u64) -> Option<usize> {
    let subject = 7;

    let mut val = 1;
    for i in 1.. {
        val *= subject;
        val = val % 20201227;
        if val == key {
            return Some(i);
        }
    }

    None
}

fn ans(keys: &(u64, u64)) -> u64 {
    let (card, door) = *keys;

    let card_loop_size = get_loop_size(card).unwrap();

    transform(door, card_loop_size)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo), 11576351);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&foo), 14897079);
}
