use std::collections::HashSet;

use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<u64>> {
    let file = File::open(path)?;

    Ok(io::BufReader::new(file)
        .lines()
        .map(|x| x.unwrap().parse::<u64>().unwrap())
        .collect())
}

fn sums_to(preamble: &HashSet<u64>, sum: u64) -> bool {
    preamble
        .iter()
        .any(|&x| x < sum && sum != x * 2 && preamble.contains(&(sum - x)))
}

fn ans(nums: &Vec<u64>, preamble_len: usize) -> u64 {
    let mut preamble: HashSet<u64> = nums[0..preamble_len].iter().map(|&x| x).collect();

    *nums
        .iter()
        .skip(preamble_len)
        .enumerate()
        .find(|(i, &x)| {
            let summed = !sums_to(&preamble, x);

            preamble.remove(&nums[*i]);
            preamble.insert(nums[i + preamble_len]);
            summed
        })
        .unwrap()
        .1
}

fn ans_2(nums: &Vec<u64>, to_find: u64) -> u64 {
    let mut sum: u64 = nums[0];
    let mut start: usize = 0;

    let (end, _) = nums
        .iter()
        .skip(1)
        .enumerate()
        .find(|(_, &x)| {
            sum += x;

            while sum > to_find {
                sum -= nums[start];
                start += 1;
            }

            sum == to_find
        })
        .unwrap();

    nums[start..(end + 1)].iter().min().unwrap() + nums[start..(end + 1)].iter().max().unwrap()
}

fn main() {
    let nums = read("src/input").unwrap();

    assert_eq!(ans(&nums, 25), 675280050);
    assert_eq!(ans_2(&nums, 675280050), 96081673);
}

#[test]
fn test_input() {
    let nums = read("src/test").unwrap();

    assert_eq!(ans(&nums, 5), 127);
    assert_eq!(ans_2(&nums, 127), 62);
}
