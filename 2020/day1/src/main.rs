use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<i64>> {
    let file = File::open(path)?;

    let mut nums: Vec<i64> = Vec::new();

    for l in io::BufReader::new(file).lines() {
        let num = l.unwrap().parse::<i64>().unwrap();
        nums.push(num);
    }

    Ok(nums)
}

fn vec_to_hashmap(v: &Vec<i64>) -> HashMap<i64, u64> {
    let mut h = HashMap::new();

    for e in v {
        let c = h.entry(*e).or_insert(0);
        *c += 1;
    }

    h
}

/***
 * Specifically, they need you to find the two entries that sum to 2020 and then
 * multiply those two numbers together.
 */
fn ans(nums: &Vec<i64>, sum: i64) -> Option<i64> {
    let counter = vec_to_hashmap(nums);

    for num in nums {
        let to_find = sum - *num;
        if let Some(count) = counter.get(&to_find) {
            if *num == to_find && *count < 2 {
                continue;
            }
            return Some(num * to_find);
        }
    }

    None
}

/***
 * Find the three entries that sum to 2020 and then multiply those three
 * numbers together.
 */
fn ans_2(nums: &Vec<i64>) -> Option<i64> {
    for num in nums {
        let to_find = 2020 - *num;
        if let Some(a) = ans(nums, to_find) {
            // ^ Slow to recreate the hashmap but whatevs
            return Some(a * *num);
        }
    }

    None
}

fn main() {
    let nums = read("src/input").unwrap();

    println!("{}", ans(&nums, 2020).unwrap());
    println!("{}", ans_2(&nums).unwrap());
}
