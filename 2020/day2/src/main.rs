use std::fs::File;
use std::io::{self, BufRead};
use std::io::{Error, ErrorKind};

struct Password {
    at_least: usize,
    at_most: usize,
    character: char,
    password: String,
}

fn read(path: &str) -> io::Result<Vec<Password>> {
    let file = File::open(path)?;

    let mut passwords: Vec<Password> = Vec::new();

    for l in io::BufReader::new(file).lines() {
        passwords.push(parse(&l.unwrap()).unwrap());
    }

    Ok(passwords)
}

fn parse(line: &str) -> io::Result<Password> {
    let v: Vec<&str> = line.split(' ').collect();
    if v.len() != 3 {
        return Err(Error::new(ErrorKind::Other, "invalid line!"));
    }

    let nums: Vec<usize> = v[0]
        .split('-')
        .map(|x| x.parse::<usize>().unwrap())
        .collect();
    if nums.len() != 2 {
        return Err(Error::new(ErrorKind::Other, "invalid nums!"));
    }

    let character: Vec<char> = v[1].strip_suffix(":").unwrap().chars().collect();
    if character.len() != 1 {
        return Err(Error::new(ErrorKind::Other, "invalid char!"));
    }

    Ok(Password {
        at_least: nums[0],
        at_most: nums[1],
        character: character[0],
        password: v[2].to_string(),
    })
}

fn ans(pws: &Vec<Password>) -> u64 {
    let mut valid_pws: u64 = 0;

    for pw in pws {
        let occurances = pw.password.chars().filter(|x| *x == pw.character).count();
        if occurances >= pw.at_least && occurances <= pw.at_most {
            valid_pws += 1;
        }
    }

    valid_pws
}

fn ans_2(pws: &Vec<Password>) -> u64 {
    let mut valid_pws: u64 = 0;

    for pw in pws {
        let chars: Vec<char> = pw.password.chars().collect();
        if (chars[pw.at_least - 1] == pw.character && chars[pw.at_most - 1] != pw.character)
            || (chars[pw.at_least - 1] != pw.character && chars[pw.at_most - 1] == pw.character)
        {
            valid_pws += 1;
        }
    }

    valid_pws
}

fn main() {
    let pws = read("src/input").unwrap();

    println!("{}", ans(&pws));
    println!("{}", ans_2(&pws));
}
