use std::fmt;
use std::fs::File;
use std::io::{self, BufRead};

const TREE: char = '#';
const OPEN: char = '.';

enum SquareKind {
    Tree,
    Open,
}

impl fmt::Debug for SquareKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            SquareKind::Tree => write!(f, "{}", TREE),
            SquareKind::Open => write!(f, "{}", OPEN),
        }
    }
}

fn read(path: &str) -> io::Result<Vec<Vec<SquareKind>>> {
    let file = File::open(path)?;

    let mut forest: Vec<Vec<SquareKind>> = Vec::new();

    for l in io::BufReader::new(file).lines() {
        forest.push(
            l.unwrap()
                .chars()
                .map(|x| match x {
                    TREE => SquareKind::Tree,
                    OPEN => SquareKind::Open,
                    _ => panic!("Invalid input: {}", x),
                })
                .collect(),
        );
    }

    Ok(forest)
}

fn ans(forest: &Vec<Vec<SquareKind>>, right: usize, down: usize) -> usize {
    forest
        .iter()
        .skip(down)
        .step_by(down)
        .enumerate()
        .filter(|(i, x)| match x[((i + 1) * right) % x.len()] {
            SquareKind::Tree => true, // no nicer way to do this?
            _ => false,
        })
        .count()
}

fn ans_2(forest: &Vec<Vec<SquareKind>>) -> usize {
    vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|&(r, d)| ans(forest, r, d))
        .product()
}

fn main() {
    let forest = read("src/input").unwrap();

    println!("{}", ans(&forest, 3, 1)); // 268
    println!("{}", ans_2(&forest)); // 3093068400
}

#[test]
fn test_input() {
    let forest = read("src/test").unwrap();
    assert_eq!(ans(&forest, 3, 1), 7);
    assert_eq!(ans(&forest, 1, 1), 2);
    assert_eq!(ans(&forest, 5, 1), 3);
    assert_eq!(ans(&forest, 7, 1), 4);
    assert_eq!(ans(&forest, 1, 2), 2);

    assert_eq!(ans_2(&forest), 336);
}
