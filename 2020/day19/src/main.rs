use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone)]
enum Rule {
    And(Vec<usize>),
    Or(Vec<usize>, Vec<usize>),
    Term(char),
}

fn read(path: &str) -> io::Result<(HashMap<usize, Rule>, Vec<String>)> {
    File::open(path).map(|file| {
        let mut rules: HashMap<usize, Rule> = HashMap::new();
        let mut lines: Vec<String> = Vec::new();

        for line in io::BufReader::new(file).lines().map(|x| x.unwrap()) {
            if line.is_empty() {
                continue;
            }

            if line.contains(": ") {
                let r: Vec<&str> = line.split(": ").collect();

                let rule = if r[1].contains("|") {
                    let v: Vec<Vec<usize>> = r[1]
                        .split(" | ")
                        .map(|x| x.split(" ").map(|x| x.parse::<usize>().unwrap()).collect())
                        .collect();
                    if v.len() != 2 {
                        panic!("invalid OR {}", r[1]);
                    }
                    Rule::Or(v[0].clone(), v[1].clone())
                } else if r[1].contains("\"") {
                    Rule::Term(
                        r[1].strip_prefix("\"")
                            .unwrap()
                            .strip_suffix("\"")
                            .unwrap()
                            .chars()
                            .next()
                            .unwrap(),
                    )
                } else {
                    Rule::And(
                        r[1].split(" ")
                            .map(|x| x.parse::<usize>().unwrap())
                            .collect(),
                    )
                };

                rules.insert(r[0].parse::<usize>().unwrap(), rule);
            } else {
                lines.push(line);
            }
        }

        (rules, lines)
    })
}

fn does_match(rules: &HashMap<usize, Rule>, line: &[char], should_match: &[usize]) -> bool {
    if line.is_empty() || should_match.is_empty() {
        line.len() == 0 && should_match.len() == 0
    } else {
        let rule = should_match[0];
        let should_match = &should_match[1..];

        match &rules[&rule] {
            Rule::And(xs) => does_match(
                rules,
                line,
                &xs.iter().chain(should_match).copied().collect::<Vec<_>>()[..],
            ),

            Rule::Or(xs, ys) => {
                does_match(
                    rules,
                    line,
                    &xs.iter().chain(should_match).copied().collect::<Vec<_>>()[..],
                ) || does_match(
                    rules,
                    line,
                    &ys.iter().chain(should_match).copied().collect::<Vec<_>>()[..],
                )
            }

            Rule::Term(c) => *c == line[0] && does_match(rules, &line[1..], should_match),
        }
    }
}

fn ans(rules: &HashMap<usize, Rule>, lines: &Vec<String>) -> usize {
    lines
        .iter()
        .filter(|line| does_match(rules, &line.chars().collect::<Vec<_>>()[..], &vec![0]))
        .count()
}

fn ans_2(rules: &HashMap<usize, Rule>, lines: &Vec<String>) -> usize {
    let mut rules = rules.clone();

    rules.insert(8, Rule::Or(vec![42], vec![42, 8]));
    rules.insert(11, Rule::Or(vec![42, 31], vec![42, 11, 31]));

    ans(&rules, lines)
}

fn main() {
    let (rules, lines) = read("src/input").unwrap();

    assert_eq!(ans(&rules, &lines), 162);
    assert_eq!(ans_2(&rules, &lines), 267);
}

#[test]
fn test_input() {
    let (rules, lines) = read("src/test").unwrap();

    assert_eq!(ans(&rules, &lines), 2);
}

#[test]
fn test_input_2() {
    let (rules, lines) = read("src/test_2").unwrap();

    assert_eq!(ans(&rules, &lines), 3);
}

#[test]
fn test_input_2_loop() {
    let (rules, lines) = read("src/test_2").unwrap();

    assert_eq!(ans_2(&rules, &lines), 12);
}

#[test]
fn test_input_3_loop() {
    let (rules, lines) = read("src/test_3").unwrap();

    assert_eq!(ans(&rules, &vec!["a".to_string()]), 1);
    assert_eq!(ans(&rules, &vec!["ba".to_string()]), 1);
    assert_eq!(ans(&rules, &vec!["bbbba".to_string()]), 1);

    assert_eq!(ans(&rules, &lines), 3);
}

#[test]
fn test_input_4_loop() {
    let (rules, lines) = read("src/test_4").unwrap();

    assert_eq!(ans(&rules, &vec!["a".to_string()]), 1);
    assert_eq!(ans(&rules, &vec!["ab".to_string()]), 0);

    assert_eq!(ans(&rules, &lines), 1);
}

#[test]
fn test_input_5_loop() {
    let mut rules = HashMap::new();

    rules.insert(0, Rule::Or(vec![11, 12], vec![11, 12, 0]));
    rules.insert(11, Rule::Term('a'));
    rules.insert(12, Rule::Term('b'));

    assert_eq!(ans(&rules, &vec!["abab".to_string()]), 1);
}
