use std::fs::File;
use std::io::{self, BufRead};
use std::io::{Error, ErrorKind};

struct Passport {
    byr: Option<String>,
    iyr: Option<String>,
    eyr: Option<String>,
    hgt: Option<String>,
    hcl: Option<String>,
    ecl: Option<String>,
    pid: Option<String>,
    cid: Option<String>,
}

impl Passport {
    fn new() -> Passport {
        Passport {
            byr: None,
            iyr: None,
            eyr: None,
            hgt: None,
            hcl: None,
            ecl: None,
            pid: None,
            cid: None,
        }
    }

    fn contains_required_fields(&self) -> bool {
        self.byr.is_some()
            && self.iyr.is_some()
            && self.eyr.is_some()
            && self.hgt.is_some()
            && self.hcl.is_some()
            && self.ecl.is_some()
            && self.pid.is_some()
    }

    fn contains_valid_fields(&self) -> bool {
        self.byr
            .as_ref()
            .filter(|x| (1920..2003).contains(&x.parse::<u32>().unwrap()))
            .is_some()
            && self
                .iyr
                .as_ref()
                .filter(|x| (2010..2021).contains(&x.parse::<u32>().unwrap()))
                .is_some()
            && self
                .eyr
                .as_ref()
                .filter(|x| (2020..2031).contains(&x.parse::<u32>().unwrap()))
                .is_some()
            && self
                .hgt
                .as_ref()
                .filter(|x| {
                    if x.ends_with("in") {
                        (59..77).contains(&x.strip_suffix("in").unwrap().parse::<u32>().unwrap())
                    } else if x.ends_with("cm") {
                        (150..194).contains(&x.strip_suffix("cm").unwrap().parse::<u32>().unwrap())
                    } else {
                        false
                    }
                })
                .is_some()
            && self
                .hcl
                .as_ref()
                .filter(|x| {
                    x.starts_with("#")
                        && x.chars().skip(1).all(|c| match c {
                            '0'..='9' | 'a'..='f' => true,
                            _ => false,
                        })
                        && x.chars().count() == 7
                })
                .is_some()
            && self
                .ecl
                .as_ref()
                .filter(|&x| match x.as_str() {
                    "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,

                    _ => false,
                })
                .is_some()
            && self
                .pid
                .as_ref()
                .filter(|x| {
                    x.chars().all(|c| match c {
                        '0'..='9' => true,
                        _ => false,
                    }) && x.chars().count() == 9
                })
                .is_some()
    }
}

fn read(path: &str) -> io::Result<Vec<Passport>> {
    let file = File::open(path)?;

    let mut passports: Vec<Passport> = Vec::new();
    let mut passport: String = String::new();

    for l in io::BufReader::new(file).lines().map(|x| x.unwrap()) {
        if l.is_empty() {
            passports.push(parse(&passport).unwrap());
            passport.clear();
        } else {
            if !passport.is_empty() {
                passport.push_str(" ");
            }
            passport.push_str(&l);
        }
    }

    if !passport.is_empty() {
        passports.push(parse(&passport).unwrap());
    }

    Ok(passports)
}

fn parse(line: &str) -> io::Result<Passport> {
    let mut passport: Passport = Passport::new();

    for i in line.split(' ') {
        let item: Vec<&str> = i.split(':').collect();
        if item.len() != 2 {
            return Err(Error::new(ErrorKind::Other, format!("invalid item: {}", i)));
        }

        match item[0] {
            "byr" => passport.byr = Some(item[1].to_string()),
            "iyr" => passport.iyr = Some(item[1].to_string()),
            "eyr" => passport.eyr = Some(item[1].to_string()),
            "hgt" => passport.hgt = Some(item[1].to_string()),
            "hcl" => passport.hcl = Some(item[1].to_string()),
            "ecl" => passport.ecl = Some(item[1].to_string()),
            "pid" => passport.pid = Some(item[1].to_string()),
            "cid" => passport.cid = Some(item[1].to_string()),
            _ => return Err(Error::new(ErrorKind::Other, "invalid type!")),
        }
    }

    Ok(passport)
}

fn ans(passports: &Vec<Passport>) -> usize {
    passports
        .iter()
        .filter(|&x| x.contains_required_fields())
        .count()
}

fn ans_2(passports: &Vec<Passport>) -> usize {
    passports
        .iter()
        .filter(|&x| x.contains_valid_fields())
        .count()
}

fn main() {
    let passports = read("src/input").unwrap();

    println!("{}", ans(&passports)); // 213
    println!("{}", ans_2(&passports)); // 147
}

#[test]
fn test_input() {
    let passports = read("src/test").unwrap();
    assert_eq!(ans(&passports), 2);
}

#[test]
fn test_invalid_input_2() {
    let passports = read("src/test_2").unwrap();
    assert_eq!(ans_2(&passports), 0);
}

#[test]
fn test_valid_input_2() {
    let passports = read("src/test_3").unwrap();
    assert_eq!(ans_2(&passports), 4);
}
