use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

fn read(path: &str) -> io::Result<Vec<(Vec<String>, Vec<String>)>> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();

                let s: Vec<&str> = x.split(" (contains ").collect();

                (
                    s[0].split(" ").map(|x| x.to_string()).collect(),
                    s[1].strip_suffix(")")
                        .unwrap()
                        .split(", ")
                        .map(|x| x.to_string())
                        .collect(),
                )
            })
            .collect()
    })
}

fn find_allergens(
    foods: &Vec<(Vec<String>, Vec<String>)>,
) -> (HashSet<String>, HashMap<String, String>) {
    let mut food_to_allergen: HashMap<String, HashSet<String>> = HashMap::new();

    for (ings, algs) in foods {
        for ing in ings {
            let ing_algs = food_to_allergen
                .entry(ing.to_string())
                .or_insert(HashSet::new());
            for alg in algs {
                ing_algs.insert(alg.to_string());
            }
        }
    }

    for (ings, algs) in foods {
        for alg in algs {
            food_to_allergen = food_to_allergen
                .iter()
                .map(|(i, a)| {
                    let mut a = a.clone();

                    if a.contains(alg) && !ings.iter().any(|ing| ing == i) {
                        a.remove(alg);
                    }

                    (i.to_string(), a)
                })
                .collect();
        }
    }

    let mut final_allergens = HashMap::new();

    while let Some((ing, algs)) = food_to_allergen
        .iter()
        .map(|(i, a)| (i.clone(), a.clone()))
        .find(|(_, algs)| algs.len() == 1)
    {
        let mut new_food_to_allergen: HashMap<String, HashSet<String>> = HashMap::new();

        let alg = algs.iter().next().unwrap();

        for (o_ing, o_algs) in food_to_allergen.iter().filter(|&(o_ing, _)| *o_ing != ing) {
            let mut o_algs = o_algs.clone();
            if o_algs.contains(alg) {
                o_algs.remove(alg);
            }

            new_food_to_allergen.insert(o_ing.to_string(), o_algs);
        }
        food_to_allergen = new_food_to_allergen;

        final_allergens.insert(ing, alg.clone());
    }

    let allergen_frees: HashSet<String> = food_to_allergen
        .iter()
        .filter_map(|(i, algs)| {
            if algs.is_empty() {
                Some(i.to_string())
            } else {
                None
            }
        })
        .collect();

    (allergen_frees, final_allergens)
}

fn ans(foods: &Vec<(Vec<String>, Vec<String>)>) -> usize {
    let (allergen_frees, _) = find_allergens(foods);

    foods.iter().fold(0, |acc, (ings, _)| {
        acc + ings.iter().filter(|i| allergen_frees.contains(*i)).count()
    })
}

fn ans_2(foods: &Vec<(Vec<String>, Vec<String>)>) -> String {
    let (_, final_allergens) = find_allergens(foods);

    let mut final_allergens: Vec<_> = final_allergens.iter().collect();

    final_allergens.sort_by(|(_, la), (_, ra)| la.cmp(ra));

    final_allergens
        .iter()
        .map(|(i, _)| i.to_string())
        .collect::<Vec<_>>()
        .join(",")
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo), 2230);
    assert_eq!(&ans_2(&foo), "qqskn,ccvnlbp,tcm,jnqcd,qjqb,xjqd,xhzr,cjxv");
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    let expected = vec![
        (
            vec![
                "mxmxvkd".to_string(),
                "kfcds".to_string(),
                "sqjhc".to_string(),
                "nhms".to_string(),
            ],
            vec!["dairy".to_string(), "fish".to_string()],
        ),
        (
            vec![
                "trh".to_string(),
                "fvjkl".to_string(),
                "sbzzf".to_string(),
                "mxmxvkd".to_string(),
            ],
            vec!["dairy".to_string()],
        ),
        (
            vec!["sqjhc".to_string(), "fvjkl".to_string()],
            vec!["soy".to_string()],
        ),
        (
            vec![
                "sqjhc".to_string(),
                "mxmxvkd".to_string(),
                "sbzzf".to_string(),
            ],
            vec!["fish".to_string()],
        ),
    ];

    assert_eq!(foo, expected);

    assert_eq!(ans(&foo), 5);
    assert_eq!(&ans_2(&foo), "mxmxvkd,sqjhc,fvjkl");
}
