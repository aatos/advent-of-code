use std::collections::HashMap;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone)]
struct Tile {
    data: Vec<Vec<char>>,
    top: Vec<char>,
    bottom: Vec<char>,
    left: Vec<char>,
    right: Vec<char>,
}

impl Tile {
    fn new(data: &Vec<Vec<char>>) -> Tile {
        Tile {
            data: data.to_vec(),
            top: data[0].iter().copied().collect(),
            bottom: data[data.len() - 1].iter().copied().collect(),
            left: data.iter().map(|x| x[0]).collect(),
            right: data.iter().map(|x| x[x.len() - 1]).collect(),
        }
    }
}

impl PartialEq for Tile {
    fn eq(&self, other: &Self) -> bool {
        self.bottom == other.top
            || self.bottom == other.left
            || self.bottom == other.right
            || self.bottom == other.bottom
            || self.top == other.top
            || self.top == other.left
            || self.top == other.right
            || self.top == other.bottom
            || self.right == other.top
            || self.right == other.left
            || self.right == other.right
            || self.right == other.bottom
            || self.left == other.top
            || self.left == other.left
            || self.left == other.right
            || self.left == other.bottom
    }
}

type Container = HashMap<usize, Tile>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut iter = io::BufReader::new(file).lines().map(|x| x.unwrap());
        let mut res = HashMap::new();

        // This is ugly
        loop {
            let tile_id = match iter.next() {
                Some(s) => s
                    .strip_prefix("Tile ")
                    .unwrap()
                    .strip_suffix(":")
                    .unwrap()
                    .parse::<usize>()
                    .unwrap(),
                None => break,
            };

            let mut v: Vec<Vec<char>> = vec![];

            loop {
                match iter.next() {
                    Some(x) if !x.is_empty() => v.push(x.chars().collect()),
                    _ => {
                        res.insert(tile_id, Tile::new(&v));
                        break;
                    }
                }
            }
        }
        res
    })
}

fn get_corner_pieces(tiles: &Container) -> impl Iterator<Item = usize> + '_ {
    tiles.iter().filter_map(move |(id, t)| {
        let matches = tiles
            .iter()
            .filter(|(&i, _)| i != *id)
            .filter(|(_, ot)| &t == ot || t == &Tile::new(&rotate(&rotate(&ot.data))))
            .count();

        if matches == 2 {
            Some(*id)
        } else {
            None
        }
    })
}

fn ans(tiles: &Container) -> usize {
    get_corner_pieces(tiles).product()
}

fn rotate(m: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut new = m.to_vec();

    for x in 0..(m.len() / 2) {
        for y in x..(m.len() - x - 1) {
            let temp: char = new[x][y];

            new[x][y] = new[y][m.len() - 1 - x];
            new[y][m.len() - 1 - x] = new[m.len() - 1 - x][m.len() - 1 - y];
            new[m.len() - 1 - x][m.len() - 1 - y] = new[m.len() - 1 - y][x];
            new[m.len() - 1 - y][x] = temp;
        }
    }

    new
}

fn flip_horizontally(m: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    m.iter()
        .map(|x| x.iter().rev().cloned().collect())
        .collect()
}

#[allow(dead_code)]
fn print(m: &Vec<Vec<char>>) {
    m.iter()
        .for_each(|x| println!("{}", x.iter().collect::<String>()));
    println!();
}

fn map_to_vec(h: HashMap<(i32, i32), Vec<Vec<char>>>) -> Vec<Vec<Vec<Vec<char>>>> {
    let m = (h.len() as f64).sqrt() as usize;

    let mut res = vec![vec![vec![]; m]; m];

    let min_x = h
        .iter()
        .map(|(p, _)| p)
        .min_by_key(|(x, _)| *x)
        .unwrap()
        .0
        .abs();
    let min_y = h
        .iter()
        .map(|(p, _)| p)
        .min_by_key(|(_, y)| *y)
        .unwrap()
        .1
        .abs();

    for ((x, y), t) in h {
        res[(x + min_x) as usize][(y + min_y) as usize] = t;
    }

    res
}

fn transform_res(m: Vec<Vec<Vec<Vec<char>>>>) -> Vec<Vec<char>> {
    let mut v = Vec::with_capacity(m.len() * (m[0][0].len() - 2));

    for i in 0..m.len() {
        for j in 1..m[0][0].len() - 1 {
            let slen = m[0][0][0].len();

            let mut s = Vec::with_capacity(m[0].len() * slen);
            for x in 0..m[0].len() {
                m[i][x][j]
                    .iter()
                    .skip(1)
                    .take(slen - 2)
                    .for_each(|x| s.push(*x));
            }
            v.push(s);
        }
    }

    v
}

fn ans_2(tiles: &Container) -> usize {
    let mut seen: HashSet<usize> = HashSet::new();
    let mut to_go: VecDeque<(Tile, (i32, i32))> = VecDeque::new();
    let mut res: HashMap<(i32, i32), Vec<Vec<char>>> = HashMap::new();

    let id = get_corner_pieces(&tiles).next().unwrap();

    to_go.push_back((tiles.get(&id).unwrap().clone(), (0, 0)));
    seen.insert(id);
    while !to_go.is_empty() {
        let (t, (i, j)) = to_go.pop_front().unwrap();
        res.insert((i, j), t.data);

        'outer: for (oid, ot) in tiles {
            if seen.contains(oid) {
                continue;
            }

            let mut ot = Tile::new(&rotate(&ot.data));

            for _ in 0..4 {
                if t.right == ot.left {
                    to_go.push_back((ot, (i, j + 1)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.left == ot.right {
                    to_go.push_back((ot, (i, j - 1)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.top == ot.bottom {
                    to_go.push_back((ot, (i - 1, j)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.bottom == ot.top {
                    to_go.push_back((ot, (i + 1, j)));
                    seen.insert(*oid);

                    continue 'outer;
                }
                ot = Tile::new(&rotate(&ot.data));
            }

            ot = Tile::new(&flip_horizontally(&ot.data));

            for _ in 0..4 {
                if t.right == ot.left {
                    to_go.push_back((ot, (i, j + 1)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.left == ot.right {
                    to_go.push_back((ot, (i, j - 1)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.top == ot.bottom {
                    to_go.push_back((ot, (i - 1, j)));
                    seen.insert(*oid);

                    continue 'outer;
                } else if t.bottom == ot.top {
                    to_go.push_back((ot, (i + 1, j)));
                    seen.insert(*oid);

                    continue 'outer;
                }

                ot = Tile::new(&rotate(&ot.data));
            }
        }
    }

    let image = transform_res(map_to_vec(res));

    image.iter().fold(0, |acc, line| {
        acc + line.iter().filter(|&x| *x == '#').count()
    }) - (count_monsters(image) * 15)
}

fn count_monsters(mut image: Vec<Vec<char>>) -> usize {
    for _ in 0..4 {
        let monsters = find_monsters(&image);
        if monsters != 0 {
            return monsters;
        }
        image = rotate(&image);
    }

    image = flip_horizontally(&image);

    for _ in 0..4 {
        let monsters = find_monsters(&image);
        if monsters != 0 {
            return monsters;
        }
        image = rotate(&image);
    }

    0
}

fn find_monsters(image: &Vec<Vec<char>>) -> usize {
    let monster = vec![
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    ];

    let mut monsters = 0;

    for i in 0..=(image.len() - monster.len()) {
        for j in 0..=(image[i].len() - monster[0].len()) {
            if (0..3).all(|x| is_match(&image[i + x][j..(j + monster[x].len())], monster[x])) {
                monsters += 1;
            }
        }
    }

    monsters
}

fn is_match(s: &[char], monster: &str) -> bool {
    monster
        .chars()
        .enumerate()
        .all(|(i, c)| c == ' ' || c == s[i])
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo), 30425930368573);
    assert_eq!(ans_2(&foo), 2453);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&foo), 20899048083289);
    assert_eq!(ans_2(&foo), 273);
}

#[test]
fn test_rotate() {
    let m = vec![
        "#....####.".chars().collect::<Vec<char>>(),
        "#..#.##...".chars().collect::<Vec<char>>(),
        "#.##..#...".chars().collect::<Vec<char>>(),
        "######.#.#".chars().collect::<Vec<char>>(),
        ".#...#.#.#".chars().collect::<Vec<char>>(),
        ".#########".chars().collect::<Vec<char>>(),
        ".###.#..#.".chars().collect::<Vec<char>>(),
        "########.#".chars().collect::<Vec<char>>(),
        "##...##.#.".chars().collect::<Vec<char>>(),
        "..###.#.#.".chars().collect::<Vec<char>>(),
    ];

    let expected = vec![
        "...###.#..".chars().collect::<Vec<char>>(),
        "#....##.##".chars().collect::<Vec<char>>(),
        "#..###.#..".chars().collect::<Vec<char>>(),
        "###..#.###".chars().collect::<Vec<char>>(),
        "##.######.".chars().collect::<Vec<char>>(),
        "...#.#.#.#".chars().collect::<Vec<char>>(),
        ".###.###.#".chars().collect::<Vec<char>>(),
        "..##.###.#".chars().collect::<Vec<char>>(),
        "...######.".chars().collect::<Vec<char>>(),
        "####...##.".chars().collect::<Vec<char>>(),
    ];

    assert_eq!(rotate(&m), expected);

    assert_eq!(rotate(&rotate(&rotate(&rotate(&m)))), m);
}

#[test]
fn test_flip_horizontally() {
    let m = vec![
        "..##.#..#.".chars().collect::<Vec<char>>(),
        "##..#.....".chars().collect::<Vec<char>>(),
        "#...##..#.".chars().collect::<Vec<char>>(),
        "####.#...#".chars().collect::<Vec<char>>(),
        "##.##.###.".chars().collect::<Vec<char>>(),
        "##...#.###".chars().collect::<Vec<char>>(),
        ".#.#.#..##".chars().collect::<Vec<char>>(),
        "..#....#..".chars().collect::<Vec<char>>(),
        "###...#.#.".chars().collect::<Vec<char>>(),
        "..###..###".chars().collect::<Vec<char>>(),
    ];

    let expected = vec![
        ".#..#.##..".chars().collect::<Vec<char>>(),
        ".....#..##".chars().collect::<Vec<char>>(),
        ".#..##...#".chars().collect::<Vec<char>>(),
        "#...#.####".chars().collect::<Vec<char>>(),
        ".###.##.##".chars().collect::<Vec<char>>(),
        "###.#...##".chars().collect::<Vec<char>>(),
        "##..#.#.#.".chars().collect::<Vec<char>>(),
        "..#....#..".chars().collect::<Vec<char>>(),
        ".#.#...###".chars().collect::<Vec<char>>(),
        "###..###..".chars().collect::<Vec<char>>(),
    ];

    assert_eq!(flip_horizontally(&m), expected);

    assert_eq!(flip_horizontally(&flip_horizontally(&m)), m);
}
