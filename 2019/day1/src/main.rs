use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<u64>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<u64>().unwrap())
            .collect()
    })
}

fn ans(fuels: &Container) -> u64 {
    fuels.iter().map(|w| w / 3 - 2).sum()
}

fn ans_2(fuels: &Container) -> u64 {
    fuels
        .iter()
        .map(|f| {
            let mut f = *f;
            std::iter::from_fn(|| {
                f /= 3;
                if f == 0 || f < 2 {
                    None
                } else {
                    f = f - 2;
                    Some(f)
                }
            })
            .sum::<u64>()
        })
        .sum()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(ans(&foo), 3452245);
    assert_eq!(ans_2(&foo), 5175499);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(ans(&foo), 654);
    assert_eq!(ans_2(&foo), 966);
    assert_eq!(ans_2(&vec![100756]), 50346);
}
