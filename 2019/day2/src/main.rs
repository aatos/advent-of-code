use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .split(',')
                    .map(|x| x.parse::<Inner>().unwrap())
                    .collect::<Vec<_>>()
            })
            .collect()
    })
}

fn part1(mut app: Container) -> usize {
    app[1] = 12;
    app[2] = 2;

    process(&mut app)
}

fn exec(app: &mut Container, pos: usize, f: fn(Inner, Inner) -> Inner) {
    let res = app[pos + 3] as usize;
    let lhs = app[pos + 1] as usize;
    let rhs = app[pos + 2] as usize;

    app[res] = f(app[lhs], app[rhs]);
}

fn process(app: &mut Container) -> usize {
    let mut pos = 0;
    loop {
        match app[pos] {
            1 => exec(app, pos, |x, y| x + y),
            2 => exec(app, pos, |x, y| x * y),
            99 => break,
            _ => todo!(),
        }

        pos += 4
    }

    app[0] as usize
}

fn part2(mut app: Container) -> usize {
    let target = 19690720;

    let mut noun = 1;
    let mut verb = 0;

    loop {
        app[1] = noun;
        app[2] = verb;
        if process(&mut app.clone()) == target {
            break;
        }

        verb += 1;
        if verb > 100 {
            noun += 1;
            verb = 0;
        }
    }
    (100 * noun + verb) as usize
}
fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(foo.clone()), 2692315);
    assert_eq!(part2(foo.clone()), 9507);
}

#[test]
fn test_input() {
    let mut foo = read("src/test").unwrap();

    assert_eq!(process(&mut foo), 3500);
}
