use once_cell::sync::Lazy;
use regex::Regex;
use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;
type Container = Vec<Instruction>;

enum Instruction {
    Mul(u64, u64),
    Do,
    Skip,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        static RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"mul\((\d+),(\d+)\)").unwrap());

        if let Some(c) = RE.captures(s) {
            Ok(Instruction::Mul(
                c[1].parse().unwrap(),
                c[2].parse().unwrap(),
            ))
        } else {
            match s {
                "do()" => Ok(Instruction::Do),
                "don't()" => Ok(Instruction::Skip),
                x => panic!("invalid fold: {}", x),
            }
        }
    }
}

fn read(path: &str) -> io::Result<Container> {
    let re = Regex::new(r"mul\(\d+,\d+\)|do\(\)|don't\(\)").unwrap();

    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                re.find_iter(&x.unwrap())
                    .map(|x| x.as_str().parse::<Instruction>().unwrap())
                    .collect::<Vec<_>>()
            })
            .collect()
    })
}

fn part1(v: &Container) -> u64 {
    v.iter()
        .map(|i| match i {
            Instruction::Mul(x, y) => x * y,
            _ => 0,
        })
        .sum()
}

fn part2(v: &Container) -> u64 {
    v.iter()
        .fold((true, 0), |(process, acc), i| match i {
            Instruction::Mul(x, y) => (process, if process { acc + x * y } else { acc }),
            Instruction::Do => (true, acc),
            Instruction::Skip => (false, acc),
        })
        .1
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 173517243);
    assert_eq!(part2(&data), 100450138);
}
