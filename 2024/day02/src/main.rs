use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                x.split(" ")
                    .map(|x| x.parse::<Inner>().unwrap())
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}

fn is_safe(r: &[Inner], skip_i: Option<usize>) -> bool {
    let mut prev = 0;

    for i in 0..(r.len() - 1) {
        let mut next = i + 1;
        if let Some(s) = skip_i {
            if s == i {
                continue;
            }
            if s == next {
                next += 1;
                if next >= r.len() {
                    break;
                }
            }
        }

        let diff = r[i] as i64 - (r[next] as i64);
        if prev * diff < 0 || !(1..4).contains(&diff.abs()) {
            return false;
        }
        prev = diff;
    }

    true
}

fn part1(v: &Container) -> usize {
    v.iter().filter(|r| is_safe(r, None)).count()
}

fn part2(v: &Container) -> usize {
    v.iter()
        .filter(|r| is_safe(r, None) || (0..r.len()).any(|i| is_safe(r, Some(i))))
        .count()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 218);
    assert_eq!(part2(&data), 290);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part2(&data), 4);
    assert_eq!(part2(&vec![vec![95, 89, 88, 87, 86]]), 1);
}
