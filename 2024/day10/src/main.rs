use std::cmp;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter;

type Inner = u32;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .map(|c| c.to_digit(10).unwrap())
                    .collect()
            })
            .collect()
    })
}

fn non_diag_neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    (min_y..=max_y)
        .zip(iter::repeat(x))
        .chain(iter::repeat(y).zip(min_x..=max_x))
        .filter(move |&(j, i)| (i != x || j != y))
}

fn recur(v: &Container, i: usize, j: usize) -> HashSet<(usize, usize)> {
    if v[i][j] == 9 {
        [(i, j)].into()
    } else {
        non_diag_neighbors(v.len(), v[i].len(), i, j)
            .filter(|&(y, x)| v[y][x] == v[i][j] + 1)
            .flat_map(|(y, x)| recur(v, y, x))
            .collect()
    }
}

fn part1(v: &Container) -> usize {
    let mut sum = vec![];
    for i in 0..v.len() {
        for j in 0..v[i].len() {
            if v[i][j] == 0 {
                sum.extend(recur(v, i, j).into_iter());
            }
        }
    }
    sum.len()
}

fn recur2(v: &Container, i: usize, j: usize) -> Vec<(usize, usize)> {
    if v[i][j] == 9 {
        [(i, j)].into()
    } else {
        non_diag_neighbors(v.len(), v[i].len(), i, j)
            .filter(|&(y, x)| v[y][x] == v[i][j] + 1)
            .flat_map(|(y, x)| recur2(v, y, x))
            .collect()
    }
}

fn part2(v: &Container) -> usize {
    let mut sum = 0;
    for i in 0..v.len() {
        for j in 0..v[i].len() {
            if v[i][j] == 0 {
                sum += recur2(v, i, j).len();
            }
        }
    }
    sum
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 629);
    assert_eq!(part2(&data), 1242);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 36);
    assert_eq!(part2(&data), 81);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();

    assert_eq!(part1(&data), 1);
    assert_eq!(part2(&data), 16);
}
