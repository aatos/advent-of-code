use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                let x = x.unwrap();
                x.split(' ')
                    .map(|s| s.parse::<Inner>().unwrap())
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}

fn split_cache(i: u64, cache: &mut HashMap<u64, Option<(u64, u64)>>) -> Option<(u64, u64)> {
    *cache.entry(i).or_insert_with(|| split(i))
}

fn split(i: u64) -> Option<(u64, u64)> {
    let s = i.to_string();

    if s.len() % 2 != 0 {
        None
    } else {
        Some((
            s[0..s.len() / 2].parse().unwrap(),
            s[(s.len() / 2)..s.len()]
                .trim_start_matches('0')
                .parse()
                .unwrap_or(0),
        ))
    }
}

fn simulate(v: &Container, iterations: usize) -> usize {
    let mut cache: HashMap<u64, Option<(u64, u64)>> = HashMap::new();

    let mut v = v.clone();

    for _ in 0..iterations {
        let mut next = Vec::with_capacity(v.len());
        for &i in &v {
            if i == 0 {
                next.push(1);
            } else if let Some((x, y)) = split_cache(i, &mut cache) {
                next.push(x);
                next.push(y);
            } else {
                next.push(i * 2024);
            }
        }
        v = next;
    }

    v.len()
}

fn part1(v: &Container) -> usize {
    simulate(&v.to_vec(), 25)
}

fn simulate2(mut v: Container, iterations: usize) -> usize {
    for _ in 0..iterations {
        let mut i = 0;
        while i < v.len() {
            if v[i] == 0 {
                v[i] = 1;
            } else if let Some((x, y)) = split(v[i]) {
                v[i] = x;
                v.insert(i + 1, y);
                i += 1;
            } else {
                v[i] *= 2024;
            }
            i += 1;
        }
    }

    v.len()
}

fn part2(v: &Container) -> usize {
    simulate(v, 75)
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 182081);
    //assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 55312);
    //assert_eq!(part2(&data), 0);
}

#[test]
fn test_split() {
    assert_eq!(split(1000), Some((10, 0)));
    assert_eq!(split(253000), Some((253, 0)));
    assert_eq!(split(2024), Some((20, 24)));
    assert_eq!(split(28676032), Some((2867, 6032)));
    assert_eq!(split(1), None);
}
