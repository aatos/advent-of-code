use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .replace(":", "")
                    .split(' ')
                    .map(|i| i.parse::<Inner>().unwrap())
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}

fn recur(v: &Vec<Inner>, i: usize, operator: char, acc: Inner, operators: &Vec<char>) -> Inner {
    if i == v.len() {
        acc
    } else {
        let acc = match operator {
            '*' => acc * v[i],
            '+' => acc + v[i],
            '|' => (acc.to_string() + &v[i].to_string()).parse().unwrap(),
            _ => panic!(),
        };

        if operators
            .iter()
            .any(|o| recur(v, i + 1, *o, acc, operators) == v[0])
        {
            v[0]
        } else {
            0
        }
    }
}

fn is_true(v: &Vec<Inner>, operators: &Vec<char>) -> Option<Inner> {
    if recur(v, 1, '+', 0, operators) == v[0] {
        Some(v[0])
    } else {
        None
    }
}

fn part1(v: &Container) -> Inner {
    v.iter().filter_map(|v| is_true(v, &vec!['+', '*'])).sum()
}

fn part2(v: &Container) -> Inner {
    v.iter()
        .filter_map(|v| is_true(v, &vec!['+', '*', '|']))
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 4998764814652);
    assert_eq!(part2(&data), 37598910447546);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 3749);
    assert_eq!(part2(&data), 11387);
}

#[test]
fn test_true2() {
    assert_eq!(
        is_true(&vec![192, 17, 8, 14], &vec!['+', '*', '|']),
        Some(192)
    );
    assert_eq!(is_true(&vec![156, 15, 6], &vec!['+', '*', '|']), Some(156));
    assert_eq!(
        is_true(&vec![7290, 6, 8, 6, 15], &vec!['+', '*', '|']),
        Some(7290)
    );
}
