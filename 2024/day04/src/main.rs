use itertools::iproduct;
use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    iproduct!(min_y..=max_y, min_x..=max_x).filter(move |&(j, i)| (i != x || j != y))
}

fn part1(v: &Container) -> usize {
    let mut matches = vec![];

    for i in 0..v.len() {
        for j in 0..v[i].len() {
            if v[i][j] == 'X' {
                for (y, x) in neighbors(v[i].len(), v.len(), j, i) {
                    if v[x][y] == 'M' {
                        for (yy, xx) in neighbors(v[i].len(), v.len(), y, x) {
                            if v[xx][yy] == 'A' {
                                for (yyy, xxx) in neighbors(v[i].len(), v.len(), yy, xx) {
                                    if v[xxx][yyy] == 'S' {
                                        matches.push(vec![(i, j), (x, y), (xx, yy), (xxx, yyy)]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    matches
        .iter()
        .filter(|m| {
            ((m[0].0, m[0].1 + 1) == m[1]
                && (m[0].0, m[0].1 + 2) == m[2]
                && (m[0].0, m[0].1 + 3) == m[3])
                || (m[0].1 > 2
                    && (m[0].0, m[0].1 - 1) == m[1]
                    && (m[0].0, m[0].1 - 2) == m[2]
                    && (m[0].0, m[0].1 - 3) == m[3])
                || ((m[0].0 + 1, m[0].1) == m[1]
                    && (m[0].0 + 2, m[0].1) == m[2]
                    && (m[0].0 + 3, m[0].1) == m[3])
                || (m[0].0 > 2
                    && (m[0].0 - 1, m[0].1) == m[1]
                    && (m[0].0 - 2, m[0].1) == m[2]
                    && (m[0].0 - 3, m[0].1) == m[3])
                || ((m[0].0 + 1, m[0].1 + 1) == m[1]
                    && (m[0].0 + 2, m[0].1 + 2) == m[2]
                    && (m[0].0 + 3, m[0].1 + 3) == m[3])
                || (m[0].0 > 2
                    && ((m[0].0 - 1, m[0].1 + 1) == m[1]
                        && (m[0].0 - 2, m[0].1 + 2) == m[2]
                        && (m[0].0 - 3, m[0].1 + 3) == m[3]))
                || (m[0].0 > 2
                    && m[0].1 > 2
                    && ((m[0].0 - 1, m[0].1 - 1) == m[1]
                        && (m[0].0 - 2, m[0].1 - 2) == m[2]
                        && (m[0].0 - 3, m[0].1 - 3) == m[3]))
                || (m[0].1 > 2
                    && ((m[0].0, m[0].1 - 1) == m[1]
                        && (m[0].0, m[0].1 - 2) == m[2]
                        && (m[0].0, m[0].1 - 3) == m[3]))
                || (m[0].1 > 2
                    && ((m[0].0 + 1, m[0].1 - 1) == m[1]
                        && (m[0].0 + 2, m[0].1 - 2) == m[2]
                        && (m[0].0 + 3, m[0].1 - 3) == m[3]))
        })
        .count()
}

fn part2(v: &Container) -> usize {
    let mut matches = 0;

    for i in 0..v.len() {
        for j in 0..v[i].len() {
            if i > 0 && j > 0 && i < v.len() - 1 && j < v[0].len() - 1 && v[i][j] == 'A' {
                if ((v[i + 1][j + 1] == 'S' && v[i - 1][j - 1] == 'M')
                    || (v[i - 1][j - 1] == 'S' && v[i + 1][j + 1] == 'M'))
                    && ((v[i - 1][j + 1] == 'M' && v[i + 1][j - 1] == 'S')
                        || (v[i - 1][j + 1] == 'S' && v[i + 1][j - 1] == 'M'))
                {
                    matches += 1;
                }
            }
        }
    }

    matches
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 2613);
    assert_eq!(part2(&data), 1905);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 18);
    assert_eq!(part2(&data), 9);
}
