use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = (Vec<Inner>, Vec<Inner>);

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let vec = x
                    .split("   ")
                    .map(|x| x.parse::<Inner>().unwrap())
                    .collect::<Vec<Inner>>();
                (vec[0], vec[1])
            })
            .collect()
    })
}

fn part1(v: &Container) -> u64 {
    let mut left = BinaryHeap::from(v.0.clone());
    let mut right = BinaryHeap::from(v.1.clone());

    let mut sum = 0;

    while !left.is_empty() {
        let x = left.pop().unwrap();
        let y = right.pop().unwrap();
        sum += x.abs_diff(y);
    }

    sum
}

fn part2(v: &Container) -> u64 {
    v.0.iter()
        .map(|&x| x * (v.1.iter().filter(|&y| *y == x).count() as u64))
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 2756096);
    assert_eq!(part2(&data), 23117829);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 11);
    assert_eq!(part1(&(vec![1, 2, 3], vec![10, 11, 11])), 26);
}
