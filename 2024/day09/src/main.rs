use std::fs::File;
use std::io::{self, BufRead};

type Inner = u32;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .chars()
                    .map(|c| c.to_digit(10).unwrap())
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}

fn calc_checksum(v: &Vec<Option<Inner>>) -> usize {
    v.iter()
        .enumerate()
        .filter_map(|(i, x)| x.map(|x| i * (x as usize)))
        .sum()
}

fn move_blocks(v: &mut Vec<Option<Inner>>) {
    let mut i = 0;
    let mut j = v.len() - 1;

    while i < j {
        if v[i].is_some() {
            i += 1;
        } else if v[j].is_none() {
            j -= 1;
        } else {
            v.swap(i, j);
        }
    }
}

fn part1(v: &Container) -> usize {
    let mut d = vec![];

    let mut is_free = false;
    let mut i = 0;
    for &b in v {
        for _ in 0..b {
            if is_free {
                d.push(None);
            } else {
                d.push(Some(i));
            }
        }
        if !is_free {
            i += 1;
        }
        is_free = !is_free;
    }

    move_blocks(&mut d);

    calc_checksum(&d)
}

#[derive(Clone, Copy)]
enum Block {
    File((Inner, Inner)),
    Space(Inner),
}

fn get_last_file(v: &Vec<Block>, mut j: usize) -> Option<usize> {
    while j > 1 {
        j -= 1;
        if let Block::File(_) = v[j] {
            return Some(j);
        }
    }
    None
}

fn fix_space(v: &mut Vec<Block>) {
    let mut i = 0;

    while i < v.len() - 1 {
        if let (Block::Space(s), Block::Space(ss)) = (v[i], v[i + 1]) {
            v[i] = Block::Space(s + ss);
            v.remove(i + 1);
        }
        i += 1;
    }
}

fn move_files(v: &mut Vec<Block>) {
    let mut i = 0;
    while let Some(j) = get_last_file(v, v.len()) {
        if i >= j {
            break;
        }

        if let (Block::Space(s), Block::File((sz, _))) = (v[i], v[j]) {
            if sz <= s {
                v.swap(i, j);
                if sz < s {
                    v[j] = Block::Space(sz);

                    if let Block::Space(ss) = v[i + 1] {
                        v[i + 1] = Block::Space(ss + s - sz);
                    } else {
                        v.insert(i + 1, Block::Space(s - sz));
                    }
                }
            } else {
                i += 1;
            }
        } else if let Block::File(_) = v[i] {
            i += 1;
        }
    }
}

fn part2(v: &Container) -> usize {
    let mut d = vec![];

    let mut is_free = false;
    let mut i = 0;
    for &b in v {
        if is_free {
            d.push(Block::Space(b));
        } else {
            d.push(Block::File((b, i)));
        }
        if !is_free {
            i += 1;
        }
        is_free = !is_free;
    }

    move_files(&mut d);

    d.iter()
        .enumerate()
        .filter_map(|(i, x)| {
            if let Block::File((_, x)) = x {
                Some(i * (*x as usize))
            } else {
                None
            }
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 6337367222422);
    assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 1928);
    assert_eq!(part2(&data), 2858);
}
