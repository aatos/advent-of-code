use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Updates = Vec<Vec<Inner>>;
type Rules = HashMap<Inner, Vec<Inner>>;
type Container = (Rules, Updates);

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file).lines().fold(
            (HashMap::new(), vec![]),
            |(mut rules, mut updates), x| {
                let x = x.unwrap();

                if x.contains('|') {
                    let i = x
                        .split('|')
                        .map(|i| i.parse::<Inner>().unwrap())
                        .collect::<Vec<_>>();

                    let e = rules.entry(i[0]).or_insert(vec![]);
                    e.push(i[1]);
                } else if x.contains(',') {
                    updates.push(
                        x.split(',')
                            .map(|i| i.parse::<Inner>().unwrap())
                            .collect::<Vec<_>>(),
                    );
                }
                (rules, updates)
            },
        )
    })
}

fn fix_order(r: &HashMap<Inner, Vec<Inner>>, update: &mut [Inner]) -> bool {
    for j in 0..(update.len() - 1) {
        let empty = vec![];
        let rule = r.get(&update[j]).unwrap_or(&empty);

        for x in j + 1..update.len() {
            if !rule.contains(&update[x]) {
                update.swap(j, x);
                return false;
            }
        }
    }

    true
}

fn is_correct(r: &Rules, u: &[Inner]) -> bool {
    for j in 0..(u.len() - 1) {
        let empty = vec![];
        let rule = r.get(&u[j]).unwrap_or(&empty);

        for x in &u[j + 1..u.len()] {
            if !rule.contains(x) {
                return false;
            }
        }
    }
    true
}

fn part1((r, u): &Container) -> u64 {
    u.iter()
        .filter(|u| is_correct(r, u))
        .map(|v| v[v.len() / 2])
        .sum()
}

fn part2((r, u): &Container) -> u64 {
    u.iter()
        .filter(|u| !is_correct(r, u))
        .map(|u| {
            let mut f = u.clone();
            while !fix_order(r, &mut f) {}
            f
        })
        .map(|v| v[v.len() / 2])
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 5955);
    assert_eq!(part2(&data), 4030);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 143);
    assert_eq!(part2(&data), 123);
}

#[test]
fn test_correct_order() {
    let (r, u) = read("src/test").unwrap();

    assert_eq!(
        u.iter()
            .filter(|u| is_correct(&r, u))
            .cloned()
            .collect::<Vec<_>>(),
        vec![
            vec![75, 47, 61, 53, 29],
            vec![97, 61, 53, 29, 13],
            vec![75, 29, 13]
        ]
    );

    assert!(is_correct(&r, &[97, 75, 13, 29, 47]));
}
