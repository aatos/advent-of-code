use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
type Inner = char;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn find_guard(m: &Container) -> (usize, usize) {
    for (i, r) in m.iter().enumerate() {
        for (j, c) in r.iter().enumerate() {
            match c {
                '^' | '>' | '<' | 'v' => return (i, j),
                _ => continue,
            }
        }
    }
    panic!("no guard found")
}

fn part1(m: &Container) -> usize {
    count_steps(m, find_guard(m)).unwrap()
}

fn next_dir(dir: char) -> char {
    match dir {
        '^' => '>',
        '>' => 'v',
        '<' => '^',
        'v' => '<',
        _ => panic!(),
    }
}

fn next_pos(m: &Container, dir: char, i: usize, j: usize) -> Option<(usize, usize)> {
    match dir {
        '^' if i == 0 => None,
        '>' if j == m[i].len() - 1 => None,
        '<' if j == 0 => None,
        'v' if i == m.len() - 1 => None,
        '^' => Some((i - 1, j)),
        '>' => Some((i, j + 1)),
        '<' => Some((i, j - 1)),
        'v' => Some((i + 1, j)),
        c => panic!("{} instead of guard", c),
    }
}

fn count_steps(m: &Container, (mut i, mut j): (usize, usize)) -> Option<usize> {
    let mut dir = m[i][j];
    let mut visited: HashSet<(usize, usize, char)> = HashSet::from([(i, j, dir)]);

    while let Some((ii, jj)) = next_pos(m, dir, i, j) {
        if m[ii][jj] == '#' {
            dir = next_dir(dir)
        } else if visited.contains(&(ii, jj, dir)) {
            return None;
        } else {
            (i, j) = (ii, jj);
            visited.insert((i, j, dir));
        }
    }

    Some(
        visited
            .iter()
            .map(|(i, j, _)| (i, j))
            .collect::<HashSet<_>>()
            .len(),
    )
}

fn part2(m: &Container) -> usize {
    let (start_i, start_j) = find_guard(m);
    let (mut i, mut j) = (start_i, start_j);
    let mut dir = m[i][j];

    let mut obstructions: HashSet<(usize, usize)> = HashSet::new();

    while let Some((ii, jj)) = next_pos(m, dir, i, j) {
        if m[ii][jj] == '#' {
            dir = next_dir(dir);
        } else {
            if (ii, jj) != (start_i, start_j) && !obstructions.contains(&(ii, jj)) {
                let mut mm = m.clone();
                mm[ii][jj] = '#';
                if count_steps(&mm, (start_i, start_j)).is_none() {
                    obstructions.insert((ii, jj));
                }
            }
            (i, j) = (ii, jj);
        }
    }

    obstructions.len()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 5239);
    assert_eq!(part2(&data), 1753);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 41);
    assert_eq!(part2(&data), 6);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();

    assert_eq!(part1(&data), 20);
    assert_eq!(part2(&data), 3);
}
