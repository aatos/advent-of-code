use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .chars()
                    .map(|i| i.to_digit(10).unwrap() as Inner)
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}

fn part1(nums: &Container) -> u64 {
    let reminder = if nums.last().unwrap() == nums.first().unwrap() {
        *nums.last().unwrap()
    } else {
        0
    };

    nums.windows(2)
        .filter_map(|v| if v[0] == v[1] { Some(v[0]) } else { None })
        .sum::<u64>()
        + reminder
}

fn part2(nums: &Container) -> u64 {
    (0..nums.len())
        .map(|i| {
            if nums[i] == nums[(i + nums.len() / 2) % nums.len()] {
                nums[i]
            } else {
                0
            }
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 1047);
    assert_eq!(part2(&data), 982);
}
