fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .chars()
                    .map(|i| i.to_digit(10).unwrap() as Inner)
                    .collect::<Vec<Inner>>()
            })
            .collect()
    })
}
