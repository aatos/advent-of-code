fn print(c: &Vec<Vec<char>>) {
    for i in 0..c.len() {
        for j in 0..c[i].len() {
            print!("{}", c[i][j]);
        }
        println!();
    }
    println!();
}
