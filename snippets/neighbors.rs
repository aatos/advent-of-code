// itertools = "0.12.0"
use itertools::iproduct;
use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    iproduct!(min_y..=max_y, min_x..=max_x).filter(move |&(j, i)| (i != x || j != y))
}

// use std::iter;
// use std::cmp;
fn non_diag_neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    (min_y..=max_y)
        .zip(iter::repeat(x))
        .chain(iter::repeat(y).zip(min_x..=max_x))
        .filter(move |&(j, i)| (i != x || j != y))
}
