use std::str::FromStr;

enum FoldType {
    Horizontal,
    Vertical,
}

impl FromStr for FoldType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "y" => Ok(FoldType::Horizontal),
            "x" => Ok(FoldType::Vertical),
            x => panic!("invalid fold: {}", x),
        }
    }
}
