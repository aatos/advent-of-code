use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn part1(ids: &Container) -> usize {
    let (twos, threes) = ids.iter().fold((0, 0), |(twos, threes), chars| {
        let (twins, triplets) = chars
            .iter()
            .fold(HashMap::new(), |mut counts, c| {
                *counts.entry(c).or_insert(0) += 1;
                counts
            })
            .iter()
            .fold((0, 0), |(mut has_twins, mut has_triplets), (_, &counts)| {
                if counts == 2 {
                    has_twins = 1;
                }
                if counts == 3 {
                    has_triplets = 1;
                }
                (has_twins, has_triplets)
            });

        (twos + twins, threes + triplets)
    });

    twos * threes
}

fn dist(s1: &Vec<char>, s2: &Vec<char>) -> usize {
    s1.iter()
        .zip(s2.iter())
        .fold(0, |d, (c1, c2)| if c1 != c2 { d + 1 } else { d })
}

fn get_same(s1: &Vec<char>, s2: &Vec<char>) -> String {
    s1.iter()
        .zip(s2.iter())
        .fold(String::new(), |mut s, (c1, c2)| {
            if c1 == c2 {
                s.push(*c1);
            }
            s
        })
}

fn part2(ids: &Container) -> String {
    for i in 0..(ids.len() - 1) {
        for j in i..ids.len() {
            if dist(&ids[i], &ids[j]) == 1 {
                return get_same(&ids[i], &ids[j]);
            }
        }
    }
    "".to_string()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 4693);
    assert_eq!(part2(&data), "pebjqsalrdnckzfihvtxysomg");
}
