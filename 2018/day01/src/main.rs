use std::fs::File;
use std::io::{self, BufRead};

use std::collections::HashSet;

type Inner = i64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn part1(numbers: &Container) -> i64 {
    numbers.iter().sum()
}

fn part2(numbers: &Container) -> i64 {
    let mut seen: HashSet<Inner> = HashSet::new();
    let mut freq = 0;

    for n in numbers.iter().cycle() {
        seen.insert(freq);
        freq += n;
        if seen.contains(&freq) {
            return freq;
        }
    }

    unreachable!()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 585);
    assert_eq!(part2(&data), 83173);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 3);
}

#[test]
fn test_input2() {
    let data = vec![7, 7, -2, -7, -4];

    assert_eq!(part2(&data), 14);
}
