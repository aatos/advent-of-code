use std::convert::From;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = char;
type Container = Vec<(Inner, Inner)>;

#[repr(usize)]
#[derive(PartialEq, Clone, Copy, Debug)]
enum Shape {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

impl Shape {
    fn loses_to(&self) -> Shape {
        unsafe { ::std::mem::transmute(*self as usize % 3 + 1) }
    }

    fn winner_of(&self) -> Shape {
        unsafe { ::std::mem::transmute((*self as usize + 1) % 3 + 1) }
    }
}

impl From<&char> for Shape {
    fn from(rps: &char) -> Self {
        match rps {
            'A' => Shape::Rock,
            'B' => Shape::Paper,
            'C' => Shape::Scissors,
            'X' => Shape::Rock,
            'Y' => Shape::Paper,
            'Z' => Shape::Scissors,
            _ => todo!(),
        }
    }
}

#[derive(Clone, Copy)]
enum Outcome {
    Lose = 0,
    Draw = 3,
    Win = 6,
}

impl From<&char> for Outcome {
    fn from(rps: &char) -> Self {
        match rps {
            'X' => Outcome::Lose,
            'Y' => Outcome::Draw,
            'Z' => Outcome::Win,
            _ => todo!(),
        }
    }
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let plays = x
                    .unwrap()
                    .split(' ')
                    .map(|o| o.parse::<Inner>().unwrap())
                    .collect::<Vec<_>>();
                (plays[0], plays[1])
            })
            .collect()
    })
}

fn part1(plays: &Container) -> usize {
    plays
        .iter()
        .map(|(o, p)| {
            let o: Shape = o.into();
            let p: Shape = p.into();

            p as usize
                + if o == p {
                    Outcome::Draw
                } else if o.winner_of() == p {
                    Outcome::Lose
                } else {
                    Outcome::Win
                } as usize
        })
        .sum()
}

fn part2(plays: &Container) -> usize {
    plays
        .iter()
        .map(|(o, p)| {
            let o: Shape = o.into();
            let p: Outcome = p.into();

            p as usize
                + match (o, p) {
                    (shape, Outcome::Lose) => shape.winner_of(),
                    (shape, Outcome::Win) => shape.loses_to(),
                    (shape, Outcome::Draw) => shape,
                } as usize
        })
        .sum()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 11841);
    assert_eq!(part2(&foo), 13022);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(Shape::Rock.loses_to(), Shape::Paper);
    assert_eq!(Shape::Paper.loses_to(), Shape::Scissors);
    assert_eq!(Shape::Scissors.loses_to(), Shape::Rock);

    assert_eq!(Shape::Rock.winner_of(), Shape::Scissors);
    assert_eq!(Shape::Paper.winner_of(), Shape::Rock);
    assert_eq!(Shape::Scissors.winner_of(), Shape::Paper);

    assert_eq!(part1(&foo), 15);
    assert_eq!(part2(&foo), 12);
}
