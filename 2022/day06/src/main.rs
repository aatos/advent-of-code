use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = char;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| x.unwrap().chars().collect::<Vec<_>>())
            .collect()
    })
}

fn part1(stream: &Container, marker_len: usize) -> usize {
    stream
        .windows(marker_len)
        .enumerate()
        .find_map(|(i, x)| {
            let set = x.iter().collect::<HashSet<_>>();
            if set.len() == marker_len {
                Some(i + marker_len)
            } else {
                None
            }
        })
        .unwrap()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo, 4), 1034);
    assert_eq!(part1(&foo, 14), 2472);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo, 4), 7);
    assert_eq!(part1(&foo, 14), 19);
}
