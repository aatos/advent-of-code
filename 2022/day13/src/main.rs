use std::cmp::Ordering;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;

#[derive(Debug, Clone, Eq, PartialEq)]
enum Val {
    Value(Inner),
    List(Vec<Val>),
}

impl Ord for Val {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Val::Value(l), Val::Value(r)) => l.cmp(r),
            (Val::List(l), Val::List(r)) => {
                let mut lit = l.iter();
                let mut rit = r.iter();
                loop {
                    match (lit.next(), rit.next()) {
                        (Some(l), Some(r)) => match l.cmp(r) {
                            Ordering::Equal => {}
                            o => return o,
                        },
                        (Some(_), None) => return Ordering::Greater,
                        (None, Some(_)) => return Ordering::Less,
                        (None, None) => return Ordering::Equal,
                    }
                }
            }
            (Val::List(_), Val::Value(_)) => self.cmp(&Val::List(vec![other.clone()])),
            (Val::Value(_), Val::List(_)) => Val::List(vec![self.clone()]).cmp(other),
        }
    }
}

impl PartialOrd for Val {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

type Container = Vec<Val>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .filter_map(|x| {
                let x = x.unwrap();

                let mut vecs: Vec<Vec<Val>> = vec![vec![]];

                let mut iter = x.chars().peekable();
                while let Some(c) = iter.next() {
                    if c.is_ascii_digit() {
                        let f = std::iter::from_fn(|| match iter.peek() {
                            Some(&c) if c.is_ascii_digit() => iter.next(),
                            _ => None,
                        });

                        let digit = f.fold(c.to_string(), |s, c| s + &c.to_string());

                        vecs.last_mut()
                            .unwrap()
                            .push(Val::Value(digit.parse::<Inner>().unwrap()));
                    } else if c == '[' {
                        vecs.push(Vec::new());
                    } else if c == ']' {
                        let v = vecs.pop().unwrap();
                        vecs.last_mut().unwrap().push(Val::List(v));
                    }
                }
                vecs.pop().unwrap().pop()
            })
            .collect()
    })
}

fn part1(c: &Container) -> usize {
    c.chunks(2)
        .enumerate()
        .filter_map(|(i, v)| {
            if v[0].cmp(&v[1]) != Ordering::Greater {
                Some(i + 1)
            } else {
                None
            }
        })
        .sum()
}

fn part2(c: &Container) -> usize {
    let decoders = [
        Val::List(vec![Val::List(vec![Val::Value(2)])]),
        Val::List(vec![Val::List(vec![Val::Value(6)])]),
    ];

    let mut c = c.clone();
    c.extend_from_slice(&decoders);
    c.sort();

    c.iter()
        .enumerate()
        .filter_map(|(i, c)| decoders.iter().find(|&d| d == c).map(|_| i + 1))
        .product()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 6272);
    assert_eq!(part2(&data), 22288);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 13);
    assert_eq!(part2(&data), 140);
}
