use std::fs::File;
use std::io::{self, BufRead};
use std::ops::RangeInclusive;

type Inner = u64;
type Container = Vec<(RangeInclusive<Inner>, RangeInclusive<Inner>)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let pairs = x
                    .unwrap()
                    .split(',')
                    .map(|p| {
                        let p = p
                            .split('-')
                            .map(|r| r.parse::<Inner>().unwrap())
                            .collect::<Vec<_>>();
                        (p[0], p[1])
                    })
                    .collect::<Vec<_>>();
                (pairs[0].0..=pairs[0].1, pairs[1].0..=pairs[1].1)
            })
            .collect()
    })
}

fn part1(pairs: &Container) -> usize {
    pairs
        .iter()
        .filter(|(p1, p2)| {
            (p1.contains(p2.start()) && p1.contains(p2.end()))
                || (p2.contains(p1.start()) && p2.contains(p1.end()))
        })
        .count()
}

fn part2(pairs: &Container) -> usize {
    pairs
        .iter()
        .filter(|(p1, p2)| {
            p1.contains(p2.start())
                || p1.contains(p2.end())
                || p2.contains(p1.start())
                || p2.contains(p1.end())
        })
        .count()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 503);
    assert_eq!(part2(&foo), 827);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 2);
    assert_eq!(part2(&foo), 4);
}
