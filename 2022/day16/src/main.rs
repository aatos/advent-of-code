use memoize::memoize;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Clone, Hash, Eq, PartialEq)]
struct Valve {
    name: String,
    flow_rate: usize,
    leading: Vec<String>,
}

impl FromStr for Valve {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (valve, tunnels) = s.split_once("; ").unwrap();

        let valve = valve.split(' ').collect::<Vec<_>>();

        Ok(Valve {
            name: valve[1].to_string(),
            flow_rate: valve[4].strip_prefix("rate=").unwrap().parse().unwrap(),
            leading: tunnels
                .strip_prefix("tunnel leads to ")
                .or_else(|| tunnels.strip_prefix("tunnels lead to "))
                .and_then(|s| {
                    s.strip_prefix("valves ")
                        .or_else(|| s.strip_prefix("valve "))
                })
                .map(|s| s.split(", ").map(|s| s.to_string()).collect::<Vec<_>>())
                .unwrap(),
        })
    }
}

type Container = Vec<Valve>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Valve>().unwrap())
            .collect()
    })
}

const STEPS: usize = 30;

#[memoize]
fn process(
    valves: Container,
    valve_name: String,
    len: usize,
    pressure: usize,
    opened: Vec<String>,
) -> usize {
    if len >= STEPS {
        pressure
    } else {
        let map = valves
            .iter()
            .enumerate()
            .map(|(i, valve)| (valve.name.to_string(), i))
            .collect::<HashMap<_, _>>();

        let mut pressures = BinaryHeap::new();

        let valve = &valves[map[&valve_name]];
        valve.leading.iter().for_each(|n| {
            pressures.push(process(
                valves.clone(),
                n.to_string(),
                len + 1,
                pressure,
                opened.clone(),
            ));

            if !opened.contains(&valve.name) && valve.flow_rate > 0 && len <= 28 {
                let mut opened = opened.clone();
                opened.push(valve.name.to_string());
                pressures.push(process(
                    valves.clone(),
                    n.to_string(),
                    len + 2,
                    pressure + valve.flow_rate * (STEPS - len),
                    opened,
                ));
            }
        });
        pressures.pop().unwrap()
    }
}

fn part1(valves: &Container) -> usize {
    const START: &str = "AA";

    process(valves.clone(), START.to_string(), 0, 0, vec![])
}

fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let data = read("src/input").unwrap();

    // assert_eq!(part1(&data), 0);
    // assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    // assert_eq!(part1(&data), 1651);
    // assert_eq!(part2(&data), 0);
}
