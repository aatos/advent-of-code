use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;
use std::collections::HashSet;


#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            _ => todo!(),
        })
    }
}
type Inner = (Direction, usize);
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let v = x.split(' ').collect::<Vec<_>>();
                (v[0].parse().unwrap(),
                 v[1].parse().unwrap())
            })
            .collect()
    })
}

fn update_tail(head: (usize, usize), tail: (usize, usize)) -> (usize, usize) {
    if (head.0 as i64 - tail.0 as i64).abs() <= 1 && (head.1 as i64 - tail.1 as i64).abs() <= 1 {
        tail
    } else {
        let f = |t, h| if t < h {
            t + 1
        } else if t > h {
            t - 1
        } else {
            t
        };

        (f(tail.0, head.0), f(tail.1, head.1))
    }
}

fn update_head(d: &Direction, head: (usize, usize)) -> (usize, usize) {
    match d {
        Direction::Up => {
            (head.0 + 1, head.1)
        }
        Direction::Down => {
            (head.0 - 1, head.1)
        }
        Direction::Left => {
            (head.0, head.1 - 1)
        }
        Direction::Right => {
            (head.0, head.1 + 1)
        }
    }
}

fn part1(c: &Container) -> usize {
    traverse(c, 1)
}

fn part2(c: &Container) -> usize {
    traverse(c, 9)
}

fn traverse(c: &Container, tail_count: usize) -> usize {
    let mut visited = HashSet::new();
    let mut head = (1000, 1000);
    let mut tails = vec![(1000, 1000); tail_count];

    for (d, l) in c {
        for _ in 0..*l {
            head = update_head(d, head);
            tails[0] = update_tail(head, tails[0]);
            for i in 1..tails.len() {
                tails[i] = update_tail(tails[i - 1], tails[i]);
            }
            visited.insert(tails.last().unwrap().clone());
        }
    }

    visited.len()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 6332);
    assert_eq!(part2(&data), 2511);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();
    assert_eq!(part1(&data), 13);
    assert_eq!(part2(&data), 1);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();
    assert_eq!(part2(&data), 36);
}
