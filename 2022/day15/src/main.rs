use rayon::prelude::*;
use regex::Regex;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = i64;
type Container = Vec<((Inner, Inner), (Inner, Inner))>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let re = Regex::new(
            r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)",
        )
        .unwrap();

        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let r = re
                    .captures(&x.unwrap())
                    .iter()
                    .flat_map(|c| {
                        c.iter()
                            .skip(1)
                            .map(|x| x.unwrap().as_str().parse::<Inner>().unwrap())
                            .collect::<Vec<_>>()
                    })
                    .collect::<Vec<_>>();

                ((r[0], r[1]), (r[2], r[3]))
            })
            .collect()
    })
}

fn part1(c: &Container, row: Inner) -> usize {
    let mut not_beacons: HashSet<Inner> = HashSet::new();

    c.iter().for_each(|&((sx, sy), (bx, by))| {
        let beacon_dist = (sx - bx).abs() + (sy - by).abs();
        let row_dist = (row - sy).abs();

        if row_dist <= beacon_dist {
            for x in (sx - beacon_dist + row_dist)..=(sx + beacon_dist - row_dist) {
                if bx != x || row != by {
                    not_beacons.insert(x);
                }
            }
        }
    });

    not_beacons.len()
}

fn part2(c: &Container, start: Inner, end: Inner) -> Inner {
    let mut x = start;

    let row = (start..=end)
        .find(|row| 'again: loop {
            for &((sx, sy), (bx, by)) in c {
                let beacon_dist = (sx - bx).abs() + (sy - by).abs();
                let row_dist = (row - sy).abs();

                if row_dist <= beacon_dist {
                    let min_x = sx - beacon_dist + row_dist;
                    let max_x = sx + beacon_dist - row_dist;

                    if min_x <= x && x <= max_x {
                        x = max_x + 1;
                        if x > end {
                            x = start;
                            return false;
                        } else {
                            continue 'again;
                        }
                    }
                }
            }
            return true;
        })
        .unwrap();

    x * 4_000_000 + row
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data, 2_000_000), 4725496);
    assert_eq!(part2(&data, 0, 4_000_000), 12051287042458);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data, 10), 26);
    assert_eq!(_part1_brute_force(&data, 10), 26);
    assert_eq!(part2(&data, 0, 20), 56000011);
    assert_eq!(_part2_brute_force(&data, 0, 20), 56000011);
}

// --------------------------------------------------------------

fn _print(c: &Container, not_beacons: &HashSet<(Inner, Inner)>) {
    const SIZE_X: Inner = 0;
    const SIZE_Y: Inner = 20;

    let beacons = c
        .iter()
        .map(|&(_, (bx, by))| (bx, by))
        .collect::<HashSet<_>>();
    let signals = c
        .iter()
        .map(|&((sx, sy), _)| (sx, sy))
        .collect::<HashSet<_>>();

    print!("    ");
    for j in SIZE_X..SIZE_Y {
        print!("{:1}", (j as i32).abs() % 10);
    }
    println!();

    for i in SIZE_X..SIZE_Y {
        print!("{:3} ", i);

        for j in SIZE_X..SIZE_Y {
            if beacons.contains(&(j, i)) {
                print!("B");
            } else if signals.contains(&(j, i)) {
                print!("S");
            } else if not_beacons.contains(&(j, i)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
    print!("    ");
    for j in SIZE_X..SIZE_Y {
        print!("{:1}", (j as i32).abs() % 10);
    }
    println!();
}

fn _part1_brute_force(c: &Container, row: Inner) -> usize {
    let mut not_beacons: HashSet<(Inner, Inner)> = HashSet::new();

    c.iter().for_each(|&((sx, sy), (bx, by))| {
        let dist = (sx - bx).abs() + (sy - by).abs() + 1;
        let mut q = vec![(sx, sy, dist)];

        while !q.is_empty() {
            let (x, y, dist) = q.pop().unwrap();
            not_beacons.insert((x, y));
            if dist != 1 {
                q.push((x + 1, y, dist - 1));
                q.push((x - 1, y, dist - 1));
                q.push((x, y + 1, dist - 1));
                q.push((x, y - 1, dist - 1));
            }
        }
        not_beacons.remove(&(bx, by));
    });

    _print(c, &not_beacons);

    not_beacons.iter().filter(|(_, y)| *y == row).count()
}

fn _part2_brute_force(c: &Container, start: Inner, end: Inner) -> usize {
    fn find_not(c: &Container, row: Inner, start: Inner, end: Inner) -> HashSet<Inner> {
        let mut not_beacons: HashSet<Inner> = HashSet::new();

        c.iter().for_each(|&((sx, sy), (bx, by))| {
            let beacon_dist = (sx - bx).abs() + (sy - by).abs();
            let row_dist = (row - sy).abs();

            if row_dist <= beacon_dist {
                let min_x = sx - beacon_dist + row_dist;
                let max_x = sx + beacon_dist - row_dist;

                for x in start.max(min_x)..=end.min(max_x) {
                    if bx != x || row != by {
                        not_beacons.insert(x);
                    }
                }
            }
        });

        not_beacons
    }

    let not_here = c
        .iter()
        .map(|&(_, (bx, by))| (bx, by))
        .chain(c.iter().map(|&((sx, sy), _)| (sx, sy)))
        .collect::<HashSet<_>>();

    let not_here = (start..=end)
        .map(|row| {
            not_here
                .iter()
                .filter_map(|&(i, j)| if row == j { Some(i) } else { None })
                .collect::<HashSet<_>>()
        })
        .collect::<Vec<_>>();

    (start..=end)
        .collect::<Vec<_>>()
        .par_iter()
        .find_map_first(|&row| {
            println!("checking row {}", row);
            let mut not_beacons = find_not(c, row, start, end);
            if not_beacons.len() != ((end - start).abs() + 1) as usize {
                not_beacons.extend(not_here[row as usize].clone());
                if not_beacons.len() != ((end - start).abs() + 1) as usize {
                    (start..=end).find_map(|j| {
                        if !not_beacons.contains(&j) {
                            println!("found {},{}", j, row);
                            Some((j * 4000000 + row) as usize)
                        } else {
                            None
                        }
                    })
                } else {
                    None
                }
            } else {
                None
            }
        })
        .unwrap()
}
