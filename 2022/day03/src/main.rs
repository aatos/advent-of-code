use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = char;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect::<Vec<Inner>>())
            .collect()
    })
}

fn char_to_prio(c: char) -> usize {
    if c.is_lowercase() {
        // 1 - 26
        c as usize - 'a' as usize + 1
    } else {
        // 27 - 52
        c as usize - 'A' as usize + 27
    }
}

fn part1(rucksacks: &Container) -> usize {
    rucksacks
        .iter()
        .map(|s| {
            let (first, second) = s.split_at(s.len() / 2);
            let second = second.iter().collect::<HashSet<_>>();

            let both = first.iter().find(|x| second.contains(x)).unwrap();

            char_to_prio(*both)
        })
        .sum()
}

fn part2(rucksacks: &Container) -> usize {
    rucksacks
        .chunks(3)
        .map(|g| {
            let first = &g[0];
            let second = g[1].iter().collect::<HashSet<_>>();
            let third = g[2].iter().collect::<HashSet<_>>();

            let both = first
                .iter()
                .find(|x| second.contains(x) && third.contains(x))
                .unwrap();

            char_to_prio(*both)
        })
        .sum()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 7581);
    assert_eq!(part2(&foo), 2525);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(char_to_prio('a'), 1);
    assert_eq!(char_to_prio('A'), 27);

    assert_eq!(part1(&foo), 157);
    assert_eq!(part2(&foo), 70);
}
