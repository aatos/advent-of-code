use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Elf>;

struct Elf {
    cals: Vec<Inner>,
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let (mut elves, last_cals) =
            io::BufReader::new(file)
                .lines()
                .fold((vec![], vec![]), |(mut elves, mut cals), x| {
                    let x = x.unwrap();

                    if x.is_empty() {
                        elves.push(Elf { cals });
                        (elves, vec![])
                    } else {
                        let cal = x.parse::<Inner>().unwrap();
                        cals.push(cal);
                        (elves, cals)
                    }
                });

        elves.push(Elf { cals: last_cals });
        elves
    })
}

fn part1(elves: &Container) -> Inner {
    elves.iter().map(|e| e.cals.iter().sum()).max().unwrap()
}

fn part2(elves: &Container) -> Inner {
    let calories: BinaryHeap<_> = elves.iter().map(|e| e.cals.iter().sum()).collect();

    calories.iter().take(3).sum()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 67450);
    assert_eq!(part2(&foo), 199357);
}
