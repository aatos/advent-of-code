use std::fs::File;
use std::io::{self, BufRead};

type Inner = u32;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .map(|c| c.to_digit(10).unwrap())
                    .collect::<Vec<_>>()
            })
            .collect()
    })
}

fn part1(c: &Container) -> usize {
    let mut count = 0;

    for i in 1..(c.len() - 1) {
        for j in 1..(c[i].len() - 1) {
            let tree = c[i][j];

            let up = (0..i).any(|ii| tree <= c[ii][j]);
            let down = ((i + 1)..c.len()).any(|ii| tree <= c[ii][j]);
            let left = (0..j).any(|jj| tree <= c[i][jj]);
            let right = ((j + 1)..c[i].len()).any(|jj| tree <= c[i][jj]);

            if !(up && down && left && right) {
                count += 1;
            }
        }
    }

    count + 2 * c.len() + 2 * (c[0].len() - 2)
}

fn part2(c: &Container) -> usize {
    let mut max = 0;

    for i in 1..(c.len() - 1) {
        for j in 1..(c[i].len() - 1) {
            let tree = c[i][j];

            let mut up = 0;
            for ii in (0..i).rev() {
                up += 1;
                if tree <= c[ii][j] {
                    break;
                }
            }

            let mut down = 0;
            for ii in (i + 1)..c.len() {
                down += 1;
                if tree <= c[ii][j] {
                    break;
                }
            }

            let mut left = 0;
            for jj in (0..j).rev() {
                left += 1;
                if tree <= c[i][jj] {
                    break;
                }
            }

            let mut right = 0;
            for jj in (j + 1)..c[i].len() {
                right += 1;
                if tree <= c[i][jj] {
                    break;
                }
            }

            max = std::cmp::max(max, up * down * right * left);
        }
    }

    max
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 1843);
    assert_eq!(part2(&data), 180000);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 21);
    assert_eq!(part2(&data), 8);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();

    assert_eq!(part2(&data), 8);
}
