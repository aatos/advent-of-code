use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Debug)]
enum Instruction {
    Noop,
    Addx(i64),
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let v = s.split(' ').collect::<Vec<_>>();
        Ok(match v[0] {
            "noop" => Instruction::Noop,
            "addx" => Instruction::Addx(v[1].parse().unwrap()),
            _ => todo!(),
        })
    }
}

type Inner = Instruction;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn part1(c: &Container) -> usize {
    c.iter()
        .fold((0, 1, 1), |(mut signal, mut cycle, mut x), instruction| {
            if cycle >= 20 && (cycle - 20) % 40 == 0 {
                signal += cycle * x;
            }

            match instruction {
                Instruction::Noop => (),
                Instruction::Addx(u) => {
                    cycle += 1;
                    if cycle >= 20 && (cycle - 20) % 40 == 0 {
                        signal += cycle * x;
                    }
                    x += *u;
                }
            };

            (signal, cycle + 1, x)
        })
        .0 as usize
}

fn part2(c: &Container) -> Vec<String> {
    fn draw(v: &mut Vec<String>, cycle: i64, x: i64) {
        let pos = (cycle - 1) % 40;
        let pixel = if ((x - 1)..=(x + 1)).contains(&pos) {
            '#'
        } else {
            '.'
        };
        v.last_mut().unwrap().push(pixel);
        if cycle % 40 == 0 {
            v.push(String::new());
        }
    }

    c.iter()
        .fold::<(Vec<String>, _, _), _>(
            (vec![String::new()], 1, 1),
            |(mut v, mut cycle, mut x), instruction| {
                draw(&mut v, cycle, x);

                match instruction {
                    Instruction::Noop => (),
                    Instruction::Addx(u) => {
                        cycle += 1;
                        draw(&mut v, cycle, x);

                        x += *u;
                    }
                };

                (v, cycle + 1, x)
            },
        )
        .0
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 15140);
    assert_eq!(
        part2(&data),
        vec![
            "###..###....##..##..####..##...##..###..",
            "#..#.#..#....#.#..#....#.#..#.#..#.#..#.",
            "###..#..#....#.#..#...#..#....#..#.#..#.",
            "#..#.###.....#.####..#...#.##.####.###..",
            "#..#.#....#..#.#..#.#....#..#.#..#.#....",
            "###..#.....##..#..#.####..###.#..#.#....",
            ""
        ]
    );
}

#[test]
fn test_input0() {
    let data = read("src/test0").unwrap();

    assert_eq!(part1(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 13140);
    assert_eq!(
        part2(&data),
        vec![
            "##..##..##..##..##..##..##..##..##..##..",
            "###...###...###...###...###...###...###.",
            "####....####....####....####....####....",
            "#####.....#####.....#####.....#####.....",
            "######......######......######......####",
            "#######.......#######.......#######.....",
            ""
        ]
    );
}
