use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
struct Point {
    x: Inner,
    y: Inner,
    z: Inner,
}

type Inner = i64;
type Container = Vec<Point>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let x = x
                    .split(',')
                    .map(|x| x.parse::<Inner>().unwrap())
                    .collect::<Vec<_>>();
                Point {
                    x: x[0],
                    y: x[1],
                    z: x[2],
                }
            })
            .collect()
    })
}

fn part1(c: &Container) -> usize {
    let map: HashSet<Point> = HashSet::from_iter(c.iter().cloned());

    c.iter().fold(0, |acc, p| {
        acc + [
            (1, 0, 0),
            (-1, 0, 0),
            (0, 1, 0),
            (0, -1, 0),
            (0, 0, 1),
            (0, 0, -1),
        ]
        .iter()
        .fold(6, |acc, (dx, dy, dz)| {
            if map.contains(&Point {
                x: p.x + dx,
                y: p.y + dy,
                z: p.z + dz,
            }) {
                acc - 1
            } else {
                acc
            }
        })
    })
}

fn part2(c: &Container) -> usize {
    let map: HashSet<Point> = HashSet::from_iter(c.iter().cloned());
    let max = Point {
        x: c.iter().map(|p| p.x).max().unwrap() + 1,
        y: c.iter().map(|p| p.y).max().unwrap() + 1,
        z: c.iter().map(|p| p.z).max().unwrap() + 1,
    };
    let min = Point {
        x: c.iter().map(|p| p.x).min().unwrap() - 1,
        y: c.iter().map(|p| p.y).min().unwrap() - 1,
        z: c.iter().map(|p| p.z).min().unwrap() - 1,
    };

    let mut q = VecDeque::from([min]);
    let mut visited = HashSet::new();
    let mut count = 0;
    while let Some(p) = q.pop_front() {
        if visited.contains(&p) {
            continue;
        }

        visited.insert(p);

        let directions = vec![
            (1, 0, 0),
            (-1, 0, 0),
            (0, 1, 0),
            (0, -1, 0),
            (0, 0, 1),
            (0, 0, -1),
        ];
        for (dx, dy, dz) in directions {
            let p = Point {
                x: p.x + dx,
                y: p.y + dy,
                z: p.z + dz,
            };
            if p.x >= min.x
                && p.y >= min.y
                && p.z >= min.z
                && p.x <= max.x
                && p.y <= max.y
                && p.z <= max.z
            {
                if map.contains(&p) {
                    count += 1;
                } else {
                    q.push_back(p);
                }
            }
        }
    }
    count
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 4332);
    assert_eq!(part2(&data), 2524);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 64);
    assert_eq!(part2(&data), 58);
}
