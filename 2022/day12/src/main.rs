use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect::<Vec<_>>())
            .collect()
    })
}

const START: char = 'S';
const END: char = 'E';

fn can_move(c: &Container, pos: (usize, usize), dest: (usize, usize)) -> bool {
    if dest.0 >= c.len() || dest.1 >= c[0].len() || pos.0 >= c.len() || pos.1 >= c[0].len() {
        false
    } else {
        let mut pos = c[pos.0][pos.1];
        let mut dest = c[dest.0][dest.1];

        if pos == START {
            pos = 'a';
        }

        if dest == END {
            dest = 'z';
        }

        let pos = pos as u32;
        let dest = dest as u32;

        pos >= dest || pos + 1 == dest
    }
}

fn find(c: &Container, spot: char) -> (usize, usize) {
    (0..c.len())
        .find_map(|i| (0..c[i].len()).find(|&j| c[i][j] == spot).map(|j| (i, j)))
        .unwrap()
}

fn bfs(
    c: &Container,
    start: char,
    end: char,
    is_adjacent: fn(&Container, (usize, usize), (usize, usize)) -> bool,
) -> usize {
    let mut q = VecDeque::new();
    let mut seen = HashSet::new();

    let (i, j) = find(c, start);

    q.push_back(((i, j), 0));
    seen.insert((i, j));

    while let Some(((i, j), len)) = q.pop_front() {
        if c[i][j] == end {
            return len;
        }

        vec![
            (i + 1, j),
            (i.wrapping_sub(1), j),
            (i, j + 1),
            (i, j.wrapping_sub(1)),
        ]
        .iter()
        .for_each(|&(ii, jj)| {
            if is_adjacent(c, (i, j), (ii, jj)) && !seen.contains(&(ii, jj)) {
                seen.insert((ii, jj));
                q.push_back(((ii, jj), len + 1));
            }
        });
    }
    todo!()
}

fn part1(c: &Container) -> usize {
    bfs(c, START, END, can_move)
}

fn part2(c: &Container) -> usize {
    bfs(c, END, 'a', |c, pos, dest| can_move(c, dest, pos))
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 484);
    assert_eq!(part2(&data), 478);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 31);
    assert_eq!(part2(&data), 29);
}
