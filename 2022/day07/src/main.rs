use std::fs::File;
use std::io::{self, BufRead};

#[derive(Debug)]
struct Node {
    _name: String,
    files: Vec<Node>,
    size: usize,
}

type Container = Node;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut tree = io::BufReader::new(file).lines().fold(
            vec![Node {
                _name: "/".to_string(),
                files: vec![],
                size: 0,
            }],
            |mut tree, line| {
                let line = line.unwrap();
                let cmd = line.split(" ").collect::<Vec<_>>();

                if cmd[0].starts_with('$') {
                    match cmd[1] {
                        "ls" => (),
                        "cd" => match cmd[2] {
                            ".." => {
                                let subdir: Node = tree.pop().unwrap();
                                tree.last_mut().unwrap().files.push(subdir);
                            }
                            "/" => {
                                while tree.len() != 1 {
                                    let subdir: Node = tree.pop().unwrap();
                                    tree.last_mut().unwrap().files.push(subdir);
                                }
                            }
                            dir => tree.push(Node {
                                _name: dir.to_string(),
                                files: vec![],
                                size: 0,
                            }),
                        },
                        _ => todo!(),
                    }
                    tree
                } else {
                    match cmd[0] {
                        "dir" => (),
                        size => tree.last_mut().unwrap().files.push(Node {
                            _name: cmd[1].to_string(),
                            files: vec![],
                            size: size.parse().unwrap(),
                        }),
                    }
                    tree
                }
            },
        );

        while tree.len() != 1 {
            let subdir: Node = tree.pop().unwrap();
            tree.last_mut().unwrap().files.push(subdir);
        }

        tree.pop().unwrap()
    })
}

fn recur(c: &Container, f: fn(usize) -> bool) -> (usize, Vec<usize>) {
    if c.files.is_empty() {
        (c.size, vec![])
    } else {
        let (size, mut dirs) = c.files.iter().fold((0, vec![]), |(sum, mut v), fi| {
            let (s, mut dirs) = recur(fi, f);
            v.append(&mut dirs);
            (sum + s, v)
        });

        if f(size) {
            dirs.push(size)
        }

        (size, dirs)
    }
}

fn part1(c: &Container) -> usize {
    const SIZE_LIMIT: usize = 100000;

    let (_, dirs) = recur(&c, |s| s <= SIZE_LIMIT);

    dirs.iter().sum()
}

fn part2(c: &Container) -> usize {
    const DISK_SPACE: usize = 70000000;
    const NEEDED_SPACE: usize = 30000000;

    let (_, mut dirs) = recur(&c, |_| true);

    dirs.sort();

    let free_space = DISK_SPACE - dirs.last().unwrap();

    *dirs
        .iter()
        .find(|s| free_space + *s >= NEEDED_SPACE)
        .unwrap()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 1334506);
    assert_eq!(part2(&data), 7421137);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();
    assert_eq!(part1(&data), 95437);
    assert_eq!(part2(&data), 24933642);
}
