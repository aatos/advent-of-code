use std::collections::BinaryHeap;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Clone)]
struct Monkey {
    items: VecDeque<Inner>,
    operation: fn(Inner, Inner) -> Inner,
    operand: Inner,
    divisor: Inner,
    true_monkey: usize,
    false_monkey: usize,
    inspects: usize,
}

impl Monkey {
    fn new() -> Monkey {
        Monkey {
            items: VecDeque::new(),
            operation: |x, _y| x,
            operand: 0,
            divisor: 1,
            true_monkey: 0,
            false_monkey: 0,
            inspects: 0,
        }
    }
}

type Inner = u64;
type Container = Vec<Monkey>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .fold(vec![], |mut monkeys, l| {
                let l = l.unwrap();

                if l.trim().is_empty() {
                } else if l.starts_with("Monkey ") {
                    monkeys.push(Monkey::new());
                } else if l.starts_with("  Starting items: ") {
                    monkeys.last_mut().unwrap().items = l
                        .strip_prefix("  Starting items: ")
                        .unwrap()
                        .split(", ")
                        .map(|w| w.parse::<Inner>().unwrap())
                        .collect();
                } else if l.starts_with("  Operation: new = old ") {
                    let ops = l
                        .strip_prefix("  Operation: new = old ")
                        .unwrap()
                        .split(' ')
                        .collect::<VecDeque<_>>();

                    let m = &mut monkeys.last_mut().unwrap();

                    m.operation = match (ops[0], ops[1]) {
                        ("*", "old") => |x, _y| x * x,
                        ("+", "old") => |x, _y| x + x,
                        ("*", o) => {
                            m.operand = o.parse::<Inner>().unwrap();
                            |x, y| x * y
                        }
                        ("+", o) => {
                            m.operand = o.parse::<Inner>().unwrap();
                            |x, y| x + y
                        }
                        _ => todo!(),
                    };
                } else if l.starts_with("  Test: divisible by ") {
                    monkeys.last_mut().unwrap().divisor = l
                        .strip_prefix("  Test: divisible by ")
                        .unwrap()
                        .parse::<Inner>()
                        .unwrap();
                } else if l.starts_with("    If ") {
                    let idx = l.split(' ').last().unwrap().parse::<usize>().unwrap();
                    let m = &mut monkeys.last_mut().unwrap();
                    if l.contains("true") {
                        m.true_monkey = idx;
                    } else {
                        m.false_monkey = idx;
                    }
                } else {
                    todo!()
                }

                monkeys
            })
    })
}

fn process(monkeys: &mut Container, rounds: usize, worrier: Box<dyn Fn(Inner) -> Inner>) -> usize {
    for _ in 0..rounds {
        for midx in 0..monkeys.len() {
            for item in &monkeys[midx].items.clone() {
                let new_level = worrier((monkeys[midx].operation)(*item, monkeys[midx].operand));
                let idx = if new_level % monkeys[midx].divisor == 0 {
                    monkeys[midx].true_monkey
                } else {
                    monkeys[midx].false_monkey
                };

                monkeys[idx].items.push_back(new_level);
                monkeys[midx].items.pop_front();
                monkeys[midx].inspects += 1;
            }
        }
    }

    let mut prio = monkeys
        .iter()
        .map(|m| m.inspects)
        .collect::<BinaryHeap<_>>();
    prio.pop().unwrap() * prio.pop().unwrap()
}

fn part1(mut monkeys: Container) -> usize {
    process(&mut monkeys, 20, Box::new(|level| level / 3))
}

fn part2(mut monkeys: Container) -> usize {
    let lcm: Inner = monkeys.iter().map(|m| m.divisor).product();

    process(&mut monkeys, 10_000, Box::new(move |level| level % lcm))
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(data.clone()), 54253);
    assert_eq!(part2(data), 13119526120);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(data.clone()), 10605);
    assert_eq!(part2(data), 2713310158);
}
