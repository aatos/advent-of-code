use std::fs::File;
use std::io::{self, BufRead};

type Inner = usize;
type Container = Vec<Vec<(Inner, Inner)>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();

                x.split(" -> ")
                    .map(|p| {
                        let coords = p
                            .split(',')
                            .map(|c| c.parse::<Inner>().unwrap())
                            .collect::<Vec<_>>();
                        (coords[0], coords[1])
                    })
                    .collect()
            })
            .collect()
    })
}

const SIZE: usize = 1000;
const SAND_POINT: (Inner, Inner) = (500, 0);
const FLOOR: char = '#';
const AIR: char = '.';
const SAND: char = 'o';

fn _print(cave: &Vec<Vec<char>>) {
    for i in 0..170 {
        println!("{}", cave[i].iter().skip(480).take(80).collect::<String>());
    }
}

fn make_cave(paths: &Container) -> Vec<Vec<char>> {
    let mut cave = vec![vec![AIR; SIZE]; SIZE];
    for path in paths {
        for p in path.windows(2) {
            let (y1, x1) = p[0];
            let (y2, x2) = p[1];

            if x1 == x2 {
                let start = y1.min(y2);
                let end = y1.max(y2);

                for y in start..=end {
                    cave[x1][y] = FLOOR;
                }
            } else {
                let start = x1.min(x2);
                let end = x1.max(x2);

                for x in start..=end {
                    cave[x][y1] = FLOOR;
                }
            }
        }
    }

    cave
}

fn simulate(mut cave: Vec<Vec<char>>) -> usize {
    let f = || loop {
        let (mut y, x) = SAND_POINT;
        if cave[x][y] != AIR {
            return None;
        }

        for x in x.. {
            if x >= SIZE {
                return None;
            }

            if cave[x][y] != AIR {
                if cave[x][y - 1] == AIR {
                    y -= 1;
                } else if cave[x][y + 1] == AIR {
                    y += 1;
                } else {
                    cave[x - 1][y] = SAND;
                    return Some(1);
                }
            }
        }
    };

    std::iter::from_fn(f).count()
}

fn part1(paths: &Container) -> usize {
    let cave = make_cave(paths);

    simulate(cave)
}

fn part2(paths: &Container) -> usize {
    const FLOOR_DELTA: usize = 2;

    let mut cave = make_cave(paths);

    let floor_pos = paths
        .iter()
        .map(|path| path.iter().map(|(_, x)| x).max())
        .flatten()
        .max()
        .unwrap()
        + FLOOR_DELTA;

    cave[floor_pos] = vec![FLOOR; SIZE];

    simulate(cave)
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 655);
    assert_eq!(part2(&data), 26484);
}

#[test]
fn test_input1() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 24);
}

#[test]
fn test_input2() {
    let data = read("src/test").unwrap();

    assert_eq!(part2(&data), 93);
}
