use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Point {
    x: usize,
    y: usize,
}

enum Jet {
    Left,
    Right,
}

#[derive(Copy, Clone, Debug)]
enum RockType {
    Flat,
    Plus,
    L,
    I,
    Square,
}

impl RockType {
    fn height(&self) -> usize {
        match self {
            RockType::Flat => 0,
            RockType::Plus | RockType::Square => 1,
            RockType::L => 3,
            RockType::I => 4,
        }
    }

    fn width(&self) -> usize {
        match self {
            RockType::Flat => 3,
            RockType::Plus | RockType::Square => 1,
            RockType::L => 2,
            RockType::I => 0,
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Rock {
    kind: RockType,
    pos: Point,
    is_moving: bool,
}

const WIDTH: usize = 7;

impl Rock {
    fn new(kind: RockType, pos: Point) -> Self {
        Rock {
            kind,
            pos,
            is_moving: true,
        }
    }

    fn points(&self) -> HashSet<Point> {
        match self.kind {
            RockType::Flat => HashSet::from([
                Point {
                    x: self.pos.x,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 2,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 3,
                    y: self.pos.y,
                },
            ]),
            RockType::Plus => HashSet::from([
                Point {
                    x: self.pos.x,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 2,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y + 1,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y - 1,
                },
            ]),
            RockType::L => HashSet::from([
                Point {
                    x: self.pos.x,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 2,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 2,
                    y: self.pos.y + 1,
                },
                Point {
                    x: self.pos.x + 2,
                    y: self.pos.y + 2,
                },
            ]),
            RockType::I => HashSet::from([
                Point {
                    x: self.pos.x,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x,
                    y: self.pos.y + 1,
                },
                Point {
                    x: self.pos.x,
                    y: self.pos.y + 2,
                },
                Point {
                    x: self.pos.x,
                    y: self.pos.y + 3,
                },
            ]),
            RockType::Square => HashSet::from([
                Point {
                    x: self.pos.x,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y,
                },
                Point {
                    x: self.pos.x,
                    y: self.pos.y + 1,
                },
                Point {
                    x: self.pos.x + 1,
                    y: self.pos.y + 1,
                },
            ]),
        }
    }

    fn move_right(&mut self, rocks: &HashSet<Point>) -> bool {
        let m = match self.kind {
            RockType::Flat => self.pos.x + 3 + 1 >= WIDTH,
            RockType::Plus => self.pos.x + 2 + 1 >= WIDTH,
            RockType::L => self.pos.x + 2 + 1 >= WIDTH,
            RockType::I => self.pos.x + 1 >= WIDTH,
            RockType::Square => self.pos.x + 1 + 1 >= WIDTH,
        };
        if m {
            return false;
        }

        self.pos.x += 1;

        let intersects = self.points().intersection(rocks).next().is_some();

        if intersects {
            self.pos.x -= 1;
        }

        !intersects
    }

    fn move_left(&mut self, rocks: &HashSet<Point>) -> bool {
        if self.pos.x < 1 {
            false
        } else {
            self.pos.x -= 1;

            let intersects = self.points().intersection(rocks).next().is_some();

            if intersects {
                self.pos.x += 1;
            }

            !intersects
        }
    }

    fn move_down(&mut self, rocks: &HashSet<Point>) -> bool {
        if self.pos.y < 1 {
            false
        } else {
            self.pos.y -= 1;

            let intersects =
                self.intersects_row(0) || self.points().intersection(rocks).next().is_some();

            if intersects {
                self.pos.y += 1;
            }
            !intersects
        }
    }

    fn intersects_row(&self, row: usize) -> bool {
        match self.kind {
            RockType::Flat => self.pos.y == row,
            RockType::Plus => ((self.pos.y - 1)..=(self.pos.y + 1)).contains(&row),
            RockType::L => ((self.pos.y)..=(self.pos.y + 2)).contains(&row),
            RockType::I => ((self.pos.y)..=(self.pos.y + 3)).contains(&row),
            RockType::Square => ((self.pos.y)..=(self.pos.y + 1)).contains(&row),
        }
    }

    fn intersects(&self, point: &Point) -> bool {
        match self.kind {
            RockType::Flat => {
                (self.pos.x..=(self.pos.x + 3)).contains(&point.x) && self.pos.y == point.y
            }
            RockType::Plus => {
                (self.pos.y - 1 == point.y && self.pos.x + 1 == point.x)
                    || (self.pos.y + 1 == point.y && self.pos.x + 1 == point.x)
                    || (self.pos.y == point.y && (self.pos.x..=self.pos.x + 2).contains(&point.x))
            }
            RockType::L => {
                (self.pos.y + 2 == point.y && self.pos.x + 2 == point.x)
                    || (self.pos.y + 1 == point.y && self.pos.x + 2 == point.x)
                    || (self.pos.y == point.y && (self.pos.x..=self.pos.x + 2).contains(&point.x))
            }
            RockType::I => {
                self.pos.x == point.x && (self.pos.y..=self.pos.y + 3).contains(&point.y)
            }
            RockType::Square => {
                (self.pos.y..=self.pos.y + 1).contains(&point.y)
                    && (self.pos.x..=self.pos.x + 1).contains(&point.x)
            }
        }
    }
}

type Inner = Jet;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                let x = x.unwrap();
                x.chars()
                    .map(|c| match c {
                        '<' => Jet::Left,
                        '>' => Jet::Right,
                        _ => todo!(),
                    })
                    .collect::<Vec<_>>()
            })
            .collect()
    })
}

// fn print(rock: Option<&Rock>, rocks: &HashSet<Rock>) {
//     let mut rocks = rocks.clone();
//     if let Some(rock) = rock {
//         rocks.push(rock.clone());
//     }

//     const MAX_ROW: usize = 100;

//     let row_map = (0..MAX_ROW)
//         .map(|row| {
//             (
//                 row,
//                 rocks
//                     .iter()
//                     .filter(|rock| rock.intersects_row(row))
//                     .copied()
//                     .collect::<Vec<_>>(),
//             )
//         })
//         .filter(|(_, rocks)| !rocks.is_empty())
//         .collect::<HashMap<usize, HashSet<Rock>>>();

//     let max_row = row_map
//         .iter()
//         .max_by(|(r1, _), (r2, _)| r1.cmp(&r2))
//         .map(|(row, rocks)| {
//             row + rocks
//                 .iter()
//                 .max_by_key(|rock| rock.kind.height())
//                 .map(|rock| rock.kind.height())
//                 .unwrap()
//         })
//         .unwrap_or(1);

//     for y in (1..=max_row).rev() {
//         print!("|");
//         let empty = vec![]; // TODO
//         let rocks = row_map.get(&y).unwrap_or(&empty);
//         for x in 0..WIDTH {
//             let point = Point { x, y };
//             if let Some(rock) = rocks.iter().find(|rock| rock.intersects(&point)) {
//                 if rock.is_moving {
//                     print!("@");
//                 } else {
//                     print!("#");
//                 }
//             } else {
//                 print!(".");
//             }
//         }
//         println!("|");
//     }

//     println!("+-------+");
// }

fn process(jets: &Container, count: u64) -> usize {
    const CACHE_SIZE: usize = 100;
    let rocks = [
        RockType::Flat,
        RockType::Plus,
        RockType::L,
        RockType::I,
        RockType::Square,
    ];
    let mut rock_type_iter = rocks.iter().cycle();
    let mut jet_iter = jets.iter().cycle();

    let mut highest_y = 0;
    let mut rocks = HashSet::new();

    for _ in 0..count {
        let next_type = *rock_type_iter.next().unwrap();

        let next_y = highest_y
            + match next_type {
                RockType::Plus => 5,
                _ => 4,
            };

        let mut rock = Rock::new(next_type, Point { x: 2, y: next_y });

        loop {
            match jet_iter.next().unwrap() {
                Jet::Left => {
                    rock.move_left(&rocks);
                }
                Jet::Right => {
                    rock.move_right(&rocks);
                }
            }

            let moved = rock.move_down(&rocks);
            if !moved {
                highest_y = std::cmp::max(
                    highest_y,
                    rock.pos.y
                        + match rock.kind {
                            RockType::Plus | RockType::Square => 1,
                            RockType::L => 2,
                            RockType::I => 3,
                            RockType::Flat => 0,
                        },
                );
                rock.is_moving = false;
                rocks.extend(&rock.points());
                if highest_y >= CACHE_SIZE {
                    rocks.retain(|p| ((highest_y - CACHE_SIZE)..=highest_y).contains(&p.y));
                }
                break;
            }
        }
    }

    highest_y
}

fn part1(c: &Container) -> usize {
    process(c, 2022)
}

fn part2(c: &Container) -> usize {
    process(c, 1_000_000_000_000_u64)
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 3109);
    //    assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 3068);
    //    assert_eq!(part2(&data), 1_514_285_714_288);
}
