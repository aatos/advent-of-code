use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

type Inner = char;
type Container = (Vec<Vec<Inner>>, Vec<Instruction>);

#[derive(Debug, Clone)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let line = s.split(' ').collect::<Vec<_>>();

        Ok(Instruction {
            count: line[1].parse().unwrap(),
            from: line[3].parse().unwrap(),
            to: line[5].parse().unwrap(),
        })
    }
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .fold((vec![], vec![]), |(mut stacks, mut ins), x| {
                let x = x.unwrap();

                if x.is_empty() || x.starts_with(" 1") {
                    (stacks, ins)
                } else if x.starts_with("move") {
                    let instruction = x.parse().unwrap();
                    ins.push(instruction);
                    (stacks, ins)
                } else {
                    if stacks.is_empty() {
                        let count = (x.len() + 1) / 4;
                        stacks = vec![vec![]; count];
                    }

                    x.chars().skip(1).step_by(4).enumerate().for_each(|(i, c)| {
                        if c != ' ' {
                            stacks[i].insert(0, c)
                        }
                    });

                    (stacks, ins)
                }
            })
    })
}

fn part1(c: Container) -> String {
    let (mut stacks, instructions) = c;

    instructions.iter().for_each(|ins| {
        for _ in 0..ins.count {
            let c = stacks[ins.from - 1].pop().unwrap();
            stacks[ins.to - 1].push(c);
        }
    });

    stacks
        .iter()
        .map(|stack| stack.last().unwrap())
        .collect::<String>()
}

fn part2(c: Container) -> String {
    let (mut stacks, instructions) = c;

    instructions.iter().for_each(|ins| {
        let from = &mut stacks[ins.from - 1];
        let mut cs = from.split_off(from.len() - ins.count);
        stacks[ins.to - 1].append(&mut cs);
    });

    stacks
        .iter()
        .map(|stack| stack.last().unwrap())
        .collect::<String>()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(foo.clone()), "BZLVHBWQF");
    assert_eq!(part2(foo), "TDGJQTZSL");
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(foo.clone()), "CMZ");
    assert_eq!(part2(foo), "MCD");
}
