#!/usr/bin/env bash

set -euo pipefail

day="${1:-$(date +'%-d')}"
year="${2:-$(date +'%Y')}"

tmpfile=$(mktemp /tmp/advent.XXXXXX)

code=$(curl -s -o "$tmpfile" -w "%{http_code}" -H "Cookie: session=${ADVENT_TOKEN}" "https://adventofcode.com/$year/day/$day/input")

if [ "$code" != "200" ]
then
    echo "Failed to get input, received $code"
    cat "$tmpfile"
    exit 1
fi

day=$(printf "%02d" "$day")

if ! [ -f Cargo.toml ]
then
    echo "[workspace]
resolver = \"2\"
members = [\"day$day\"]
" > Cargo.toml
elif ! grep -q "day$day" Cargo.toml
then
    gsed -i 's/"]/", "day'"$day"'"]/g' Cargo.toml
fi

cargo new "day$day"

mv "$tmpfile" "day$day/src/input"

echo "use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn part1(_: &Container) -> usize {
    panic!(\"part 1 unfinished\")
}

fn part2(_: &Container) -> usize {
    panic!(\"part 2 unfinished\")
}

fn main() {
    let data = read(\"src/input\").unwrap();

    assert_eq!(part1(&data), 0);
    assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read(\"src/test\").unwrap();

    assert_eq!(part1(&data), 0);
    assert_eq!(part2(&data), 0);
}
" > "day$day/src/main.rs"
