use std::fs::File;
use std::io::{self, BufRead};
use std::num::ParseIntError;
use std::str::FromStr;

enum OperationKind {
    Forward(i32),
    Up(i32),
    Down(i32),
}

impl FromStr for OperationKind {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let x = s.split(' ').collect::<Vec<_>>();
        let a = x[1].parse::<i32>()?;

        match x[0] {
            "forward" => Ok(OperationKind::Forward(a)),
            "up" => Ok(OperationKind::Up(a)),
            "down" => Ok(OperationKind::Down(a)),
            x => panic!("invalid operation: {}", x),
        }
    }
}

type Inner = OperationKind;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse().unwrap())
            .collect()
    })
}

fn part1(path: &Container) -> i32 {
    let (h, v) = path.iter().fold((0, 0), |(h, v), x| match x {
        OperationKind::Forward(a) => (h + a, v),
        OperationKind::Up(a) => (h, v - a),
        OperationKind::Down(a) => (h, v + a),
    });

    h * v
}

fn part2(path: &Container) -> i32 {
    let (h, v, _) = path.iter().fold((0, 0, 0), |(h, v, d), x| match x {
        OperationKind::Forward(a) => (h + a, v + d * a, d),
        OperationKind::Up(a) => (h, v, d - a),
        OperationKind::Down(a) => (h, v, d + a),
    });

    h * v
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 1947824);
    assert_eq!(part2(&foo), 1813062561);
}
