use std::cmp;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

use itertools::iproduct;

type Inner = u32;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .map(|x| x.to_digit(10).unwrap())
                    .collect()
            })
            .collect()
    })
}

fn neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    iproduct!(min_y..=max_y, min_x..=max_x).filter(move |&(j, i)| (i != x || j != y))
}

fn step(octopuses: &mut Container) -> usize {
    let x_size = octopuses[0].len();
    let y_size = octopuses.len();

    let mut flashes: HashSet<(usize, usize)> = HashSet::new();
    let mut to_visit: Vec<(usize, usize)> = iproduct!(0..y_size, 0..x_size)
        .filter(|&(y, x)| {
            octopuses[y][x] += 1;
            octopuses[y][x] > 9
        })
        .collect();

    while !to_visit.is_empty() {
        let (y, x) = to_visit.pop().unwrap();

        if octopuses[y][x] <= 9 {
            continue;
        }

        octopuses[y][x] = 0;

        flashes.insert((y, x));

        to_visit.extend(
            neighbors(y_size, x_size, y, x)
                .filter(|&(j, i)| !flashes.contains(&(j, i)))
                .filter(|&(j, i)| {
                    octopuses[j][i] += 1;
                    octopuses[j][i] > 9
                }),
        );
    }

    flashes.len()
}

fn part1(octopuses: &Container, steps: usize) -> usize {
    let mut octopuses = octopuses.clone();

    (0..steps).fold(0, |acc, _| acc + step(&mut octopuses))
}

fn part2(octopuses: &Container) -> usize {
    let mut octopuses = octopuses.clone();

    (1..)
        .find(|_| step(&mut octopuses) == octopuses.len() * octopuses[0].len())
        .unwrap()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo, 100), 1729);
    assert_eq!(part2(&foo), 237);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();
    assert_eq!(part1(&foo, 3), 9);

    let foo2 = read("src/test_2").unwrap();
    assert_eq!(part1(&foo2, 100), 1656);
    assert_eq!(part2(&foo2), 195);
}
