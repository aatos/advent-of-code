use std::cmp;
use std::collections::BinaryHeap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter;

use itertools::iproduct;

type Inner = u32;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .map(|x| x.to_digit(10).unwrap())
                    .collect()
            })
            .collect()
    })
}

fn non_diag_neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    (min_y..=max_y)
        .zip(iter::repeat(x))
        .chain(iter::repeat(y).zip(min_x..=max_x))
        .filter(move |&(j, i)| (i != x || j != y))
}

fn low_points(heights: &Container) -> impl Iterator<Item = (usize, usize)> + '_ {
    iproduct!(0..heights.len(), 0..heights[0].len()).filter(|&(y, x)| {
        non_diag_neighbors(heights.len(), heights[y].len(), y, x)
            .all(|(j, i)| heights[j][i] > heights[y][x])
    })
}

fn part1(heights: &Container) -> usize {
    low_points(heights)
        .map(|(y, x)| heights[y][x] + 1)
        .sum::<Inner>() as usize
}

fn count_basin(heights: &Container, low: (usize, usize)) -> usize {
    let mut seen: HashSet<(usize, usize)> = HashSet::new();
    let mut to_visit: Vec<(usize, usize)> = vec![low];

    while !to_visit.is_empty() {
        let (y, x) = to_visit.pop().unwrap();

        if heights[y][x] == 9 || seen.contains(&(y, x)) {
            continue;
        }

        seen.insert((y, x));

        to_visit.extend(non_diag_neighbors(heights.len(), heights[y].len(), y, x));
    }

    seen.len()
}

fn part2(heights: &Container) -> usize {
    let mut sizes = low_points(heights)
        .map(|c| count_basin(heights, c))
        .collect::<BinaryHeap<_>>();

    // `BinaryHeap::into_iter_sorted()` would be nice but it's nightly.
    (0..3).fold(1, |p, _| p * sizes.pop().unwrap())
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 535);
    assert_eq!(part2(&foo), 1122700);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 15);
    assert_eq!(part2(&foo), 1134);
}
