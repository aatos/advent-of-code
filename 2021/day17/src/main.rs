use std::cmp;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::ops::RangeInclusive;
use std::str::FromStr;

type Inner = i64;
type Container = Target;

struct Target {
    x: RangeInclusive<Inner>,
    y: RangeInclusive<Inner>,
}

impl Target {
    fn contains(&self, pos: (Inner, Inner)) -> bool {
        self.x.contains(&pos.0) && self.y.contains(&pos.1)
    }

    fn is_past(&self, pos: (Inner, Inner)) -> bool {
        pos.0 > *self.x.end() || pos.1 < *self.y.start()
    }
}

impl FromStr for Target {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn parse_coord(coords: &Vec<&str>, coord: &str) -> (Inner, Inner) {
            coords
                .iter()
                .find_map(|s| {
                    s.strip_prefix(coord)
                        .map(|x| {
                            x.split("..")
                                .map(|x| x.parse::<Inner>().unwrap())
                                .collect::<Vec<_>>()
                        })
                        .map(|x| (x[0], x[1]))
                })
                .unwrap()
        }

        let coords = s
            .strip_prefix("target area: ")
            .unwrap()
            .split(", ")
            .collect::<Vec<_>>();

        let (x_start, x_end) = parse_coord(&coords, "x=");
        let (y_start, y_end) = parse_coord(&coords, "y=");

        Ok(Target {
            x: x_start..=x_end,
            y: y_start..=y_end,
        })
    }
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .take(1)
            .map(|x| x.unwrap().parse().unwrap())
            .next()
            .unwrap()
    })
}

fn does_hit(target: &Container, start_velocity: (Inner, Inner)) -> Option<Inner> {
    let mut pos: (Inner, Inner) = (0, 0);

    let mut max_y = Inner::MIN;

    let mut velocity: (Inner, Inner) = start_velocity;
    while !target.contains(pos) && !target.is_past(pos) {
        max_y = cmp::max(max_y, pos.1);

        pos.0 += velocity.0;
        pos.1 += velocity.1;

        if velocity.0 < 0 {
            velocity.0 += 1;
        } else if velocity.0 > 0 {
            velocity.0 -= 1;
        }

        velocity.1 -= 1;
    }

    if !target.is_past(pos) {
        Some(max_y)
    } else {
        None
    }
}

fn part1(target: &Container) -> Inner {
    let mut x = 1;
    let mut y = 1;

    while does_hit(target, (x, y)).is_none() {
        x += 1;
    }

    let mut max_y = Inner::MIN;
    loop {
        y += 1;

        if let Some(hit_y) = does_hit(target, (x, y)) {
            max_y = cmp::max(max_y, hit_y);
        } else if y >= 200 {
            break;
        }
    }

    max_y
}

fn part2(target: &Container) -> usize {
    let hits: HashSet<(Inner, Inner)> = (0..500)
        .flat_map(|x| (-500..500).filter_map(move |y| does_hit(target, (x, y)).map(|_| (x, y))))
        .collect();

    hits.len()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 4278);
    assert_eq!(part2(&foo), 1994);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(does_hit(&foo, (6, 9)), Some(45));

    assert_eq!(part1(&foo), 45);
    assert_eq!(part2(&foo), 112);
}
