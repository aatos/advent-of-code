use itertools::iproduct;
use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Player = u64;
type Score = u64;
type Position = u64;
type Container = HashMap<Player, Score>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let x = x
                    .strip_prefix("Player ")
                    .unwrap()
                    .split(" starting position: ")
                    .collect::<Vec<_>>();

                (
                    x[0].parse::<Player>().unwrap(),
                    x[1].parse::<Score>().unwrap(),
                )
            })
            .collect()
    })
}

fn part1(positions: &Container) -> Score {
    let max_score = 1000;
    let max_die = 100;

    let mut die = 0;
    let mut rolls = 0;
    let mut scores = positions
        .iter()
        .map(|(p, _)| (*p, 0 as Score))
        .collect::<HashMap<Player, Score>>();

    let mut positions = positions.clone();

    'outer: loop {
        for player in 1..=2 {
            let pos: &mut _ = positions.entry(player).or_insert(0);
            let score: &mut _ = scores.entry(player).or_insert(0);

            for _ in 0..3 {
                rolls += 1;
                die = die % max_die + 1;
                *pos = (*pos + die - 1) % 10 + 1;
            }
            *score += *pos;
            if *score >= max_score {
                break 'outer;
            }
        }
    }

    scores
        .iter()
        .find_map(|(_, score)| if *score < 1000 { Some(*score) } else { None })
        .unwrap()
        * rolls
}

fn part2(positions: &Container) -> usize {
    fn recur(
        p1: (Position, Score),
        p2: (Position, Score),
        cache: &mut HashMap<((Position, Score), (Position, Score)), (usize, usize)>,
    ) -> (usize, usize) {
        if let Some(&wins) = cache.get(&(p1, p2)) {
            return wins;
        }

        let mut wins = (0, 0);

        for dice in iproduct!(1..=3, 1..=3, 1..=3) {
            let (pos, score) = p1;
            let pos = (pos + dice.0 + dice.1 + dice.2 - 1) % 10 + 1;
            let score = score + pos;

            if score >= 21 {
                wins = (wins.0 + 1, wins.1);
            } else {
                let (p2, p1) = recur(p2, (pos, score), cache);
                wins = (wins.0 + p1, wins.1 + p2);
            }
        }

        cache.insert((p1, p2), wins);

        return wins;
    }

    let mut cache: HashMap<((Position, Score), (Position, Score)), (usize, usize)> = HashMap::new();

    let wins = recur(
        (*positions.get(&1).unwrap(), 0 as Score),
        (*positions.get(&2).unwrap(), 0 as Score),
        &mut cache,
    );

    cmp::max(wins.0, wins.1)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 551901);
    assert_eq!(part2(&foo), 272847859601291);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 739785);
    assert_eq!(part2(&foo), 444356092776315);
}
