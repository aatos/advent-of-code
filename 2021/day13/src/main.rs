use itertools::iproduct;

use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

enum FoldType {
    Horizontal,
    Vertical,
}

impl FromStr for FoldType {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "y" => Ok(FoldType::Horizontal),
            "x" => Ok(FoldType::Vertical),
            x => panic!("invalid fold: {}", x),
        }
    }
}

type Paper = Vec<Vec<char>>;
type Fold = (FoldType, usize);

type Container = (Paper, Vec<Fold>);

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut coords: Vec<(usize, usize)> = vec![];
        let mut folds: Vec<Fold> = vec![];

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();

            if x.contains(',') {
                let coord = x
                    .split(',')
                    .map(|i| i.parse::<usize>().unwrap())
                    .collect::<Vec<usize>>();

                coords.push((coord[0], coord[1]));
            } else if let Some(fold) = x.strip_prefix("fold along ") {
                let fold = fold.split('=').collect::<Vec<&str>>();
                folds.push((fold[0].parse().unwrap(), fold[1].parse::<usize>().unwrap()));
            }
        });

        let &(max_y, _) = coords.iter().max_by_key(|&(y, _)| y).unwrap();
        let &(_, max_x) = coords.iter().max_by_key(|&(_, x)| x).unwrap();

        let mut map = vec![vec!['.'; max_y + 1]; max_x + 1];

        coords.iter().for_each(|&(x, y)| map[y][x] = '#');

        (map, folds)
    })
}

fn fold<'a, I>(paper: &Paper, folds: I) -> Paper
where
    I: Iterator<Item = &'a Fold>,
{
    folds.fold(paper.clone(), |paper, (axis, coord)| match axis {
        FoldType::Horizontal => {
            let bottom_len = paper.len() - coord - 1;
            // The paper doesn't always fold in half, so some magic is needed.
            let diff = coord - bottom_len;

            (0..*coord)
                .map(|y| {
                    (0..paper[0].len())
                        .map(|x| {
                            if y >= diff
                                && y < bottom_len
                                && paper[paper.len() - 1 - y + diff][x] == '#'
                            {
                                '#'
                            } else {
                                paper[y][x]
                            }
                        })
                        .collect()
                })
                .collect::<Paper>()
        }
        FoldType::Vertical => (0..paper.len())
            .map(|y| {
                (0..*coord)
                    .map(|x| {
                        if paper[y][paper[0].len() - 1 - x] == '#' {
                            '#'
                        } else {
                            paper[y][x]
                        }
                    })
                    .collect()
            })
            .collect::<Paper>(),
    })
}

fn part1(t: &Container) -> usize {
    let paper = fold(&t.0, t.1.iter().take(1));

    iproduct!(0..paper.len(), 0..paper[0].len())
        .filter(|&(y, x)| paper[y][x] == '#')
        .count()
}

fn part2(t: &Container) -> Paper {
    fold(&t.0, t.1.iter())
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 735);
    let part2_ans: Vec<Vec<char>> = vec![
        "#..#.####.###..####.#..#..##..#..#.####.".chars().collect(),
        "#..#.#....#..#....#.#.#..#..#.#..#....#.".chars().collect(),
        "#..#.###..#..#...#..##...#..#.#..#...#..".chars().collect(),
        "#..#.#....###...#...#.#..####.#..#..#...".chars().collect(),
        "#..#.#....#.#..#....#.#..#..#.#..#.#....".chars().collect(),
        ".##..#....#..#.####.#..#.#..#..##..####.".chars().collect(),
    ];

    assert_eq!(part2(&foo), part2_ans);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 17);
    let part2_ans: Vec<Vec<char>> = vec![
        "#####".chars().collect(),
        "#...#".chars().collect(),
        "#...#".chars().collect(),
        "#...#".chars().collect(),
        "#####".chars().collect(),
        ".....".chars().collect(),
        ".....".chars().collect(),
    ];

    assert_eq!(part2(&foo), part2_ans);
}
