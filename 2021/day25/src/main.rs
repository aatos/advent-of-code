use itertools::iproduct;
use std::fs::File;
use std::io::{self, BufRead};

#[derive(Copy, Clone)]
enum Type {
    Empty,
    East,
    South,
}

impl Type {
    fn new(c: char) -> Type {
        match c {
            '>' => Type::East,
            'v' => Type::South,
            '.' => Type::Empty,
            _ => panic!("invalid seacucumber"),
        }
    }
}

type Container = Vec<Vec<Type>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();

                x.chars().map(|x| Type::new(x)).collect::<Vec<Type>>()
            })
            .collect()
    })
}

fn next(x_size: usize, y_size: usize, x: usize, y: usize, c: &Type) -> (usize, usize) {
    match *c {
        Type::East => {
            if x + 1 >= x_size {
                (0, y)
            } else {
                (x + 1, y)
            }
        }
        Type::South => {
            if y + 1 >= y_size {
                (x, 0)
            } else {
                (x, y + 1)
            }
        }
        _ => (x, y),
    }
}

#[cfg(test)]
fn print(c: &Container) {
    iproduct!(0..c.len(), 0..c[0].len()).for_each(|(y, x)| {
        if x == 0 {
            println!();
        }

        print!(
            "{}",
            match c[y][x] {
                Type::East => '>',
                Type::South => 'v',
                Type::Empty => '.',
            }
        );
    });

    println!("\n");
}

fn part1(c: &Container) -> usize {
    let y_size = c.len();
    let x_size = c[0].len();

    let mut steps = 0;
    let mut c = c.clone();
    loop {
        let mut changed = false;

        let mut new = c.clone();
        iproduct!(0..y_size, 0..x_size)
            .filter(|&(y, x)| matches!(&c[y][x], Type::East))
            .for_each(|(y, x)| {
                if matches!(c[y][x], Type::East) {
                    let (nx, ny) = next(x_size, y_size, x, y, &c[y][x]);

                    if matches!(c[ny][nx], Type::Empty) {
                        new[ny][nx] = new[y][x];
                        new[y][x] = Type::Empty;
                        changed = true;
                    }
                }
            });

        c = new.clone();

        iproduct!(0..y_size, 0..x_size)
            .filter(|&(y, x)| matches!(&c[y][x], Type::South))
            .for_each(|(y, x)| {
                if matches!(c[y][x], Type::South) {
                    let (nx, ny) = next(x_size, y_size, x, y, &c[y][x]);

                    if matches!(c[ny][nx], Type::Empty) {
                        new[ny][nx] = new[y][x];
                        new[y][x] = Type::Empty;
                        changed = true;
                    }
                }
            });

        steps += 1;

        if !changed {
            break;
        }

        c = new;
    }

    steps
}

fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 482);
    // TODO: not finished
    // assert_eq!(part2(&foo), 0);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 58);
    //assert_eq!(part2(&foo), 0);
}
