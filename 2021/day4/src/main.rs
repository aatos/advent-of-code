use std::fmt;
use std::fs::File;
use std::io::{self, BufRead};
use std::num::ParseIntError;
use std::str::FromStr;

type Inner = u64;
type Container = Bingo;

type Board = Vec<Vec<Number>>;

#[derive(Clone, Copy)]
enum Number {
    Marked(Inner),
    Unmarked(Inner),
}

impl fmt::Debug for Number {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Number::Marked(i) => write!(f, "({: >4})", i),
            Number::Unmarked(i) => write!(f, "{: >5} ", i),
        }
    }
}

impl FromStr for Number {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let a = s.parse::<Inner>()?;
        Ok(Number::Unmarked(a))
    }
}

#[derive(Clone)]
struct Bingo {
    boards: Vec<Board>,
    numbers: Vec<Inner>,
    last_drawn: Option<Inner>,
}

impl fmt::Debug for Bingo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = String::new();

        for b in &self.boards {
            buf.push_str("\n");
            for column in b {
                buf.push_str(&format!("{:?}\n", column));
            }
        }
        write!(f, "Bingo {{\nnumbers: {:?}\n {}\n}}", self.numbers, buf)
    }
}

impl Bingo {
    fn new() -> Bingo {
        Bingo {
            boards: Vec::new(),
            numbers: Vec::new(),
            last_drawn: None,
        }
    }

    fn draw(&mut self) {
        let drawn = self.numbers.pop().unwrap();

        self.last_drawn = Some(drawn);

        self.boards = self
            .boards
            .iter()
            .map(|b| {
                b.iter()
                    .map(|r| {
                        r.iter()
                            .map(|n| match n {
                                Number::Unmarked(n) if *n == drawn => Number::Marked(*n),
                                n => *n, // TODO: maybe not needed
                            })
                            .collect()
                    })
                    .collect()
            })
            .collect();
    }

    fn is_boards_left(&self) -> bool {
        !self.boards.is_empty()
    }

    fn pop_winning(&mut self) -> Option<(Inner, Board)> {
        let (winners, losers) = self.boards.clone().into_iter().partition(|b| {
            b.iter()
                .any(|r| r.iter().all(|n| matches!(n, Number::Marked(_))))
                || (0..5).any(|c| b.iter().all(|r| matches!(r[c], Number::Marked(_))))
        });

        self.boards = losers;

        winners
            .first()
            .map(|b| (self.last_drawn.unwrap(), b.clone()))
    }

    fn add_numbers(&mut self, numbers: Vec<Inner>) {
        self.numbers = numbers.into_iter().rev().collect();
    }

    fn add_row(&mut self, row: &mut Vec<Number>) {
        if self.boards.is_empty() || self.boards.last().unwrap().len() == 5 {
            self.boards.push(vec![]);
        }

        self.boards.last_mut().unwrap().push(row.to_owned());
    }
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut bingo = Bingo::new();

        for line in io::BufReader::new(file).lines().map(|x| x.unwrap()) {
            if line.contains(",") {
                bingo.add_numbers(line.split(',').map(|x| x.parse().unwrap()).collect());
                continue;
            }

            if line.is_empty() {
                continue;
            }

            let mut row: Vec<Number> = line
                .split(' ')
                .map(|x| x.trim())
                .filter(|x| !x.is_empty())
                .map(|x| x.parse().expect(&format!("failed on {}", x)))
                .collect();

            bingo.add_row(&mut row);
        }

        bingo
    })
}

fn calculate_score(last_drawn: Inner, board: &Board) -> Inner {
    last_drawn
        * board
            .iter()
            .map(|r| {
                r.iter()
                    .map(|n| match n {
                        Number::Unmarked(n) => *n,
                        _ => 0,
                    })
                    .sum::<Inner>()
            })
            .sum::<Inner>()
}

fn part1(bingo: &mut Container) -> Inner {
    loop {
        bingo.draw();

        match bingo.pop_winning() {
            Some((last_drawn, winning_board)) => {
                return calculate_score(last_drawn, &winning_board)
            }
            _ => continue,
        }
    }
}

fn part2(bingo: &mut Container) -> Inner {
    loop {
        bingo.draw();

        match bingo.pop_winning() {
            Some((last_drawn, winning_board)) if !bingo.is_boards_left() => {
                return calculate_score(last_drawn, &winning_board)
            }
            _ => continue,
        }
    }
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&mut foo.clone()), 35670);
    assert_eq!(part2(&mut foo.clone()), 22704);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&mut foo.clone()), 4512);

    let mut column_win = foo.clone();
    column_win.add_numbers(vec![22, 8, 21, 6, 1]);
    assert_eq!(part1(&mut column_win), 242);

    assert_eq!(part2(&mut foo.clone()), 1924);
}
