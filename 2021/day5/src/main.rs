use itertools::iproduct;

use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

type Inner = usize;

#[derive(Clone, Copy)]
struct Point {
    x: Inner,
    y: Inner,
}

impl FromStr for Point {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let x = s
            .split(',')
            .map(|x| x.parse::<Inner>().unwrap())
            .collect::<Vec<Inner>>();

        Ok(Point { x: x[0], y: x[1] })
    }
}

type Container = Vec<(Point, Point)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let points = x
                    .unwrap()
                    .split(" -> ")
                    .map(|x| x.parse::<Point>().unwrap())
                    .collect::<Vec<_>>();
                (points[0], points[1])
            })
            .collect()
    })
}

fn mark_straight(points: &Vec<&(Point, Point)>) -> HashMap<(usize, usize), usize> {
    points.iter().fold(HashMap::new(), |mut diagram, (p1, p2)| {
        let (min, max) = if p1.x < p2.x || p1.y < p2.y {
            (p1, p2)
        } else {
            (p2, p1)
        };

        iproduct!(min.x..=max.x, min.y..=max.y).for_each(|p| {
            *diagram.entry(p).or_insert(0) += 1;
        });

        diagram
    })
}

fn part1(points: &Container) -> usize {
    let straight = points
        .iter()
        .filter(|&(p1, p2)| p1.x == p2.x || p1.y == p2.y)
        .collect();

    mark_straight(&straight)
        .iter()
        .filter(|(_, &r)| r > 1)
        .count()
}

fn part2(points: &Container) -> usize {
    let (straight, diag): (Vec<&(Point, Point)>, Vec<&(Point, Point)>) = points
        .iter()
        .partition(|&(p1, p2)| p1.x == p2.x || p1.y == p2.y);

    diag.iter()
        .fold(mark_straight(&straight), |mut diagram, (p1, p2)| {
            // It would be nicer to just return the iterator, but that doesn't seem to be possible.
            let diag_iter = |c1, c2| -> Vec<_> {
                if c1 > c2 {
                    (c2..=c1).rev().collect()
                } else {
                    (c1..=c2).collect()
                }
            };

            diag_iter(p1.x, p2.x)
                .into_iter()
                .zip(diag_iter(p1.y, p2.y).into_iter())
                .for_each(|p| {
                    *diagram.entry(p).or_insert(0) += 1;
                });

            diagram
        })
        .iter()
        .filter(|(_, &r)| r > 1)
        .count()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 7142);
    assert_eq!(part2(&foo), 20012);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 5);
    assert_eq!(part2(&foo), 12);
}
