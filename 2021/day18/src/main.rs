use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter::Peekable;
use std::result::Result;
use std::str::Chars;
use std::str::FromStr;

type Inner = u64;

type NodeIdx = usize;

#[derive(Clone)]
struct Node {
    left: Result<Inner, NodeIdx>,
    right: Result<Inner, NodeIdx>,
}

#[derive(Clone)]
struct Tree {
    nodes: Vec<Node>,
    root: Option<NodeIdx>,
}

impl FromStr for Tree {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fn parse(iter: &mut Peekable<Chars>, tree: &mut Tree) -> Result<Inner, Node> {
            match iter.peek() {
                Some('[') => {
                    iter.next();
                    let left = parse(iter, tree);
                    iter.next();
                    let right = parse(iter, tree);
                    iter.next();
                    Err(Node {
                        left: left.map_err(|n| tree.add_node(n)),
                        right: right.map_err(|n| tree.add_node(n)),
                    })
                }
                Some(_) => {
                    let mut s = String::new();
                    while let Some(c) = iter.peek() {
                        if *c != ']' && *c != ',' {
                            s.push(*c);
                            iter.next();
                        } else {
                            break;
                        }
                    }
                    let val: Inner = s.parse().unwrap();
                    Ok(val)
                }
                None => panic!("shouldn't be here"),
            }
        }

        let mut tree = Tree::new();
        let node = parse(&mut s.chars().peekable(), &mut tree);
        tree.add_root(node.err().unwrap());

        Ok(tree)
    }
}

impl Tree {
    fn new() -> Tree {
        Tree {
            nodes: vec![],
            root: None,
        }
    }

    fn add_node(&mut self, node: Node) -> NodeIdx {
        let i = self.nodes.len();
        self.nodes.push(node);
        i
    }

    fn add_root(&mut self, node: Node) -> NodeIdx {
        self.root = Some(self.add_node(node));

        self.root.unwrap()
    }

    fn add(&mut self, other: &Tree) {
        fn copy(tree: &mut Tree, other: &Tree, i: NodeIdx) -> NodeIdx {
            let old = &other.nodes[i];

            let new = Node {
                left: old.left.map_err(|o| copy(tree, other, o)),
                right: old.right.map_err(|o| copy(tree, other, o)),
            };

            tree.add_node(new)
        }

        let n = Node {
            left: Err(self.root.unwrap()),
            right: Err(copy(self, other, other.root.unwrap())),
        };

        self.add_root(n);
    }

    fn split(&mut self) -> bool {
        fn recur(tree: &mut Tree, i: NodeIdx) -> bool {
            if let Ok(v) = tree.nodes[i].left {
                if v >= 10 {
                    let new = Node {
                        left: Ok(v / 2),
                        right: Ok((v as f32 / 2.0).ceil() as Inner),
                    };

                    tree.nodes[i].left = Err(tree.add_node(new));
                    return true;
                }
            } else if let Err(n) = tree.nodes[i].left {
                if recur(tree, n) {
                    return true;
                }
            }

            if let Ok(v) = tree.nodes[i].right {
                if v >= 10 {
                    let new = Node {
                        left: Ok(v / 2),
                        right: Ok((v as f32 / 2.0).ceil() as Inner),
                    };

                    tree.nodes[i].right = Err(tree.add_node(new));
                    return true;
                }
            } else if let Err(n) = tree.nodes[i].right {
                return recur(tree, n);
            }

            return false;
        }

        recur(self, self.root.unwrap())
    }

    fn reduce(&mut self) {
        while self.explode() || self.split() {}
    }

    fn explode(&mut self) -> bool {
        fn add_right(tree: &mut Tree, i: NodeIdx, value: Inner) -> bool {
            if let Ok(v) = tree.nodes[i].left {
                tree.nodes[i].left = Ok(v + value);
                return true;
            } else if let Err(n) = tree.nodes[i].left {
                if add_right(tree, n, value) {
                    return true;
                }
            }
            if let Ok(v) = tree.nodes[i].right {
                tree.nodes[i].right = Ok(v + value);
                return true;
            } else if let Err(n) = tree.nodes[i].right {
                return add_right(tree, n, value);
            } else {
                panic!("shouldn't be here")
            }
        }

        fn add_left(tree: &mut Tree, i: NodeIdx, value: Inner) -> bool {
            if let Ok(v) = tree.nodes[i].right {
                tree.nodes[i].right = Ok(v + value);
                return true;
            } else if let Err(n) = tree.nodes[i].right {
                if add_left(tree, n, value) {
                    return true;
                }
            }
            if let Ok(v) = tree.nodes[i].left {
                tree.nodes[i].left = Ok(v + value);
                return true;
            } else if let Err(n) = tree.nodes[i].left {
                return add_left(tree, n, value);
            } else {
                panic!("shouldn't be here")
            }
        }

        fn recur(
            tree: &mut Tree,
            i: NodeIdx,
            depth: usize,
        ) -> Option<(Option<Inner>, Option<Inner>, bool)> {
            if depth >= 4 && tree.nodes[i].left.is_ok() && tree.nodes[i].right.is_ok() {
                return Some((
                    Some(tree.nodes[i].left.ok().unwrap()),
                    Some(tree.nodes[i].right.ok().unwrap()),
                    false,
                ));
            } else if let Err(n) = tree.nodes[i].left {
                let mut res = recur(tree, n, depth + 1);

                if let Some((Some(l), Some(r), false)) = res {
                    tree.nodes[i].left = Ok(0);
                    res = Some((Some(l), Some(r), true))
                }

                if let Some((l, Some(r), f)) = res {
                    if let Ok(v) = tree.nodes[i].right {
                        tree.nodes[i].right = Ok(v + r);
                        return Some((l, None, f));
                    } else {
                        let added = add_right(tree, tree.nodes[i].right.err().unwrap(), r);
                        if added {
                            return Some((l, None, f));
                        }
                    }
                }

                if res.is_some() {
                    return res;
                }
            }

            if let Err(n) = tree.nodes[i].right {
                let mut res = recur(tree, n, depth + 1);

                if let Some((Some(l), Some(r), false)) = res {
                    tree.nodes[i].right = Ok(0);
                    res = Some((Some(l), Some(r), true))
                }

                if let Some((Some(l), r, f)) = res {
                    if let Ok(v) = tree.nodes[i].left {
                        tree.nodes[i].left = Ok(v + l);
                        return Some((None, r, f));
                    } else {
                        let added = add_left(tree, tree.nodes[i].left.err().unwrap(), l);
                        if added {
                            return Some((None, r, f));
                        }
                    }
                }

                res
            } else {
                return None;
            }
        }

        recur(self, self.root.unwrap(), 0).is_some()
    }

    fn magnitude(&self) -> usize {
        fn recur(tree: &Tree, node: NodeIdx) -> usize {
            let node = &tree.nodes[node];
            3 * match node.left {
                Ok(v) => v as usize,
                Err(n) => recur(tree, n),
            } + 2 * match node.right {
                Ok(v) => v as usize,
                Err(n) => recur(tree, n),
            }
        }

        recur(self, self.root.unwrap())
    }

    #[cfg(test)]
    fn print(&self) -> String {
        fn recur(tree: &Tree, node: NodeIdx) -> String {
            let mut s = String::new();

            let node = &tree.nodes[node];

            s.push('[');
            match node.left {
                Ok(v) => s.push_str(&format!("{}", v)),
                Err(n) => {
                    s.push_str(&recur(tree, n));
                }
            }
            s.push_str(",");
            match node.right {
                Ok(v) => s.push_str(&format!("{}", v)),
                Err(n) => {
                    s.push_str(&recur(tree, n));
                }
            }
            s.push(']');

            s
        }

        recur(self, self.root.unwrap())
    }
}

type Container = Vec<Tree>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse().unwrap())
            .collect()
    })
}

fn part1(trees: &Container) -> usize {
    let tree = trees
        .iter()
        .cloned()
        .reduce(|mut res: Tree, new: Tree| {
            res.add(&new);
            res.reduce();
            res
        })
        .unwrap();

    tree.magnitude()
}

fn part2(trees: &Container) -> usize {
    let mut max = usize::MIN;

    for i in 0..trees.len() {
        for j in i..trees.len() {
            let mut tree = trees[i].clone();

            tree.add(&trees[j]);
            tree.reduce();

            max = cmp::max(max, tree.magnitude());

            let mut tree = trees[j].clone();

            tree.add(&trees[i]);
            tree.reduce();

            max = cmp::max(max, tree.magnitude());
        }
    }

    max
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 4132);
    assert_eq!(part2(&foo), 4685);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    let tree = foo.first().unwrap();

    assert_eq!(
        tree.print(),
        "[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]"
    );

    let mut tree: Tree = "[1,2]".parse().unwrap();

    tree.add(&"[[3,4],5]".parse().unwrap());
    assert_eq!(tree.print(), "[[1,2],[[3,4],5]]");

    let mut tree: Tree = "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]".parse().unwrap();
    tree.split();
    assert_eq!(tree.print(), "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]");

    let mut tree: Tree = "[[[[[9,8],1],2],3],4]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[[[[0,9],2],3],4]");

    let mut tree: Tree = "[7,[6,[5,[4,[3,2]]]]]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[7,[6,[5,[7,0]]]]");

    let mut tree: Tree = "[[6,[5,[4,[3,2]]]],1]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[[6,[5,[7,0]]],3]");

    let mut tree: Tree = "[[6,[5,[4,[3,2]]]],1]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[[6,[5,[7,0]]],3]");

    let mut tree: Tree = "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");

    let mut tree: Tree = "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]".parse().unwrap();
    tree.explode();
    assert_eq!(tree.print(), "[[3,[2,[8,0]]],[9,[5,[7,0]]]]");

    let tree: Tree = "[[1,2],[[3,4],5]]".parse().unwrap();
    assert_eq!(tree.magnitude(), 143);

    let tree: Tree = "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]".parse().unwrap();
    assert_eq!(tree.magnitude(), 1384);

    let tree: Tree = "[[[[1,1],[2,2]],[3,3]],[4,4]]".parse().unwrap();
    assert_eq!(tree.magnitude(), 445);

    let tree: Tree = "[[[[3,0],[5,3]],[4,4]],[5,5]]".parse().unwrap();
    assert_eq!(tree.magnitude(), 791);

    let tree: Tree = "[[[[5,0],[7,4]],[5,5]],[6,6]]".parse().unwrap();
    assert_eq!(tree.magnitude(), 1137);

    let tree: Tree = "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
        .parse()
        .unwrap();
    assert_eq!(tree.magnitude(), 3488);

    assert_eq!(part1(&foo), 4140);
    assert_eq!(part2(&foo), 3993);
}
