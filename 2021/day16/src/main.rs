use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Clone)]
struct Packet {
    data: Vec<char>,
    versions: Vec<usize>,
    i: usize,
}

impl FromStr for Packet {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Packet {
            data: hex_to_bit_vec(s),
            versions: vec![],
            i: 0,
        })
    }
}

impl Packet {
    fn next(&mut self) -> char {
        let prev = self.i;
        self.i += 1;

        self.data[prev]
    }

    fn get_num(&mut self, count: usize) -> usize {
        let prev = self.i;
        self.i += count;

        usize::from_str_radix(&self.data[prev..self.i].iter().collect::<String>(), 2).unwrap()
    }

    fn process(&mut self) -> usize {
        let version = self.get_num(3);

        self.versions.push(version);

        let id = self.get_num(3);

        match id {
            4 => self.literal(),
            x => self.operator(x),
        }
    }

    fn literal(&mut self) -> usize {
        let mut result = 0;

        while self.next() != '0' {
            let num = self.get_num(4);

            result = (result << 4) + num;
        }

        (result << 4) + self.get_num(4)
    }

    fn operator(&mut self, id: usize) -> usize {
        let is_total_length = self.next() == '0';

        let nums: Vec<_> = if is_total_length {
            let total_length = self.get_num(15);

            let start = self.i;

            std::iter::from_fn(move || {
                if self.i - start < total_length {
                    Some(self.process())
                } else {
                    None
                }
            })
            .collect()
        } else {
            let n_packets = self.get_num(11);

            (0..n_packets).map(|_| self.process()).collect()
        };

        match id {
            0 => nums.iter().sum(),
            1 => nums.iter().product(),
            2 => *nums.iter().min().unwrap(),
            3 => *nums.iter().max().unwrap(),
            5 => {
                if nums[0] > nums[1] {
                    1
                } else {
                    0
                }
            }
            6 => {
                if nums[0] < nums[1] {
                    1
                } else {
                    0
                }
            }
            7 => {
                if nums[0] == nums[1] {
                    1
                } else {
                    0
                }
            }
            id => panic!("unsupported id: {}", id),
        }
    }
}

fn read(path: &str) -> io::Result<Packet> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap())
            .collect::<String>()
            .parse()
            .unwrap()
    })
}

fn hex_to_bit_vec(line: &str) -> Vec<char> {
    line.chars()
        .map(|x| x.to_digit(16).unwrap())
        .map(|i| format!("{:04b}", i))
        .map(|x| x.chars().collect::<Vec<char>>())
        .flatten()
        .collect()
}

fn part1(packet: &Packet) -> usize {
    let mut packet = packet.clone();

    packet.process();

    packet.versions.iter().sum()
}

fn part2(packet: &Packet) -> usize {
    let mut packet = packet.clone();
    packet.process()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 945);
    assert_eq!(part2(&foo), 10637009915279);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(
        foo.data.iter().collect::<String>(),
        "110100101111111000101000"
    );

    let foo2 = read("src/test2").unwrap();

    assert_eq!(
        foo2.data.iter().collect::<String>(),
        "00111000000000000110111101000101001010010001001000000000"
    );

    let mut packet: Packet = "D2FE28".parse().unwrap();
    packet.i += 6;
    assert_eq!(packet.literal(), 2021);
    assert_eq!(packet.i, 21);

    let mut packet: Packet = "38006F45291200".parse().unwrap();
    packet.i += 6;
    assert_eq!(packet.operator(0), 30);
    assert_eq!(packet.i, 49);

    let mut packet: Packet = "EE00D40C823060".parse().unwrap();
    packet.i += 6;
    assert_eq!(packet.operator(0), 6);
    assert_eq!(packet.i, 51);

    assert_eq!(part1(&foo), 6);
    assert_eq!(part2(&foo), 2021);
}
