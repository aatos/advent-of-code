use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

struct Display {
    input: Vec<HashSet<char>>,
    output: Vec<HashSet<char>>,
}

impl FromStr for Display {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let line = s.split(" | ").collect::<Vec<_>>();

        let collect = |l: &str| {
            l.split(' ')
                .map(|x| x.chars().collect::<HashSet<_>>())
                .collect::<Vec<_>>()
        };

        Ok(Display {
            input: collect(line[0]),
            output: collect(line[1]),
        })
    }
}

type Inner = Display;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn part1(displays: &Container) -> usize {
    displays.iter().fold(0, |sum, d| {
        sum + d
            .output
            .iter()
            .filter(|o| match o.len() {
                2 | 4 | 3 | 7 => true,
                _ => false,
            })
            .count()
    })
}

fn find_and_remove<F: Fn(&HashSet<char>) -> bool>(
    v: &mut Vec<HashSet<char>>,
    f: F,
) -> HashSet<char> {
    let i = v.iter().position(f).unwrap();
    v.swap_remove(i)
}

fn part2(displays: &Container) -> usize {
    displays.iter().fold(0, |sum, d| {
        let mut input = d.input.clone();
        let mut m: HashMap<usize, HashSet<char>> = HashMap::new();

        m.insert(1, find_and_remove(&mut input, |x| x.len() == 2));
        m.insert(4, find_and_remove(&mut input, |x| x.len() == 4));
        m.insert(7, find_and_remove(&mut input, |x| x.len() == 3));
        m.insert(8, find_and_remove(&mut input, |x| x.len() == 7));

        m.insert(
            9,
            find_and_remove(&mut input, |x| {
                x.len() == 6 && m.get(&4).unwrap().is_subset(x)
            }),
        );

        m.insert(
            0,
            find_and_remove(&mut input, |x| {
                x.len() == 6 && m.get(&1).unwrap().is_subset(x)
            }),
        );

        m.insert(6, find_and_remove(&mut input, |x| x.len() == 6));

        m.insert(
            5,
            find_and_remove(&mut input, |x| {
                x.len() == 5 && x.is_subset(m.get(&6).unwrap())
            }),
        );

        m.insert(
            3,
            find_and_remove(&mut input, |x| {
                x.len() == 5 && x.is_subset(m.get(&9).unwrap())
            }),
        );

        m.insert(2, input.pop().unwrap());

        sum + d.output.iter().fold(0, |acc, x| {
            acc * 10 + m.iter().find(|&(_, v)| *v == *x).unwrap().0
        })
    })
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 367);
    assert_eq!(part2(&foo), 974512);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 26);

    let line =
        "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
    let d: Display = line.parse().unwrap();
    assert_eq!(part2(&vec![d]), 5353);

    assert_eq!(part2(&foo), 61229);
}
