use std::fs::File;
use std::io::{self, BufRead};
use std::str::FromStr;

#[derive(Copy, Clone, Debug)]
enum Variable {
    W = 0,
    X = 1,
    Y = 2,
    Z = 3,
}

type Int = i64;

impl FromStr for Variable {
    type Err = Int;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "w" => Ok(Variable::W),
            "x" => Ok(Variable::X),
            "y" => Ok(Variable::Y),
            "z" => Ok(Variable::Z),
            _ => Err(s.parse().expect(&format!("{}", s))),
        }
    }
}

type Rhs = Result<Variable, Int>;

#[derive(Debug)]
enum Instruction {
    Input(Variable),
    Add(Variable, Rhs),
    Multiply(Variable, Rhs),
    Divide(Variable, Rhs),
    Modulo(Variable, Rhs),
    Equal(Variable, Rhs),
}

impl FromStr for Instruction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let words = s.split(' ').collect::<Vec<_>>();

        Ok(match words[0] {
            "inp" => Instruction::Input(words[1].parse().unwrap()),
            "add" => Instruction::Add(words[1].parse().unwrap(), words[2].parse()),
            "mul" => Instruction::Multiply(words[1].parse().unwrap(), words[2].parse()),
            "div" => Instruction::Divide(words[1].parse().unwrap(), words[2].parse()),
            "mod" => Instruction::Modulo(words[1].parse().unwrap(), words[2].parse()),
            "eql" => Instruction::Equal(words[1].parse().unwrap(), words[2].parse()),
            _ => panic!("invalid input: {:?}", words),
        })
    }
}

type Inner = Instruction;
type Container = Vec<Instruction>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn execute(c: &Container, input: u64) -> [Int; 4] {
    let mut state: [Int; 4] = [0; 4];

    let mut n = 0;
    let mut input = std::iter::from_fn(move || {
        if n > 13 {
            None
        } else {
            let ten: u64 = 10;

            let next = (input / ten.pow(13 - n)) % 10;

            n += 1;

            Some(next as Int)
        }
    });

    for i in c {
        match i {
            Instruction::Input(v) => {
                state[*v as usize] = input.next().unwrap();
            }
            Instruction::Add(lhs, rhs) => {
                state[*lhs as usize] += match rhs {
                    Ok(v) => state[*v as usize],
                    Err(i) => *i,
                }
            }
            Instruction::Multiply(lhs, rhs) => {
                state[*lhs as usize] *= match rhs {
                    Ok(v) => state[*v as usize],
                    Err(i) => *i,
                }
            }
            Instruction::Divide(lhs, rhs) => {
                state[*lhs as usize] /= match rhs {
                    Ok(v) => state[*v as usize],
                    Err(i) => *i,
                }
            }
            Instruction::Modulo(lhs, rhs) => {
                state[*lhs as usize] %= match rhs {
                    Ok(v) => state[*v as usize],
                    Err(i) => *i,
                }
            }
            Instruction::Equal(lhs, rhs) => {
                state[*lhs as usize] = if state[*lhs as usize]
                    == match rhs {
                        Ok(v) => state[*v as usize],
                        Err(i) => *i,
                    } {
                    1
                } else {
                    0
                }
            }
        }
    }

    state
}

fn part1(c: &Container) -> u64 {
    for i in 0..9999999999999 {
        let model = 99999999999999 - i;

        let state = execute(c, model);
        if state[Variable::Z as usize] == 0 {
            if model.to_string().contains('0') {
                continue;
            }

            return model;
        }
    }

    panic!("part 1 unfinished")
}

fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let foo = read("src/input").unwrap();

    // TODO: doesn't finish in sane time
    // assert_eq!(part1(&foo), 0);
    // assert_eq!(part2(&foo), 0);
}

#[test]
fn test_input() {
    let instructions = vec![
        Instruction::Input(Variable::W),
        Instruction::Add(Variable::W, Err(9)),
        Instruction::Input(Variable::X),
        Instruction::Multiply(Variable::X, Ok(Variable::W)),
        Instruction::Input(Variable::Y),
        Instruction::Divide(Variable::Y, Err(2)),
        Instruction::Input(Variable::Z),
        Instruction::Modulo(Variable::Z, Err(7)),
    ];

    assert_eq!(execute(&instructions, 12395678912345), [10, 20, 1, 2]);
}

#[test]
fn test_input2() {
    let instructions = vec![
        Instruction::Input(Variable::W),
        Instruction::Equal(Variable::W, Err(9)),
    ];

    assert_eq!(execute(&instructions, 92345678912345), [1, 0, 0, 0]);
}

#[test]
fn test_input3() {
    let instructions = vec![
        Instruction::Input(Variable::Z),
        Instruction::Equal(Variable::Z, Err(1)),
    ];

    assert_eq!(part1(&instructions), 99999999999999);
}
