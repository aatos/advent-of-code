use std::fs::File;
use std::io::{self, BufRead};

const MAX_AGE: usize = 9;

type Inner = usize;
type Container = [Inner; MAX_AGE];

fn read(path: &str) -> io::Result<Container> {
    File::open(path)
        .map(|file| {
            io::BufReader::new(file)
                .lines()
                .map(|x| {
                    x.unwrap()
                        .split(',')
                        .map(|x| x.parse::<Inner>().unwrap())
                        .collect()
                })
                .collect()
        })
        .map(|v: Vec<Vec<Inner>>| {
            v.iter().fold([0; MAX_AGE], |mut acc, x| {
                x.iter().for_each(|i| acc[*i] += 1);

                acc
            })
        })
}

fn simulate(fish: &Container, days: usize) -> usize {
    (1..=days)
        .fold(fish.clone(), |fish, _| {
            fish.into_iter()
                .enumerate()
                .fold([0; MAX_AGE], |mut new_fish, (age, count)| {
                    if count > 0 {
                        match age {
                            0 => {
                                new_fish[6] += count;
                                new_fish[8] += count;
                            }
                            x => new_fish[x - 1] += count,
                        }
                    }
                    new_fish
                })
        })
        .iter()
        .sum()
}

fn part1(fish: &Container) -> usize {
    simulate(fish, 80)
}

fn part2(fish: &Container) -> usize {
    simulate(fish, 256)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 372984);
    assert_eq!(part2(&foo), 1681503251694);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    let arr = [0, 1, 1, 2, 1, 0, 0, 0, 0];
    assert_eq!(foo, arr);

    assert_eq!(part1(&foo), 5934);
    assert_eq!(part2(&foo), 26984457539);
}
