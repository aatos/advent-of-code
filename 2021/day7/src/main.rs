use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .flat_map(|x| {
                x.unwrap()
                    .split(',')
                    .map(|x| x.parse::<Inner>().unwrap())
                    .collect::<Container>()
            })
            .collect()
    })
}

fn part1(crabs: &Container) -> Inner {
    let mut crabs = crabs.clone();
    crabs.sort();

    let median = crabs[crabs.len() / 2];

    crabs
        .iter()
        .map(|&x| cmp::max(x, median) - cmp::min(x, median))
        .sum()
}

fn part2(crabs: &Container) -> Inner {
    let avg = crabs.iter().sum::<Inner>() as f64 / crabs.len() as f64;

    let fuel_to = |pos| {
        crabs
            .iter()
            .map(|&x| {
                let diff = cmp::max(x, pos) - cmp::min(x, pos);
                diff * (diff + 1) / 2
            })
            .sum()
    };

    cmp::min(fuel_to(avg.floor() as u64), fuel_to(avg.ceil() as u64))
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 356179);
    assert_eq!(part2(&foo), 99788435);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 37);
    assert_eq!(part2(&foo), 168);
}
