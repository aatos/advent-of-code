use itertools::iproduct;
use std::cmp::max;
use std::cmp::min;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};
use std::ops;
use std::str::FromStr;

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
struct Range {
    start: Inner,
    end: Inner,
}

impl Range {
    fn is_within(&self, x: ops::RangeInclusive<Inner>) -> bool {
        x.contains(&self.start) && x.contains(&self.end)
    }

    fn negate(&self, other: &Range) -> Option<Range> {
        let new = Range {
            start: max(self.start, other.start),
            end: min(self.end, other.end),
        };

        if new.start <= new.end {
            Some(new)
        } else {
            None
        }
    }

    fn area(&self) -> Inner {
        self.end - self.start + 1
    }
}

impl FromStr for Range {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let x = s
            .chars()
            .skip(2)
            .collect::<String>()
            .split("..")
            .map(|x| x.parse::<Inner>().expect(&format!("{}", x)))
            .collect::<Vec<_>>();

        Ok(Range {
            start: x[0],
            end: x[1],
        })
    }
}

type Inner = i64;
type Container = Vec<(bool, Range, Range, Range)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let line = x.unwrap();
                let x = line.split(' ').collect::<Vec<_>>();
                let is_on = match x[0] {
                    "on" => true,
                    "off" => false,
                    f => panic!("{}", f),
                };

                let coords = x[1].split(',').collect::<Vec<_>>();

                let x = coords[0].parse::<Range>().unwrap();
                let y = coords[1].parse::<Range>().unwrap();
                let z = coords[2].parse::<Range>().unwrap();

                (is_on, x, y, z)
            })
            .collect()
    })
}

fn part1(start: &Container) -> usize {
    let mut state: HashSet<(Inner, Inner, Inner)> = HashSet::new();

    start.iter().for_each(|(is_on, x, y, z)| {
        if x.is_within(-50..=50) && y.is_within(-50..=50) && z.is_within(-50..=50) {
            iproduct!(x.start..=x.end, y.start..=y.end, z.start..=z.end).for_each(|(x, y, z)| {
                if *is_on {
                    state.insert((x, y, z))
                } else {
                    state.remove(&(x, y, z))
                };
            });
        }
    });

    state.len()
}

fn part2(start: &Container) -> Inner {
    let mut state: HashMap<(Range, Range, Range), i64> = HashMap::new();

    start.iter().for_each(|&(is_on, x, y, z)| {
        for ((x2, y2, z2), sign) in state.clone() {
            let xs = x.negate(&x2);
            let ys = y.negate(&y2);
            let zs = z.negate(&z2);

            if xs.is_some() && ys.is_some() && zs.is_some() {
                *state
                    .entry((xs.unwrap(), ys.unwrap(), zs.unwrap()))
                    .or_insert(0) -= sign;
            }
        }

        if is_on {
            *state.entry((x, y, z)).or_insert(0) += 1;
        }
    });

    state.iter().fold(0, |sum, ((x, y, z), sign)| {
        sum + x.area() * y.area() * z.area() * sign
    })
}
fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 567496);
    assert_eq!(part2(&foo), 1355961721298916);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 590784);
}

#[test]
fn test_input2() {
    let foo = read("src/test2").unwrap();

    assert_eq!(part1(&foo), 39);
    assert_eq!(part2(&foo), 39);
}

#[test]
fn test_input3() {
    let foo = read("src/test3").unwrap();
    assert_eq!(part2(&foo), 2758514936282235);
}
