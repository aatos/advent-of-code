use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = i64;
type Pos = (Inner, Inner, Inner);

struct Scanner {
    coords: Vec<Pos>,
    rotations: Vec<HashSet<Pos>>,
}

impl Scanner {
    fn new(coords: &Vec<Pos>) -> Scanner {
        Scanner {
            coords: coords.clone(),
            rotations: vec![
                HashSet::from_iter(coords.iter().cloned()),
                coords.iter().map(|&(x, y, z)| (x, -z, y)).collect(),
                coords.iter().map(|&(x, y, z)| (x, -y, -z)).collect(),
                coords.iter().map(|&(x, y, z)| (x, z, -y)).collect(),
                coords.iter().map(|&(x, y, z)| (-x, -y, z)).collect(),
                coords.iter().map(|&(x, y, z)| (-x, -z, -y)).collect(),
                coords.iter().map(|&(x, y, z)| (-x, y, -z)).collect(),
                coords.iter().map(|&(x, y, z)| (-x, z, y)).collect(),
                coords.iter().map(|&(x, y, z)| (-z, x, -y)).collect(),
                coords.iter().map(|&(x, y, z)| (y, x, -z)).collect(),
                coords.iter().map(|&(x, y, z)| (z, x, y)).collect(),
                coords.iter().map(|&(x, y, z)| (-y, x, z)).collect(),
                coords.iter().map(|&(x, y, z)| (z, -x, -y)).collect(),
                coords.iter().map(|&(x, y, z)| (y, -x, z)).collect(),
                coords.iter().map(|&(x, y, z)| (-z, -x, y)).collect(),
                coords.iter().map(|&(x, y, z)| (-y, -x, -z)).collect(),
                coords.iter().map(|&(x, y, z)| (-y, -z, x)).collect(),
                coords.iter().map(|&(x, y, z)| (z, -y, x)).collect(),
                coords.iter().map(|&(x, y, z)| (y, z, x)).collect(),
                coords.iter().map(|&(x, y, z)| (-z, y, x)).collect(),
                coords.iter().map(|&(x, y, z)| (z, y, -x)).collect(),
                coords.iter().map(|&(x, y, z)| (-y, z, -x)).collect(),
                coords.iter().map(|&(x, y, z)| (-z, -y, -x)).collect(),
                coords.iter().map(|&(x, y, z)| (y, -z, -x)).collect(),
            ],
        }
    }
}

type Container = Vec<Scanner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let (mut scanners, last_coords) = io::BufReader::new(file).lines().fold(
            (vec![], vec![]),
            |(mut scanners, mut coords), x| {
                let x = x.unwrap();

                if x.is_empty() {
                    scanners.push(Scanner::new(&coords));
                    (scanners, vec![])
                } else if !x.starts_with("--- ") {
                    let pos = x
                        .split(',')
                        .map(|x| x.parse::<Inner>().unwrap())
                        .collect::<Vec<_>>();
                    coords.push((pos[0], pos[1], pos[2]));
                    (scanners, coords)
                } else {
                    (scanners, coords)
                }
            },
        );

        scanners.push(Scanner::new(&last_coords));
        scanners
    })
}

fn add_offset(beacons: &HashSet<Pos>, offset: Pos) -> HashSet<Pos> {
    beacons
        .iter()
        .map(|&(x, y, z)| (x + offset.0, y + offset.1, z + offset.2))
        .collect()
}

fn max_manhattan_distance(scanners: &Vec<Pos>) -> usize {
    (0..(scanners.len() - 1))
        .map(|i| {
            (i..scanners.len())
                .map(move |j| {
                    let (x, y, z) = scanners[i];
                    let (xx, yy, zz) = scanners[j];

                    (x - xx + y - yy + z - zz).abs() as usize
                })
                .max()
                .unwrap()
        })
        .max()
        .unwrap()
}

fn part1and2(scanners: &mut Container) -> (usize, usize) {
    let first = scanners.swap_remove(0);

    let mut fixed_beacons = HashSet::from_iter(first.coords.iter().cloned());
    let mut positions: Vec<Pos> = Vec::new();

    'start: while !scanners.is_empty() {
        for i in 0..scanners.len() {
            for rotated_beacons in &scanners[i].rotations {
                for (f_x, f_y, f_z) in &fixed_beacons {
                    for (b_x, b_y, b_z) in rotated_beacons {
                        let offset = (f_x - b_x, f_y - b_y, f_z - b_z);
                        let shifted = add_offset(&rotated_beacons, offset);

                        if fixed_beacons.intersection(&shifted).count() >= 12 {
                            fixed_beacons.extend(&shifted);
                            positions.push(offset);
                            scanners.swap_remove(i);
                            continue 'start;
                        }
                    }
                }
            }
        }
    }

    (fixed_beacons.len(), max_manhattan_distance(&positions))
}

fn main() {
    let mut foo = read("src/input").unwrap();

    assert_eq!(part1and2(&mut foo), (472, 12092));
}

#[test]
fn test_input() {
    let mut foo = read("src/test").unwrap();

    assert_eq!(part1and2(&mut foo), (79, 3621));
}

#[test]
fn test_input2() {
    let scanners = read("src/test2").unwrap();

    let expected = scanners
        .first()
        .unwrap()
        .coords
        .iter()
        .cloned()
        .collect::<HashSet<_>>();

    for s in scanners.iter().skip(1) {
        assert!(s.rotations.iter().any(|rotated| { *rotated == expected }));
    }
}
