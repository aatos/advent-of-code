use std::cmp;
use std::fs::File;
use std::io::{self, BufRead};

use itertools::iproduct;

struct Image {
    enhancement: Vec<char>,
    original: Vec<Vec<char>>,
}

type Container = Image;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file).lines().fold(
            Image {
                enhancement: vec![],
                original: vec![],
            },
            |mut image, x| {
                let x = x.unwrap();

                if image.enhancement.is_empty() {
                    image.enhancement = x.chars().collect::<Vec<char>>();
                } else if !x.is_empty() {
                    image.original.push(x.chars().collect::<Vec<char>>());
                }
                image
            },
        )
    })
}

fn neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    iproduct!(min_y..=max_y, min_x..=max_x)
}

fn enhance(image: &Container, times: usize) -> usize {
    let len = 500;
    let offset = len / 2;

    let enlarged = (0..len)
        .map(|y| {
            (0..len)
                .map(|x| {
                    if y >= offset
                        && y - offset < image.original.len()
                        && x >= offset
                        && x - offset < image.original[0].len()
                    {
                        image.original[y - offset][x - offset]
                    } else {
                        '.'
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<Vec<_>>>();

    let output = (0..times).fold(enlarged, |output, _| {
        (0..len)
            .map(|y| {
                (0..len)
                    .map(|x| {
                        let i = neighbors(len, len, y, x).fold(0, |bin, (y, x)| {
                            (bin << 1) | if output[y][x] == '.' { 0 } else { 1 }
                        });

                        image.enhancement[i]
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<Vec<_>>>()
    });

    iproduct!(0..output.len(), 0..output[0].len())
        .filter(|&(y, x)| output[y][x] == '#')
        .count()
}

fn part1(image: &Container) -> usize {
    enhance(image, 2)
}

fn part2(image: &Container) -> usize {
    enhance(image, 50)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 5259);
    assert_eq!(part2(&foo), 15287);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 35);
    assert_eq!(part2(&foo), 3351);
}
