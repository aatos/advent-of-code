use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().parse::<Inner>().unwrap())
            .collect()
    })
}

fn part1(depths: &Container) -> usize {
    depths
        .windows(2)
        .filter(|s| s.first().unwrap() < s.last().unwrap())
        .count()
}

fn part2(depths: &Container) -> usize {
    let windows: Vec<Inner> = depths.windows(3).map(|s| s.iter().sum()).collect();
    part1(&windows)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 1791);
    assert_eq!(part2(&foo), 1822);
}
