use std::cmp;
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter;

use itertools::iproduct;

// https://doc.rust-lang.org/std/collections/binary_heap/index.html#examples
#[derive(Copy, Clone, Eq, PartialEq)]
struct State {
    cost: usize,
    position: (usize, usize),
}

impl Ord for State {
    fn cmp(&self, other: &Self) -> Ordering {
        other
            .cost
            .cmp(&self.cost)
            .then_with(|| self.position.cmp(&other.position))
    }
}

impl PartialOrd for State {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

type Inner = u64;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .chars()
                    .map(|x| x.to_digit(10).unwrap() as Inner)
                    .collect()
            })
            .collect()
    })
}

fn non_diag_neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    (min_y..=max_y)
        .zip(iter::repeat(x))
        .chain(iter::repeat(y).zip(min_x..=max_x))
        .filter(move |&(j, i)| (i != x || j != y))
}

fn shortest_path(c: &Container, start: (usize, usize), end: (usize, usize)) -> usize {
    let y_size = c.len();
    let x_size = c[0].len();

    let mut dist: HashMap<(usize, usize), usize> = iproduct!(0..y_size, 0..x_size)
        .map(|(y, x)| ((y, x), usize::MAX))
        .collect();

    dist.insert(start, 0);

    let mut heap = BinaryHeap::new();

    heap.push(State {
        cost: 0,
        position: start,
    });

    while let Some(State { cost, position }) = heap.pop() {
        if position == end {
            return cost;
        }

        if cost > *dist.get(&position).unwrap() {
            continue;
        }

        for (yy, xx) in non_diag_neighbors(y_size, x_size, position.0, position.1) {
            let next = State {
                cost: cost + c[yy][xx] as usize,
                position: (yy, xx),
            };

            if next.cost < *dist.get(&next.position).unwrap() {
                heap.push(next);
                dist.insert(next.position, next.cost);
            }
        }
    }

    0
}

fn part1(c: &Container) -> usize {
    shortest_path(c, (0, 0), (c.len() - 1, c[0].len() - 1))
}

fn enlarge(c: &Container) -> Container {
    (0..5)
        .flat_map(|ii| {
            c.iter().map(move |r| {
                (0..5)
                    .flat_map(|i| {
                        r.iter()
                            .map(|x| (x + i + ii - 1) % 9 + 1)
                            .collect::<Vec<_>>()
                    })
                    .collect()
            })
        })
        .collect()
}

fn part2(c: &Container) -> usize {
    part1(&enlarge(c))
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 619);
    assert_eq!(part2(&foo), 2922);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 40);

    assert_eq!(
        enlarge(&vec![vec![8]]),
        vec![
            vec![8, 9, 1, 2, 3],
            vec![9, 1, 2, 3, 4],
            vec![1, 2, 3, 4, 5],
            vec![2, 3, 4, 5, 6],
            vec![3, 4, 5, 6, 7],
        ]
    );

    assert_eq!(part2(&foo), 315);
}
