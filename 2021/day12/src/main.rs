use std::collections::HashSet;
use std::fs::File;
use std::io::{self, BufRead};

const START_NODE: &'static str = "start";
const END_NODE: &'static str = "end";

// roughly following http://smallcultfollowing.com/babysteps/blog/2015/04/06/modeling-graphs-in-rust-using-vector-indices/
struct Graph {
    nodes: Vec<NodeData>,
    edges: Vec<EdgeData>,
}

type NodeIndex = usize;

struct NodeData {
    name: String,
    edge: Option<EdgeIndex>,
    is_big: bool,
}

type EdgeIndex = usize;

struct EdgeData {
    target: NodeIndex,
    next: Option<EdgeIndex>,
}

impl Graph {
    fn new() -> Graph {
        Graph {
            nodes: vec![],
            edges: vec![],
        }
    }

    fn get_node(&self, node_name: &str) -> Option<NodeIndex> {
        self.nodes.iter().position(|x| x.name == node_name)
    }

    fn add_node(&mut self, node_name: &str) -> NodeIndex {
        match self.get_node(node_name) {
            Some(i) => i,
            None => {
                let index = self.nodes.len();
                self.nodes.push(NodeData {
                    name: node_name.to_string(),
                    edge: None,
                    is_big: node_name == node_name.to_uppercase(),
                });

                index
            }
        }
    }

    fn add_edges(&mut self, node1: &str, node2: &str) {
        let node1 = self.add_node(node1);
        let node2 = self.add_node(node2);

        self.add_edge(node1, node2);
        self.add_edge(node2, node1);
    }

    fn add_edge(&mut self, source: NodeIndex, target: NodeIndex) {
        let index = self.edges.len();

        self.edges.push(EdgeData {
            target: target,
            next: self.nodes[source].edge,
        });

        self.nodes[source].edge = Some(index);
    }
}

type Container = Graph;

fn read(path: &str) -> io::Result<Graph> {
    File::open(path).map(|file| {
        let mut graph = Graph::new();

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();
            let nodes = x.split('-').collect::<Vec<_>>();

            graph.add_edges(nodes[0], nodes[1]);
        });

        graph
    })
}

fn part1(graph: &Container) -> usize {
    let mut ends = 0;

    let mut q: Vec<(NodeIndex, HashSet<NodeIndex>)> = vec![];
    q.push((graph.get_node(START_NODE).unwrap(), HashSet::new()));

    while !q.is_empty() {
        let (w, mut seen) = q.pop().unwrap();

        let node = &graph.nodes[w];

        if node.name == END_NODE {
            ends += 1;
            continue;
        }

        if seen.contains(&w) {
            continue;
        }

        if !node.is_big {
            seen.insert(w);
        }

        let mut next = node.edge;

        while let Some(e) = next.map(|e| &graph.edges[e]) {
            q.push((e.target, seen.clone()));
            next = e.next;
        }
    }

    ends
}

fn part2(graph: &Container) -> usize {
    let mut paths: HashSet<Vec<NodeIndex>> = HashSet::new();

    let start_node = graph.get_node(START_NODE).unwrap();
    let end_node = graph.get_node(END_NODE).unwrap();

    let mut q: Vec<(NodeIndex, HashSet<NodeIndex>, bool, Vec<NodeIndex>)> = vec![];
    q.push((start_node, HashSet::new(), false, vec![]));

    while !q.is_empty() {
        let (w, mut seen, mut used_two, mut path) = q.pop().unwrap();

        let node = &graph.nodes[w];

        if w == end_node {
            paths.insert(path);
            continue;
        }

        if seen.contains(&w) {
            if used_two {
                continue;
            }
            used_two = true;
        }

        if !node.is_big {
            seen.insert(w);
        }

        let mut next = node.edge;

        path.push(w);

        while let Some(e) = next.map(|e| &graph.edges[e]) {
            // TODO: shouldn't have to go through the exact same paths multiple times.

            if e.target != start_node {
                q.push((e.target, seen.clone(), used_two, path.clone()));
                if !used_two {
                    q.push((e.target, seen.clone(), true, path.clone()));
                }
            }

            next = e.next;
        }
    }

    paths.len()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 3230);
    assert_eq!(part2(&foo), 83475);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 10);
    assert_eq!(part2(&foo), 36);
}
