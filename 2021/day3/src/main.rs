use std::fs::File;
use std::io::{self, BufRead};
use std::ops::ControlFlow;

type Inner = u64;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| Inner::from_str_radix(&x.unwrap(), 2).unwrap())
            .collect()
    })
}

fn part1<const LEN: usize>(bits: &Container) -> usize {
    let gamma = (0..LEN).fold(0, |acc, i| if is_on(bits, i) { acc | 1 << i } else { acc });

    gamma * (!gamma & ((1 << LEN) - 1))
}

fn is_on(bits: &Container, i: usize) -> bool {
    bits.iter()
        .fold(0, |acc, b| acc + if b & (1 << i) != 0 { 1 } else { -1 })
        >= 0
}

fn find_rating<const LEN: usize>(bits: &Container, most_common: bool) -> Inner {
    match (0..LEN).rev().try_fold(bits.clone(), |mut acc, i| {
        let common = is_on(&acc, i);

        acc.retain(|b| ((*b & (1 << i) != 0) == common) == most_common);

        if acc.len() == 1 {
            ControlFlow::Break(acc[0])
        } else {
            ControlFlow::Continue(acc)
        }
    }) {
        ControlFlow::Break(x) => x,
        _ => panic!("couldn't find rating"),
    }
}

fn part2<const LEN: usize>(bits: &Container) -> Inner {
    find_rating::<LEN>(bits, true) * find_rating::<LEN>(bits, false)
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1::<12>(&foo), 2250414);
    assert_eq!(part2::<12>(&foo), 6085575);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1::<5>(&foo), 198);
    assert_eq!(part2::<5>(&foo), 230);
}
