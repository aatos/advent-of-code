use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
use std::ops::ControlFlow;

type Inner = String;
type Container = Vec<Inner>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap())
            .collect()
    })
}

fn handle_corrupted(line: &str) -> Result<usize, Vec<char>> {
    let map = HashMap::from([
        ('}', ('{', 1197)),
        (')', ('(', 3)),
        (']', ('[', 57)),
        ('>', ('<', 25137)),
    ]);

    match line.chars().try_fold(vec![], |mut stack, c| match c {
        '{' | '(' | '[' | '<' => {
            stack.push(c);
            ControlFlow::Continue(stack)
        }
        _ => {
            let last = stack.pop().unwrap();
            let (expected, p) = map.get(&c).unwrap();
            if last != *expected {
                ControlFlow::Break(*p)
            } else {
                ControlFlow::Continue(stack)
            }
        }
    }) {
        ControlFlow::Break(p) => Ok(p),
        ControlFlow::Continue(s) => Err(s),
    }
}

fn part1(chunks: &Container) -> usize {
    chunks
        .iter()
        .filter_map(|line| handle_corrupted(line).ok())
        .sum()
}

fn part2(chunks: &Container) -> usize {
    let mut points = chunks
        .iter()
        .filter_map(|line| handle_corrupted(line).err())
        .map(|stack| {
            stack.iter().rev().fold(0, |p, c| {
                p * 5
                    + match c {
                        '(' => 1,
                        '[' => 2,
                        '{' => 3,
                        '<' => 4,
                        _ => panic!("invalid char"),
                    }
            })
        })
        .collect::<BinaryHeap<_>>();

    // `BinaryHeap::into_iter_sorted()` would be nice but it's nightly.
    (0..=points.len() / 2)
        .map(|_| points.pop().unwrap())
        .last()
        .unwrap()
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 271245);
    assert_eq!(part2(&foo), 1685293086);
}
