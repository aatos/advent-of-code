use itertools::Itertools;
use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Polymer = Vec<char>;
type Translater = HashMap<(char, char), char>;
type Container = (Polymer, Translater);

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut polymer: Polymer = Vec::new();
        let mut translater: Translater = HashMap::new();

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();

            if x.contains(" -> ") {
                let t = x.split(" -> ").collect::<Vec<_>>();
                let from = t[0].chars().take(2).collect::<Vec<_>>();
                translater.insert((from[0], from[1]), t[1].chars().nth(0).unwrap());
            } else if !x.is_empty() {
                polymer = x.chars().collect();
            }
        });
        (polymer, translater)
    })
}

fn part1(c: &Container) -> usize {
    let (polymer, translater) = c;

    let polymer = (0..10).fold(polymer.clone(), |polymer, _| {
        let additions = polymer
            .windows(2)
            .map(|x| *translater.get(&(x[0], x[1])).unwrap())
            .collect::<Vec<char>>();

        polymer
            .into_iter()
            .interleave(additions.into_iter())
            .collect::<Polymer>()
    });

    let mut occurrences: HashMap<char, usize> = HashMap::new();
    polymer.iter().for_each(|c| {
        *occurrences.entry(*c).or_insert(0) += 1;
    });

    let (min, max) = occurrences
        .iter()
        .fold((usize::MAX, usize::MIN), |(min, max), (_, c)| {
            (cmp::min(min, *c), cmp::max(max, *c))
        });

    max - min
}

fn part2(c: &Container) -> usize {
    let (polymer, translater) = c;

    let mut occurrences: HashMap<(char, char), usize> = HashMap::new();
    polymer.windows(2).for_each(|cc| {
        *occurrences.entry((cc[0], cc[1])).or_insert(0) += 1;
    });

    let occurrences = (0..40).fold(occurrences, |occurrences, _| {
        let mut n: HashMap<(char, char), usize> = HashMap::new();

        occurrences.iter().for_each(|(&(k1, k2), v)| {
            let to = *translater.get(&(k1, k2)).unwrap();
            *n.entry((k1, to)).or_insert(0) += v;
            *n.entry((to, k2)).or_insert(0) += v;
        });

        n
    });

    let mut one_letter: HashMap<char, usize> = HashMap::new();
    occurrences.iter().for_each(|(&(k1, k2), c)| {
        *one_letter.entry(k1).or_insert(0) += c / 2;
        *one_letter.entry(k2).or_insert(0) += c / 2;
    });

    let (min, max) = one_letter
        .iter()
        .fold((usize::MAX, usize::MIN), |(min, max), (_, c)| {
            (cmp::min(min, *c), cmp::max(max, *c))
        });

    max - min
}

fn main() {
    let foo = read("src/input").unwrap();

    assert_eq!(part1(&foo), 2068);
    assert_eq!(part2(&foo), 2158894777814);
}

#[test]
fn test_input() {
    let foo = read("src/test").unwrap();

    assert_eq!(part1(&foo), 1588);
    assert_eq!(part2(&foo), 2188189693529);
}
