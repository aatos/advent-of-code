use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Container = (HashMap<String, (String, String)>, Vec<char>);

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut directions = vec![];
        let mut map = HashMap::new();

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();
            if !x.is_empty() && !x.contains(' ') {
                directions.extend(x.chars());
            } else if !x.is_empty() {
                let m: Vec<_> = x.split(" = ").collect();
                let turns: Vec<_> = m[1]
                    .strip_prefix('(')
                    .unwrap()
                    .strip_suffix(')')
                    .unwrap()
                    .split(", ")
                    .collect();
                map.insert(
                    m[0].to_string(),
                    (turns[0].to_string(), turns[1].to_string()),
                );
            }
        });
        (map, directions)
    })
}

fn part1(c: &Container) -> usize {
    let (map, directions) = c;
    let mut steps = 0;
    let mut location = "AAA";

    for dir in directions.iter().cycle() {
        if location == "ZZZ" {
            break;
        }
        let next = map.get(location).unwrap();
        location = if *dir == 'L' { &next.0 } else { &next.1 };
        steps += 1;
    }

    steps
}

fn part2(c: &Container) -> usize {
    let (map, directions) = c;
    map.keys()
        .filter(|l| l.ends_with('A'))
        .map(|mut location| {
            let mut steps = 0;
            for dir in directions.iter().cycle() {
                if location.ends_with('Z') {
                    break;
                }
                let next = map.get(location).unwrap();
                location = if *dir == 'L' { &next.0 } else { &next.1 };

                steps += 1;
            }
            steps
        })
        .fold(1, num::integer::lcm)
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 20569);
    assert_eq!(part2(&data), 21366921060721);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 2);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();

    assert_eq!(part2(&data), 6);
}
