use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn part1(values: &Container) -> u32 {
    values
        .iter()
        .map(|v| {
            let digits: Vec<_> = v.iter().filter(|&c| c.is_numeric()).collect();
            let mut num = String::new();
            num.push(**digits.first().unwrap());
            num.push(**digits.last().unwrap());
            num.parse::<u32>().unwrap()
        })
        .sum()
}

fn get_digit(s: &String) -> Option<char> {
    match &s[..] {
        "one" => Some('1'),
        "two" => Some('2'),
        "three" => Some('3'),
        "four" => Some('4'),
        "five" => Some('5'),
        "six" => Some('6'),
        "seven" => Some('7'),
        "eight" => Some('8'),
        "nine" => Some('9'),
        _ => None,
    }
}

fn part2(values: &Container) -> u32 {
    values
        .iter()
        .map(|v| {
            let mut num = String::new();
            for i in 0..v.len() {
                if v[i].is_numeric() {
                    if num.is_empty() {
                        num.push(v[i]);
                        num.push(v[i]);
                    } else {
                        num.pop();
                        num.push(v[i]);
                    }
                } else {
                    // TODO: improve this
                    for j in 0..i {
                        let s: String = v.iter().skip(j).take(i + 1 - j).collect();
                        if let Some(d) = get_digit(&s) {
                            if num.is_empty() {
                                num.push(d);
                                num.push(d);
                            } else {
                                num.pop();
                                num.push(d);
                            }
                        }
                    }
                }
            }
            num.parse::<u32>().unwrap()
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 55971);
    assert_eq!(part2(&data), 54719);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part2(&data), 281);
}
