use linked_hash_map::LinkedHashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file).lines().fold(vec![], |mut seq, x| {
            let x = x.unwrap();
            seq.extend(x.split(',').map(|x| x.chars().collect()));
            seq
        })
    })
}

fn hash(chars: &Vec<char>) -> usize {
    chars
        .iter()
        .fold(0, |hash, &c| ((hash + c as usize) * 17) % 256)
}

fn part1(c: &Container) -> usize {
    c.iter().map(hash).sum()
}

fn part2(c: &Container) -> usize {
    let boxes = c
        .iter()
        .fold(vec![LinkedHashMap::new(); 256], |mut boxes, chars| {
            let label: String = chars.iter().take_while(|c| c.is_alphabetic()).collect();
            let h = hash(&label.chars().collect::<Vec<_>>());
            match chars[label.len()] {
                '-' => {
                    boxes[h].remove(&label);
                }
                '=' => {
                    *boxes[h].entry(label.clone()).or_default() =
                        chars[label.len() + 1].to_digit(10).unwrap() as usize;
                }
                x => panic!("{}", x),
            };
            boxes
        });

    boxes
        .iter()
        .enumerate()
        .map(|(i, lenses)| {
            lenses
                .values()
                .enumerate()
                .map(|(j, v)| (i + 1) * (j + 1) * v)
                .sum::<usize>()
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 516657);
    assert_eq!(part2(&data), 210906);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 1320);
    assert_eq!(part2(&data), 145);
}

#[test]
fn test_hash() {
    assert_eq!(hash(&vec!['r', 'n']), 0);
    assert_eq!(hash(&vec!['q', 'p']), 1);
}
