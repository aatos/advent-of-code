use itertools::iproduct;
use std::cmp;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn to_map(c: &Container) -> HashMap<usize, (usize, usize)> {
    let mut counter = 1;

    iproduct!(0..c.len(), 0..c[0].len())
        .filter_map(|(i, j)| {
            if c[i][j] == '#' {
                counter += 1;
                Some((counter, (i, j)))
            } else {
                None
            }
        })
        .collect()
}

fn sort_tuple<T: Ord + Copy>(c: &(T, T)) -> (T, T) {
    (cmp::min(c.0, c.1), cmp::max(c.0, c.1))
}

fn part2(c: &Container, expand_size: usize) -> usize {
    let empty_rows: HashSet<_> = (0..c.len())
        .filter(|&row| c[row].iter().all(|&c| c == '.'))
        .collect();

    let empty_columns: HashSet<_> = (0..c[0].len())
        .filter(|&column| (0..c.len()).all(|row| c[row][column] == '.'))
        .collect();

    let locations = to_map(c);
    let galaxies: Vec<_> = locations.keys().cloned().collect();

    let mut counted: HashSet<(usize, usize)> = HashSet::new();

    galaxies
        .iter()
        .map(|&g| {
            galaxies
                .iter()
                .filter(|&&o| o != g)
                .map(|&o| {
                    let (o, g) = sort_tuple(&(o, g));
                    if counted.contains(&(o, g)) {
                        0
                    } else {
                        counted.insert((o, g));

                        let &(ox, oy) = locations.get(&o).unwrap();
                        let &(x, y) = locations.get(&g).unwrap();

                        let (x1, x2) = sort_tuple(&(ox, x));
                        let (y1, y2) = sort_tuple(&(oy, y));

                        let extras = ((x1..=x2).filter(|x| empty_rows.contains(x)).count()
                            + (y1..=y2).filter(|y| empty_columns.contains(y)).count())
                            * (expand_size - 1);

                        ((x1 as i64 - x2 as i64).abs() + (y1 as i64 - y2 as i64).abs()) as usize
                            + extras
                    }
                })
                .sum::<usize>()
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part2(&data, 2), 9957702);
    assert_eq!(part2(&data, 1_000_000), 512240933238);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part2(&data, 2), 374);
}
