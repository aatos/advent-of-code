use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<(Vec<char>, Vec<usize>)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let (record, sizes) = x.split_once(' ').unwrap();
                let sizes = sizes
                    .split(',')
                    .map(|c| c.parse::<usize>().unwrap())
                    .collect();

                (record.chars().collect(), sizes)
            })
            .collect()
    })
}

struct Process {
    memory: HashMap<String, usize>,
    sizes: Vec<usize>,
}

impl Process {
    fn new(sizes: &Vec<usize>) -> Process {
        Process {
            memory: HashMap::new(),
            sizes: sizes.clone(),
        }
    }

    fn matches(&self, record: &Vec<char>) -> bool {
        let mut i = 0;
        let mut j = 0;

        let mut cur_group_len = 0;

        while i < record.len() {
            match record[i] {
                '#' => cur_group_len += 1,
                '.' => {
                    if cur_group_len != 0 {
                        if j >= self.sizes.len() || cur_group_len != self.sizes[j] {
                            return false;
                        }
                        cur_group_len = 0;
                        j += 1;
                    }
                }
                _ => panic!(),
            }

            i += 1;
        }

        if cur_group_len != 0 && (j >= self.sizes.len() || cur_group_len != self.sizes[j]) {
            return false;
        }

        (cur_group_len == 0 && j == self.sizes.len())
            || (cur_group_len != 0 && j + 1 == self.sizes.len())
    }

    fn is_valid(&mut self, record: &Vec<char>, i: usize) -> usize {
        let s: String = record.iter().collect();

        if let Some(&count) = self.memory.get(&s) {
            return count;
        }

        let count = if i == record.len() {
            if self.matches(record) {
                1
            } else {
                0
            }
        } else if record[i] == '?' {
            let mut next1 = record.clone();
            next1[i] = '#';
            let mut next2 = record.clone();
            next2[i] = '.';
            self.is_valid(&next1, i + 1) + self.is_valid(&next2, i + 1)
        } else {
            self.is_valid(record, i + 1)
        };

        self.memory.insert(s, count);

        count
    }
}

fn process(record: &Vec<char>, sizes: &Vec<usize>) -> usize {
    let mut p = Process::new(sizes);
    p.is_valid(record, 0)
}

fn part1(c: &Container) -> usize {
    c.iter().map(|(record, sizes)| process(record, sizes)).sum()
}

// TODO: fix this
fn part2(c: &Container) -> usize {
    c.iter()
        .map(|(record, sizes)| {
            let mut n = vec![];
            for i in 0..5 {
                n.extend(record);
                if i != 4 {
                    n.push('?');
                }
            }

            process(&n, &sizes.repeat(5))
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 7633);
    //assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 21);
    //assert_eq!(part2(&data), 525152);
}

#[test]
fn test_process() {
    assert_eq!(process(&"???.###".chars().collect(), &vec![1, 1, 3]), 1);
    assert_eq!(
        process(&".??..??...?##.".chars().collect(), &vec![1, 1, 3]),
        4
    );
    assert_eq!(
        process(&"?#?#?#?#?#?#?#?".chars().collect(), &vec![1, 3, 1, 6]),
        1
    );
    assert_eq!(
        process(&"????.#...#...".chars().collect(), &vec![4, 1, 1]),
        1
    );
    assert_eq!(
        process(&"????.######..#####.".chars().collect(), &vec![1, 6, 5]),
        4
    );
    assert_eq!(
        process(&"?###????????".chars().collect(), &vec![3, 2, 1]),
        10
    );
}
