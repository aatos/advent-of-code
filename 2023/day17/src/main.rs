use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u32;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                x.chars().map(|c| c.to_digit(10).unwrap()).collect()
            })
            .collect()
    })
}

#[derive(PartialEq, Debug, Hash, Eq, Clone)]
enum Direction {
    Up(u32),
    Down(u32),
    Right(u32),
    Left(u32),
}

impl Direction {
    fn get_len(&self) -> u32 {
        match self {
            Direction::Up(x) => *x,
            Direction::Down(x) => *x,
            Direction::Right(x) => *x,
            Direction::Left(x) => *x,
        }
    }
}

// TODO: this is too slow (release optimized takes 5.8s on my machine)
fn part1(c: &Container) -> usize {
    let dest = ((c.len() - 1) as i64, (c[c.len() - 1].len() - 1) as i64);

    let mut q: VecDeque<(i64, i64, Direction, usize)> = VecDeque::new();
    let mut map: HashMap<(i64, i64, Direction), usize> = HashMap::new();

    q.push_back((0, 1, Direction::Right(1), 0));
    q.push_back((1, 0, Direction::Down(1), 0));

    while let Some((i, j, d, heat)) = q.pop_front() {
        if i < 0
            || j < 0
            || (i as usize >= c.len())
            || (j as usize >= c[i as usize].len())
            || d.get_len() > 3
        {
            continue;
        } else {
            let heat = heat + c[i as usize][j as usize] as usize;
            if let Some(prev) = map.get(&(i, j, d.clone())) {
                if *prev <= heat {
                    continue;
                }
            }
            map.insert((i, j, d.clone()), heat);

            if (i, j) == dest {
                continue;
            }

            match d {
                Direction::Up(x) => {
                    q.push_back((i - 1, j, Direction::Up(x + 1), heat));
                    q.push_back((i, j + 1, Direction::Right(1), heat));
                    q.push_back((i, j - 1, Direction::Left(1), heat));
                }

                Direction::Down(x) => {
                    q.push_back((i + 1, j, Direction::Down(x + 1), heat));
                    q.push_back((i, j + 1, Direction::Right(1), heat));
                    q.push_back((i, j - 1, Direction::Left(1), heat));
                }
                Direction::Left(x) => {
                    q.push_back((i, j - 1, Direction::Left(x + 1), heat));
                    q.push_back((i - 1, j, Direction::Up(1), heat));
                    q.push_back((i + 1, j, Direction::Down(1), heat));
                }
                Direction::Right(x) => {
                    q.push_back((i, j + 1, Direction::Right(x + 1), heat));
                    q.push_back((i - 1, j, Direction::Up(1), heat));
                    q.push_back((i + 1, j, Direction::Down(1), heat));
                }
            };
        }
    }

    map.into_iter()
        .filter_map(|((i, j, _), v)| if (i, j) == dest { Some(v) } else { None })
        .min()
        .unwrap()
}

// TODO: finish this
fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 668);
    //assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 102);
    //assert_eq!(part2(&data), 0);
}
