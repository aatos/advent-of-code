use itertools::iproduct;
use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    iproduct!(min_y..=max_y, min_x..=max_x).filter(move |&(j, i)| (i != x || j != y))
}

fn part1(schematic: &Container) -> usize {
    let mut sum = 0;

    for i in 0..schematic.len() {
        let mut cur_num = String::new();
        let mut touches = false;

        for j in 0..schematic[i].len() {
            if schematic[i][j].is_numeric() {
                cur_num.push(schematic[i][j]);
                if neighbors(schematic[i].len(), schematic.len(), j, i)
                    .any(|(y, x)| !schematic[x][y].is_numeric() && schematic[x][y] != '.')
                {
                    touches = true;
                }
            } else {
                if !cur_num.is_empty() && touches {
                    sum += cur_num.parse::<usize>().unwrap();
                }
                cur_num = String::new();
                touches = false;
            }
        }
        if !cur_num.is_empty() && touches {
            sum += cur_num.parse::<usize>().unwrap();
        }
    }

    sum
}

fn part2(schematic: &Container) -> usize {
    let mut gears: HashMap<(usize, usize), Vec<usize>> = HashMap::new();

    for i in 0..schematic.len() {
        let mut cur_num = String::new();
        let mut touches = None;

        for j in 0..schematic[i].len() {
            if schematic[i][j].is_numeric() {
                cur_num.push(schematic[i][j]);
                if let Some((x, y)) = neighbors(schematic[i].len(), schematic.len(), j, i)
                    .find(|&(y, x)| schematic[x][y] == '*')
                {
                    touches = Some((x, y));
                }
            } else {
                if let Some((x, y)) = touches {
                    if !cur_num.is_empty() {
                        gears
                            .entry((x, y))
                            .or_default()
                            .push(cur_num.parse::<usize>().unwrap());
                    }
                }
                cur_num = String::new();
                touches = None;
            }
        }
        if let Some((x, y)) = touches {
            if !cur_num.is_empty() {
                gears
                    .entry((x, y))
                    .or_default()
                    .push(cur_num.parse::<usize>().unwrap());
            }
        }
    }

    gears
        .values()
        .filter(|gears| gears.len() > 1)
        .map(|gears| gears.iter().product::<usize>())
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 540025);
    assert_eq!(part2(&data), 84584891);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 4361);
    assert_eq!(part2(&data), 467835);
}
