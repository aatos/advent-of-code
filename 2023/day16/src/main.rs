use std::collections::{HashSet, VecDeque};
use std::fs::File;
use std::io::{self, BufRead};

#[derive(PartialEq, Debug, Hash, Eq, Clone)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn process_beam(c: &Container, i: usize, j: usize, dir: Direction) -> usize {
    let mut visited: HashSet<(usize, usize, Direction)> = HashSet::new();
    let mut new_beams: VecDeque<(usize, usize, Direction)> = VecDeque::new();
    new_beams.push_back((i, j, dir));

    while let Some((i, j, dir)) = new_beams.pop_front() {
        if visited.contains(&(i, j, dir.clone())) {
            continue;
        }

        visited.insert((i, j, dir.clone()));

        let mut directions = vec![];
        match c[i][j] {
            '|' if dir == Direction::Left || dir == Direction::Right => {
                directions.push(Direction::Down);
                directions.push(Direction::Up);
            }
            '-' if dir == Direction::Up || dir == Direction::Down => {
                directions.push(Direction::Left);
                directions.push(Direction::Right);
            }
            '.' | '-' | '|' => {
                directions.push(dir);
            }
            '\\' if dir == Direction::Right => {
                directions.push(Direction::Down);
            }
            '\\' if dir == Direction::Down => {
                directions.push(Direction::Right);
            }
            '\\' if dir == Direction::Left => {
                directions.push(Direction::Up);
            }
            '\\' if dir == Direction::Up => {
                directions.push(Direction::Left);
            }

            '/' if dir == Direction::Right => {
                directions.push(Direction::Up);
            }
            '/' if dir == Direction::Down => {
                directions.push(Direction::Left);
            }
            '/' if dir == Direction::Left => {
                directions.push(Direction::Down);
            }
            '/' if dir == Direction::Up => {
                directions.push(Direction::Right);
            }
            x => panic!("{}, {}, {:?}: {}", i, j, dir, x),
        };

        directions.into_iter().for_each(|dir| match dir {
            Direction::Up if i > 0 => new_beams.push_back((i - 1, j, dir)),
            Direction::Right if j + 1 < c[i].len() => new_beams.push_back((i, j + 1, dir)),
            Direction::Down if i + 1 < c.len() => new_beams.push_back((i + 1, j, dir)),
            Direction::Left if j > 0 => new_beams.push_back((i, j - 1, dir)),
            _ => (),
        });
    }

    let visited: HashSet<_> = visited.into_iter().map(|(i, j, _)| (i, j)).collect();

    visited.len()
}

fn part1(c: &Container) -> usize {
    process_beam(c, 0, 0, Direction::Right)
}

fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 7472);
    // TODO: finish
    // assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 46);
    // assert_eq!(part2(&data), 51);
}
