use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
struct Container {
    seeds: Vec<Inner>,
    maps: Vec<Vec<(Inner, std::ops::Range<Inner>)>>,
}

impl Container {
    fn new() -> Container {
        Container {
            seeds: vec![],
            maps: vec![vec![]; 7],
        }
    }
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut container = Container::new();

        let mut dest = 0;

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();
            if x.starts_with("seeds: ") {
                container.seeds = x
                    .strip_prefix("seeds: ")
                    .unwrap()
                    .split(' ')
                    .map(|i| i.parse().unwrap())
                    .collect();
            } else if x.starts_with("seed-to-soil") {
                dest = 0;
            } else if x.starts_with("soil-to-fertilizer") {
                dest = 1;
            } else if x.starts_with("fertilizer-to-water") {
                dest = 2;
            } else if x.starts_with("water-to-light") {
                dest = 3;
            } else if x.starts_with("light-to-temperature") {
                dest = 4;
            } else if x.starts_with("temperature-to-humidity") {
                dest = 5;
            } else if x.starts_with("humidity-to-location") {
                dest = 6;
            } else if !x.is_empty() {
                let nums: Vec<Inner> = x.split(' ').map(|i| i.parse().unwrap()).collect();
                container.maps[dest].push((nums[0], nums[1]..(nums[1] + nums[2])));
            }
        });
        container
    })
}

fn part1(c: &Container) -> u64 {
    c.seeds
        .iter()
        .map(|&seed| {
            c.maps.iter().fold(seed, |v, map| {
                map.iter()
                    .find_map(|(dest, src_range)| {
                        if src_range.contains(&v) {
                            Some(*dest + (v - src_range.start))
                        } else {
                            None
                        }
                    })
                    .unwrap_or(v)
            })
        })
        .min()
        .unwrap()
}

fn part2(c: &Container) -> u64 {
    let seed_ranges: VecDeque<_> = c.seeds.chunks(2).map(|ss| ss[0]..(ss[0] + ss[1])).collect();

    c.maps
        .iter()
        .fold(seed_ranges, |mut ranges, map| {
            let mut next = VecDeque::new();
            while let Some(v_range) = ranges.pop_front() {
                next.push_back(
                    map.iter()
                        .find_map(|(dest, src_range)| {
                            if src_range.contains(&v_range.start)
                                && src_range.contains(&v_range.end)
                            {
                                let diff = v_range.start - src_range.start;
                                let len = v_range.end - v_range.start;

                                Some((dest + diff)..(dest + diff + len))
                            } else if src_range.contains(&v_range.start) {
                                ranges.push_back(src_range.end..v_range.end);

                                let diff = v_range.start - src_range.start;
                                let len = src_range.end - 1 - v_range.start;

                                Some((dest + diff)..(dest + diff + len))
                            } else if src_range.contains(&v_range.end) {
                                ranges.push_back(v_range.start..(src_range.start - 1));

                                let len = v_range.end - src_range.start;

                                Some(*dest..(dest + len))
                            } else {
                                None
                            }
                        })
                        .unwrap_or(v_range),
                );
            }
            next
        })
        .into_iter()
        .map(|range: std::ops::Range<Inner>| range.start)
        .min()
        .unwrap()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 309796150);
    assert_eq!(part2(&data), 50716416);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 35);
    assert_eq!(part2(&data), 46);
}
