use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = usize;
type Container = Vec<(String, Inner)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                let hand: Vec<_> = x.split(' ').collect();

                (hand[0].to_string(), hand[1].parse::<Inner>().unwrap())
            })
            .collect()
    })
}

fn hand_value(hand: &str) -> usize {
    let mut counts: BinaryHeap<_> = hand
        .chars()
        .fold(HashMap::new(), |mut counts, c| {
            *counts.entry(c).or_insert(0) += 1;
            counts
        })
        .into_values()
        .collect();
    match counts.pop().unwrap() {
        5 => 7,
        4 => 6,
        3 if *counts.peek().unwrap() == 2 => 5,
        3 => 4,
        2 if *counts.peek().unwrap() == 2 => 2,
        2 => 1,
        _ => 0,
    }
}

fn hand_value_joker(hand: &str) -> usize {
    let (counts, jokers): (HashMap<char, usize>, usize) =
        hand.chars()
            .fold((HashMap::new(), 0), |(mut counts, jokers), c| {
                if c == 'J' {
                    (counts, jokers + 1)
                } else {
                    *counts.entry(c).or_insert(0) += 1;
                    (counts, jokers)
                }
            });
    let mut counts: BinaryHeap<_> = counts.into_values().collect();

    match counts.pop().unwrap_or(0) + jokers {
        5 => 7,
        4 => 6,
        3 if *counts.peek().unwrap_or(&0) == 2 => 5,
        3 => 4,
        2 if *counts.peek().unwrap_or(&0) == 2 => 2,
        2 => 1,
        _ => 0,
    }
}

fn card_value(c: char) -> usize {
    match c {
        'A' => 13,
        'K' => 12,
        'Q' => 11,
        'J' => 10,
        'T' => 9,
        '9' => 8,
        '8' => 7,
        '7' => 6,
        '6' => 5,
        '5' => 4,
        '4' => 3,
        '3' => 2,
        '2' => 1,
        _ => panic!(),
    }
}

fn card_value_joker(c: char) -> usize {
    match c {
        'A' => 13,
        'K' => 12,
        'Q' => 11,
        'T' => 9,
        '9' => 8,
        '8' => 7,
        '7' => 6,
        '6' => 5,
        '5' => 4,
        '4' => 3,
        '3' => 2,
        '2' => 1,
        'J' => 0,
        _ => panic!(),
    }
}

fn cmp(this: &str, other: &str) -> Ordering {
    let sval = hand_value(this);
    let oval = hand_value(other);
    if sval == oval {
        let (c1, c2) = this
            .chars()
            .zip(other.chars())
            .find(|&(c1, c2)| c1 != c2)
            .unwrap_or((this.chars().next().unwrap(), other.chars().next().unwrap()));
        card_value(c1).cmp(&card_value(c2))
    } else {
        sval.cmp(&oval)
    }
}

fn cmp_joker(this: &str, other: &str) -> Ordering {
    let sval = hand_value_joker(this);
    let oval = hand_value_joker(other);
    if sval == oval {
        let (c1, c2) = this
            .chars()
            .zip(other.chars())
            .find(|&(c1, c2)| c1 != c2)
            .unwrap_or((this.chars().next().unwrap(), other.chars().next().unwrap()));
        card_value_joker(c1).cmp(&card_value_joker(c2))
    } else {
        sval.cmp(&oval)
    }
}

fn part1(c: &Container) -> usize {
    let mut sorted: Vec<_> = c.iter().collect();
    sorted.sort_by(|(a, _), (b, _)| cmp(a, b));
    sorted
        .into_iter()
        .enumerate()
        .map(|(rank, (_, bid))| (rank + 1) * bid)
        .sum()
}

fn part2(c: &Container) -> usize {
    let mut sorted: Vec<_> = c.iter().collect();
    sorted.sort_by(|(a, _), (b, _)| cmp_joker(a, b));
    sorted
        .into_iter()
        .enumerate()
        .map(|(rank, (_, bid))| (rank + 1) * bid)
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 248569531);
    assert_eq!(part2(&data), 250382098);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 6440);
    assert_eq!(part2(&data), 5905);
}
