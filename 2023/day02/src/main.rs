use std::cmp;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};
type Container = HashMap<usize, Vec<(usize, usize, usize)>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();

                let game: Vec<_> = x.split(": ").collect();
                let num = game[0].split(" ").last().unwrap().parse().unwrap();
                (
                    num,
                    game.last()
                        .unwrap()
                        .split("; ")
                        .map(|g| {
                            g.split(", ").fold((0, 0, 0), |(r, g, b), s| {
                                let s: Vec<_> = s.split(" ").collect();
                                let count: usize = s[0].parse().unwrap();

                                match s[1] {
                                    "red" => (r + count, g, b),
                                    "green" => (r, g + count, b),
                                    "blue" => (r, g, b + count),
                                    x => panic!("invalid cube: {}", x),
                                }
                            })
                        })
                        .collect(),
                )
            })
            .collect()
    })
}

fn part1(games: &Container) -> usize {
    let max_red_count = 12;
    let max_green_count = 13;
    let max_blue_count = 14;

    games
        .into_iter()
        .filter_map(|(i, sets)| {
            if sets.into_iter().any(|&(red, green, blue)| {
                red > max_red_count || green > max_green_count || blue > max_blue_count
            }) {
                None
            } else {
                Some(*i)
            }
        })
        .sum()
}

fn part2(games: &Container) -> usize {
    games
        .into_iter()
        .map(|(_, sets)| {
            let (min_red, min_green, min_blue) =
                sets.into_iter()
                    .fold((0, 0, 0), |(min_red, min_green, min_blue), &(r, g, b)| {
                        (
                            cmp::max(min_red, r),
                            cmp::max(min_green, g),
                            cmp::max(min_blue, b),
                        )
                    });
            min_red * min_green * min_blue
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 2727);
    assert_eq!(part2(&data), 56580);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 8);
    assert_eq!(part2(&data), 2286);
}
