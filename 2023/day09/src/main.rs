use std::fs::File;
use std::io::{self, BufRead};

type Inner = i64;
type Container = Vec<Vec<Inner>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                x.unwrap()
                    .split(' ')
                    .map(|x| {
                        x.parse::<Inner>()
                            .unwrap_or_else(|e| panic!("{}: {}", e, x))
                    })
                    .collect()
            })
            .collect()
    })
}

fn sequences(values: &[Inner]) -> Vec<Vec<Inner>> {
    let mut sequences: Vec<Vec<Inner>> = vec![values.to_owned()];
    while let Some(line) = sequences.last() {
        let next: Vec<_> = line.windows(2).map(|nums| nums[1] - nums[0]).collect();
        if next.iter().all(|&i| i == 0) {
            break;
        }
        sequences.push(next);
    }
    sequences
}

fn part1(c: &Container) -> Inner {
    c.iter()
        .map(|vals| {
            sequences(vals)
                .into_iter()
                .rev()
                .fold(0, |last, seq| last + seq.last().unwrap())
        })
        .sum()
}

fn part2(c: &Container) -> Inner {
    c.iter()
        .map(|vals| {
            sequences(vals)
                .into_iter()
                .rev()
                .fold(0, |last, seq| seq[0] - last)
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 1819125966);
    assert_eq!(part2(&data), 1140);
}
#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 114);
    assert_eq!(part2(&data), 2);
}

#[test]
fn test_input_2() {
    let data = vec![
        vec![
            11, 15, 30, 65, 125, 208, 302, 382, 407, 317, 30, -561, -1591, -3226, -5666, -9148,
            -13949, -20389, -28834, -39699, -53451,
        ],
        vec![
            -3, -2, 0, 3, 7, 12, 18, 25, 33, 42, 52, 63, 75, 88, 102, 117, 133, 150, 168, 187, 207,
        ],
    ];

    assert_eq!(part1(&data), -70384);
}
