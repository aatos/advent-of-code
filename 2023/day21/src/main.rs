use std::cmp;
use std::collections::HashSet;
use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};
use std::iter;

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();
                x.chars().collect()
            })
            .collect()
    })
}

fn non_diag_neighbors(
    y_size: usize,
    x_size: usize,
    y: usize,
    x: usize,
) -> impl Iterator<Item = (usize, usize)> + 'static {
    let min_x = cmp::max(x as i32 - 1, 0) as usize;
    let max_x = cmp::min(x + 1, x_size - 1);

    let min_y = cmp::max(y as i32 - 1, 0) as usize;
    let max_y = cmp::min(y + 1, y_size - 1);

    (min_y..=max_y)
        .zip(iter::repeat(x))
        .chain(iter::repeat(y).zip(min_x..=max_x))
        .filter(move |&(j, i)| (i != x || j != y))
}

fn part1(c: &Container, steps: usize) -> usize {
    let mut q = VecDeque::new();
    let mut visited = HashSet::new();
    let mut memory = HashSet::new();

    for i in 0..c.len() {
        for j in 0..c[i].len() {
            if c[i][j] == 'S' {
                q.push_back((i, j, steps));
            }
        }
    }

    while let Some((i, j, steps)) = q.pop_front() {
        if steps == 0 {
            visited.insert((i, j));
            continue;
        }

        non_diag_neighbors(c.len(), c[0].len(), i, j).for_each(|(x, y)| {
            if c[x][y] != '#' && !memory.contains(&(x, y, steps - 1)) {
                q.push_back((x, y, steps - 1));
                memory.insert((x, y, steps - 1));
            }
        })
    }

    visited.len()
}

// TODO: finish
fn part2(_: &Container) -> usize {
    panic!("part 2 unfinished")
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data, 64), 3788);
    //assert_eq!(part2(&data), 0);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data, 6), 16);
    //assert_eq!(part2(&data), 0);
}
