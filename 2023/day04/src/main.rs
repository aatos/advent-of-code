use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = usize;
type Container = HashMap<usize, (BinaryHeap<Inner>, BinaryHeap<Inner>)>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| {
                let x = x.unwrap();

                let game: Vec<_> = x.split(": ").collect();
                let num: usize = game[0].split(' ').last().unwrap().parse().unwrap();

                let nums: Vec<_> = game.last().unwrap().split(" | ").collect();

                let ours = nums[0];
                let winning = nums.last().unwrap();

                let ours: BinaryHeap<usize> = ours
                    .split(' ')
                    .filter(|&c| !c.is_empty())
                    .map(|n| n.parse().unwrap())
                    .collect();
                let winning: BinaryHeap<usize> = winning
                    .split(' ')
                    .filter(|&c| !c.is_empty())
                    .map(|n| n.parse().unwrap())
                    .collect();
                (num, (ours, winning))
            })
            .collect()
    })
}

fn part1(mut cards: Container) -> usize {
    cards
        .values_mut()
        .map(|(ref mut ours, ref mut theirs)| {
            let mut points = 0;
            while let (Some(&o), Some(&t)) = (ours.peek(), theirs.peek()) {
                if o == t {
                    if points == 0 {
                        points += 1;
                    } else {
                        points *= 2;
                    }
                    ours.pop();
                } else if o > t {
                    ours.pop();
                } else if t > o {
                    theirs.pop();
                }
            }
            points
        })
        .sum()
}

fn part2(mut all_cards: Container) -> usize {
    let mut cards = vec![1; all_cards.len()];
    (1..=cards.len()).fold(all_cards.len(), |sum, i| {
        let (mut ours, mut theirs) = all_cards.remove(&i).unwrap();

        let mut count = 0;
        let mut next = i + 1;
        while let (Some(&o), Some(&t)) = (ours.peek(), theirs.peek()) {
            if o == t {
                cards[next - 1] += cards[i - 1];
                count += cards[i - 1];
                next += 1;
                ours.pop();
            } else if o > t {
                ours.pop();
            } else if t > o {
                theirs.pop();
            }
        }
        sum + count
    })
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(data.clone()), 23441);
    assert_eq!(part2(data.clone()), 5923918);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(data.clone()), 13);
    assert_eq!(part2(data.clone()), 30);
}
