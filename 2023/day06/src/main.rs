use std::fmt::Write;
use std::fs::File;
use std::io::{self, BufRead};

type Inner = u64;
type Container = Vec<(Inner, Inner)>;

fn line_to_nums<'a>(x: &'a str, prefix: &str) -> impl Iterator<Item = Inner> + 'a {
    x.strip_prefix(prefix)
        .unwrap()
        .split(' ')
        .map(|s| s.trim())
        .filter_map(|s| {
            if s.is_empty() {
                None
            } else {
                s.parse::<Inner>().ok()
            }
        })
}

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut times = vec![];
        let mut distances = vec![];

        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();

            if x.starts_with("Time: ") {
                times.extend(line_to_nums(&x, "Time: "));
            } else if x.starts_with("Distance: ") {
                distances.extend(line_to_nums(&x, "Distance: "));
            }
        });
        times.into_iter().zip(distances).collect()
    })
}

fn part1(c: &Container) -> usize {
    c.iter()
        .map(|&(race_time, record_dist)| {
            (0..race_time)
                .filter(|hold_time| hold_time * (race_time - hold_time) > record_dist)
                .count()
        })
        .product()
}

fn part2(c: &Container) -> usize {
    let (time, dist) = c.iter().fold(
        (String::new(), String::new()),
        |(mut time_str, mut dist_str), &(time, dist)| {
            write!(&mut time_str, "{}", time).unwrap();
            write!(&mut dist_str, "{}", dist).unwrap();

            (time_str, dist_str)
        },
    );
    part1(&vec![(time.parse().unwrap(), dist.parse().unwrap())])
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 800280);
    assert_eq!(part2(&data), 45128024);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 288);
    assert_eq!(part2(&data), 71503);
}
