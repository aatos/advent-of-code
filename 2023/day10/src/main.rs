use std::collections::HashMap;
use std::collections::VecDeque;

use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn can_go_up(pipes: &Container, x: usize, y: usize) -> bool {
    x > 0 && ['7', '|', 'F'].contains(&pipes[x - 1][y])
}

fn can_go_down(pipes: &Container, x: usize, y: usize) -> bool {
    x + 1 < pipes.len() && ['L', 'J', '|'].contains(&pipes[x + 1][y])
}

fn can_go_left(pipes: &Container, x: usize, y: usize) -> bool {
    y > 0 && ['-', 'L', 'F'].contains(&pipes[x][y - 1])
}

fn can_go_right(pipes: &Container, x: usize, y: usize) -> bool {
    y + 1 < pipes[0].len() && ['-', 'J', '7'].contains(&pipes[x][y + 1])
}

fn count_distances(pipes: &Container, start_pipe: char) -> HashMap<(usize, usize), u32> {
    let (x, y) = pipes
        .iter()
        .enumerate()
        .find_map(|(x, p)| {
            p.iter()
                .enumerate()
                .find_map(|(y, p)| if *p == 'S' { Some(y) } else { None })
                .map(|y| (x, y))
        })
        .unwrap();

    let mut pipes = pipes.clone();
    pipes[x][y] = start_pipe;

    let mut distances: HashMap<(usize, usize), u32> = HashMap::new();

    let mut queue = VecDeque::new();
    queue.push_front((x, y, 0));

    while let Some((x, y, travel)) = queue.pop_front() {
        distances.insert((x, y), travel);
        let mut possible_moves = vec![];
        match pipes[x][y] {
            '|' => {
                if can_go_up(&pipes, x, y) {
                    possible_moves.push((x - 1, y));
                }
                if can_go_down(&pipes, x, y) {
                    possible_moves.push((x + 1, y));
                }
            }
            '-' => {
                if can_go_right(&pipes, x, y) {
                    possible_moves.push((x, y + 1));
                }
                if can_go_left(&pipes, x, y) {
                    possible_moves.push((x, y - 1));
                }
            }
            'L' => {
                if can_go_up(&pipes, x, y) {
                    possible_moves.push((x - 1, y));
                }
                if can_go_right(&pipes, x, y) {
                    possible_moves.push((x, y + 1));
                }
            }
            'J' => {
                if can_go_up(&pipes, x, y) {
                    possible_moves.push((x - 1, y));
                }
                if can_go_left(&pipes, x, y) {
                    possible_moves.push((x, y - 1));
                }
            }
            '7' => {
                if can_go_left(&pipes, x, y) {
                    possible_moves.push((x, y - 1));
                }

                if can_go_down(&pipes, x, y) {
                    possible_moves.push((x + 1, y));
                }
            }
            'F' => {
                if can_go_down(&pipes, x, y) {
                    possible_moves.push((x + 1, y));
                }
                if can_go_right(&pipes, x, y) {
                    possible_moves.push((x, y + 1));
                }
            }
            '.' => {}
            _ => panic!(),
        }

        queue.extend(possible_moves.into_iter().filter_map(|(x, y)| {
            if distances
                .get(&(x, y))
                .map(|&dist| dist > travel + 1)
                .unwrap_or(true)
            {
                Some((x, y, travel + 1))
            } else {
                None
            }
        }));
    }

    distances
}

fn part1(pipes: &Container, start_pipe: char) -> u32 {
    count_distances(pipes, start_pipe)
        .into_values()
        .max()
        .unwrap()
}

fn part2(pipes: &Container, start_pipe: char) -> usize {
    let distances = count_distances(pipes, start_pipe);

    (0..pipes.len())
        .map(|i| {
            (0..pipes[i].len())
                .filter(|&j| !distances.contains_key(&(i, j)))
                .filter(|&j| {
                    let crosses_pipes = (i..pipes.len())
                        .zip(j..pipes[i].len())
                        .filter(|&(x, y)| {
                            distances.contains_key(&(x, y)) && !['L', '7'].contains(&pipes[x][y])
                        })
                        .count();

                    crosses_pipes % 2 == 1
                })
                .count()
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data, '|'), 6909);
    assert_eq!(part2(&data, '|'), 461);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data, 'F'), 8);
}

#[test]
fn test_input2() {
    let data = read("src/test2").unwrap();

    assert_eq!(part2(&data, '7'), 10);
}

#[test]
fn test_input3() {
    let data = read("src/test3").unwrap();

    assert_eq!(part2(&data, 'F'), 4);
}
