use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<Vec<char>>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        let mut patterns = vec![];
        let mut pattern = vec![];
        io::BufReader::new(file).lines().for_each(|x| {
            let x = x.unwrap();
            if x.is_empty() {
                patterns.push(pattern.clone());
                pattern = vec![];
            } else {
                pattern.push(x.chars().collect());
            }
        });

        if !pattern.is_empty() {
            patterns.push(pattern);
        }

        patterns
    })
}

fn part1(c: &Container) -> usize {
    c.iter()
        .map(|pattern| {
            let indexes: Vec<_> = (0..pattern.len()).collect();
            let rows = indexes.windows(2).filter_map(|w| {
                if (0..=w[0])
                    .rev()
                    .zip(w[1]..pattern.len())
                    .all(|(i, j)| pattern[i] == pattern[j])
                {
                    Some((w[0] + 1) * 100)
                } else {
                    None
                }
            });

            let indexes: Vec<_> = (0..pattern[0].len()).collect();
            let columns = indexes.windows(2).filter_map(|w| {
                if (0..=w[0]).rev().zip(w[1]..pattern[0].len()).all(|(i, j)| {
                    pattern[0..pattern.len()]
                        .iter()
                        .map(|row| row[i])
                        .collect::<Vec<_>>()
                        == pattern[0..pattern.len()]
                            .iter()
                            .map(|row| row[j])
                            .collect::<Vec<_>>()
                }) {
                    Some(w[0] + 1)
                } else {
                    None
                }
            });

            columns.sum::<usize>() + rows.sum::<usize>()
        })
        .sum()
}

fn part2(c: &Container) -> usize {
    // TODO: improve this
    c.iter()
        .map(|pattern| {
            let indexes: Vec<_> = (0..pattern.len()).collect();
            let rows = indexes.windows(2).filter_map(|w| {
                let mut i = w[0];
                let mut j = w[1];

                let mut smudge_found = false;
                loop {
                    if pattern[i] != pattern[j] {
                        if !smudge_found
                            && pattern[i]
                                .iter()
                                .zip(pattern[j].iter())
                                .filter(|&(a, b)| a != b)
                                .count()
                                == 1
                        {
                            smudge_found = true;
                        } else {
                            return None;
                        }
                    }

                    if i == 0 || j + 1 == pattern.len() {
                        break;
                    }

                    i -= 1;
                    j += 1;
                }
                if !smudge_found {
                    None
                } else {
                    Some((w[0] + 1) * 100)
                }
            });

            let indexes: Vec<_> = (0..pattern[0].len()).collect();
            let columns = indexes.windows(2).filter_map(|w| {
                let mut i = w[0];
                let mut j = w[1];

                let mut smudge_found = false;
                loop {
                    let col1 = pattern[0..pattern.len()]
                        .iter()
                        .map(|row| row[i])
                        .collect::<Vec<_>>();
                    let col2 = pattern[0..pattern.len()]
                        .iter()
                        .map(|row| row[j])
                        .collect::<Vec<_>>();

                    if col1 != col2 {
                        if !smudge_found
                            && col1
                                .iter()
                                .zip(col2.iter())
                                .filter(|&(a, b)| a != b)
                                .count()
                                == 1
                        {
                            smudge_found = true;
                        } else {
                            return None;
                        }
                    }

                    if i == 0 || j + 1 == pattern[0].len() {
                        break;
                    }

                    i -= 1;
                    j += 1;
                }
                if !smudge_found {
                    None
                } else {
                    Some(w[0] + 1)
                }
            });

            columns.sum::<usize>() + rows.sum::<usize>()
        })
        .sum()
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(&data), 34911);
    assert_eq!(part2(&data), 33183);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(&data), 405);
    assert_eq!(part2(&data), 400);
}
