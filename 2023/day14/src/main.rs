use itertools::iproduct;
use std::fs::File;
use std::io::{self, BufRead};

type Container = Vec<Vec<char>>;

fn read(path: &str) -> io::Result<Container> {
    File::open(path).map(|file| {
        io::BufReader::new(file)
            .lines()
            .map(|x| x.unwrap().chars().collect())
            .collect()
    })
}

fn count_weight(c: &Container) -> usize {
    c.iter()
        .enumerate()
        .map(|(i, row)| row.iter().filter(|&&c| c == 'O').count() * (c.len() - i))
        .sum()
}

fn move_up(c: &mut Container, i: usize, j: usize) {
    for x in (0..i).rev() {
        match c[x][j] {
            '.' => {
                c[x][j] = 'O';
                c[x + 1][j] = '.';
            }
            '#' | 'O' => break,
            _ => panic!(),
        }
    }
}

fn move_right(c: &mut Container, i: usize, j: usize) {
    for y in (0..j).rev() {
        match c[i][y] {
            '.' => {
                c[i][y] = 'O';
                c[i][y + 1] = '.';
            }
            '#' | 'O' => break,
            _ => panic!(),
        }
    }
}

fn move_down(c: &mut Container, i: usize, j: usize) {
    for x in (i + 1)..c.len() {
        match c[x][j] {
            '.' => {
                c[x][j] = 'O';
                c[x - 1][j] = '.';
            }
            '#' | 'O' => break,
            _ => panic!(),
        }
    }
}

fn move_left(c: &mut Container, i: usize, j: usize) {
    for y in (j + 1)..c.len() {
        match c[i][y] {
            '.' => {
                c[i][y] = 'O';
                c[i][y - 1] = '.';
            }
            '#' | 'O' => break,
            _ => panic!(),
        }
    }
}

fn part1(mut c: Container) -> usize {
    iproduct!(1..c.len(), 0..c[0].len()).for_each(|(i, j)| {
        if c[i][j] == 'O' {
            move_up(&mut c, i, j)
        }
    });

    count_weight(&c)
}

fn part2(mut c: Container, cycles: usize) -> usize {
    for _ in 0..cycles {
        iproduct!(1..c.len(), 0..c[0].len()).for_each(|(i, j)| {
            if c[i][j] == 'O' {
                move_up(&mut c, i, j)
            }
        });

        iproduct!(0..c.len(), 1..c[0].len()).for_each(|(i, j)| {
            if c[i][j] == 'O' {
                move_right(&mut c, i, j)
            }
        });

        iproduct!((0..(c.len() - 1)).rev(), 0..c[0].len()).for_each(|(i, j)| {
            if c[i][j] == 'O' {
                move_down(&mut c, i, j)
            }
        });

        iproduct!(0..c.len(), (0..(c[0].len() - 1)).rev()).for_each(|(i, j)| {
            if c[i][j] == 'O' {
                move_left(&mut c, i, j)
            }
        });
    }
    count_weight(&c)
}

fn main() {
    let data = read("src/input").unwrap();

    assert_eq!(part1(data.clone()), 113525);
    // TODO: why it is the same with 1_000 and 1_000_000_000?
    assert_eq!(part2(data.clone(), 1_000), 101292);
}

#[test]
fn test_input() {
    let data = read("src/test").unwrap();

    assert_eq!(part1(data.clone()), 136);
    assert_eq!(part2(data.clone(), 1_000), 64);
}
